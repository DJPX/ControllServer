#include <iostream>
#include <stdexcept>
#include <stdio.h>
#include <string>
#include <vector>
#include "Service.h"
#include <unistd.h>
#include <list>
#include "Slog.h"
#include <sstream>
#include "Str2Int.h"
#include <algorithm>
#include <map>

#pragma once
typedef std::map<std::string, Service*> Servicemap;

class ControllServer
{
  public:                             
    ControllServer(std::string url);                     
    ControllServer(const ControllServer& a);   
    ~ControllServer();                     
  
    int registerService(std::string servicename,std::string serviceAddress);
    int start(std::string servicename);
    int stop(std::string servicename);
    int restart(std::string servicename);
    int status(std::string servicename);
    int startall();
    int stopall();
    int restartall();
    int statusall();
    int init();
    int startWatchermode();
    int stopWatchermode();
    // Returnvalue:
    // 0: Not running
    // 1: Running
    // 2: Server does not exist
    int isRunning(std::string servicename);


  private:
    bool watchermode=false;
    // TODO Maybe replace string with enum
    // And move this function to services to increase perfromance
    int updateStatus(std::string servicename, int status);   
    // TODO Create an execution class                     
    std::string executeCommand(std::string command);
    std::string url;
    // std::vector<Service> allServices;
    Servicemap allServices;
    // This funktion checks if there are new services or old services which need to get removed 
    int updateServiceList();
    Service* getService(std::string servicename);
    std::string getServices();
    

};