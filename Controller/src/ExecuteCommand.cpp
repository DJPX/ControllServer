#include "ExecuteCommand.h"
// ExecuteCommand::ExecuteCommand()
// {
// }
// ExecuteCommand::ExecuteCommand(const ExecuteCommand &a){};
// ExecuteCommand::~ExecuteCommand(){};


std::string ExecuteCommand::executeCommand(std::string command)
{
    char buffer[128];
    std::string result = "";

    // Open pipe to file
    FILE *pipe = popen(command.c_str(), "r");
    if (!pipe)
    {
        return "popen failed!";
    }

    // read till end of process:
    while (!feof(pipe))
    {

        // use buffer to read and add to result
        if (fgets(buffer, 128, pipe) != NULL)
            result += buffer;
    }

    pclose(pipe);
    return result;
}


// static std::string executeCommand(std::string command,bool showOutput){
//     if(!showOutput)
//     {
//         command+ " > /dev/null";
//     }
//     char buffer[128];
//     std::string result = "";

//     // Open pipe to file
//     FILE *pipe = popen(command.c_str(), "r");
//     if (!pipe)
//     {
//         return "popen failed!";
//     }

//     // read till end of process:
//     while (!feof(pipe))
//     {

//         // use buffer to read and add to result
//         if (fgets(buffer, 128, pipe) != NULL)
//             result += buffer;
//     }

//     pclose(pipe);
//     return result;
// }

// // static std::string runD(std::string command){
// //     return runD(command,true);
// // }