#include "Service.h"
Service::Service(std::string servicename, bool isPod) : BasicService(servicename)
{
    if (isPod)
    {
        setStartCommand("podman pod start " + servicename + "_server");
        setStopCommand("podman pod stop " + servicename + "_server");
        setStatusCommand("podman pod ps | grep " + servicename + "_server");
    }
    else
    {
        setStartCommand("podman start " + servicename + "_server");
        setStopCommand("podman stop " + servicename + "_server");
        setStatusCommand("podman ps | grep " + servicename + "_server");
    }
}
Service::Service(const Service &a) : BasicService(a){};
Service::~Service(){};

int Service::start()
{
    std::cout << runCommand("start") << std::endl;
    return 0;
}
int Service::stop()
{

    std::cout << runCommand("stop") << std::endl;
    return 0;
}
int Service::restart()
{
    stop();
    start();
    return 0;
}
std::string Service::getStatus()
{
    std::string output = runCommand("status");

    if (output.find(" ") != std::string::npos)
    {

        std::string temp = output.substr(output.find(" ") + 2);
        output = temp;
        if (output.find(" ") != std::string::npos)
        {

            temp = output.substr(output.find(" ") + 2);
            output = temp;
            if (output.find(" ") != std::string::npos)
            {
                temp = output.substr(0, output.find(" "));
                output = temp;
            }
        }
    }
    return output;
}

std::string Service::getServiceAddress()
{
    std::string output = runCommand("address");
    std::cout << "Serviceaddress raw: " << std::endl;
    std::cout << output << std::endl;
    if (output.find(" ") != std::string::npos)
    {

        std::string temp = output.substr(output.find(" ") + 2);
        output = temp;
        if (output.find(" ") != std::string::npos)
        {

            temp = output.substr(output.find(" ") + 2);
            output = temp;
            if (output.find(" ") != std::string::npos)
            {
                temp = output.substr(0, output.find(" "));
                output = temp;
            }
        }
    }
    return output;
}

int Service::setStartCommand(std::string command)
{
    return setCommand("start", command);
}
int Service::setStopCommand(std::string command)
{
    return setCommand("stop", command);
}
int Service::setStatusCommand(std::string command)
{
    return setCommand("status", command);
}