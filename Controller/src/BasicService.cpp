#include "BasicService.h"
BasicService::BasicService(std::string servicename)
{
    this->servicename = servicename;
}
// TODO 
BasicService::BasicService(const BasicService &service){
    this->commanddata = service.commanddata;
    this->servicename = service.servicename;
};

BasicService::~BasicService(){};


std::string BasicService::getName()
{
    return servicename;
}

std::string BasicService::runCommand(std::string commandtype)
{
    return ExecuteCommand::executeCommand(commanddata[commandtype]);
     
}                            
int BasicService::setCommand(std::string command, std::string commandtype)
{
    commanddata[commandtype] = command;
    return 0;
}