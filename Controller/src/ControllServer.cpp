#include "ControllServer.h"

ControllServer::ControllServer(std::string url)
{
    this->url = url;
};
ControllServer::ControllServer(const ControllServer &a){};
ControllServer::~ControllServer(){};

int ControllServer::updateServiceList()
{
    Slog::log("Getting services ...");
    std::string services = getServices();
    // Contain all services which are currently avaible to match it against allServices
    // to find out if a service needs to get removed
    std::__cxx11::list<std::string> activeServices;
    Slog::log("Got services:" + services);
    Slog::log("Saving services ...");
    std::size_t found = services.find(",");
    int oldvalue = 0;
    // Break down string to add new services
    while (found != std::string::npos)
    {
        std::string service = services.substr(0, found);
        activeServices.push_back(service);
        // Cutting the already found service from the string
        std::string temp = services.substr(found + 1, services.length());
        services = temp;
        oldvalue = found;
        found = services.find(",");

        if (getService(service)->getName() == "NoService")
        {
            Service *serv = new Service(service);
            allServices.insert(Servicemap::value_type(service, serv));
        }
    }
    activeServices.push_back(services);
    if (getService(services)->getName() == "NoService")
    {
        Service *serv = new Service(services);
        allServices.insert(Servicemap::value_type(services, serv));
    }

    Slog::log("Finished saving. Saved " + std::to_string(allServices.size()) + " entrys.");

    Slog::log("Checking for obsolete services");

    Servicemap::iterator it;
    // Speicher abzug Problem allServices als Pointer und services immer neuem allSerives zuweisen
    for (it = allServices.begin(); it != allServices.end(); it++)
    {
        std::string temp = it->first;
        if(activeServices.end() == std::find(activeServices.begin(), activeServices.end(), temp))
        {
            allServices.erase(temp);
        }
    }
}

// Gets all avaible services from the server
int ControllServer::init()
{
    Slog::log("Initialise ControllServer.");

    updateServiceList();

    if (getService("ControllServer")->getName() == "NoService")
    {
        Slog::log("Adding ControllServer as service.");
        Service *controllserver = new Service("ControllServer");
        controllserver->setStartCommand("./controllServer");
        controllserver->setStopCommand("curl " + url + "/service/update/" + controllserver->getName() + "/3");
        controllserver->setStatusCommand("curl " + url + "/service/status/" + controllserver->getName());
        allServices.insert(Servicemap::value_type("ControllServer", controllserver));
        Slog::log("ControllServer was added.");
    }
    else
    {
        Slog::log("Configure ControllServer.");
        Service *controllserver = getService("ControllServer");
        controllserver->setStartCommand("./controllServer");
        controllserver->setStopCommand("curl " + url + "/service/update/" + controllserver->getName() + "/3");
        controllserver->setStatusCommand("curl " + url + "/service/status/" + controllserver->getName());
        Slog::log("Configuration was succesfull.");
    }

    Slog::log("Aviable Services:");
    for (auto &service : allServices)
    {
        Slog::log("Service:" + service.second->getName());
    }
    Slog::log("ControllServer is initialised.");
    return 0;
}

std::string ControllServer::getServices()
{
    return ExecuteCommand::executeCommand("curl " + url + "/service/getServices");
}

int ControllServer::registerService(std::string servicename,std::string serviceAddress)
{
    ExecuteCommand::executeCommand("curl -d \"name="+servicename+"&status=1+&address="+serviceAddress+"\" -X POST " + url + "/service");
}

int ControllServer::start(std::string servicename)
{
    getService(servicename)->start();
    return 0;
}
int ControllServer::stop(std::string servicename)
{
    getService(servicename)->stop();
    return 0;
}
int ControllServer::restart(std::string servicename)
{
    stop(servicename);
    start(servicename);
    return 0;
}
int ControllServer::status(std::string servicename)
{
    for (auto &service : allServices)
    {
        if (service.second->getName() == servicename)
        {
            service.second->getStatus();
        }
    }
    return 0;
}

int ControllServer::startall()
{
    for (auto &service : allServices)
    {
        // service->start();
        start(service.second->getName());
    }
    return 0;
}
int ControllServer::stopall()
{
    for (auto &service : allServices)
    {
        // service->stop();
        stop(service.second->getName());
    }
    return 0;
}
int ControllServer::restartall()
{
    stopall();
    startall();
    return 0;
}
int ControllServer::statusall()
{
    for (auto &service : allServices)
    {
        service.second->getStatus();
    }
    return 0;
}

int ControllServer::updateStatus(std::string servicename, int status)
{
    ExecuteCommand::executeCommand("curl " + url + "/service/update/" + servicename + "/" + std::to_string(status));
    return 0;
}

Service *ControllServer::getService(std::string servicename)
{
    for (auto &service : allServices)
    {
        if (service.second->getName() == servicename)
        {
            return service.second;
        }
    }
    std::cout << "No service " + servicename + " found" << std::endl;
    return new Service("NoService");
}

// This mode let run the programm in an automated mode.
// It will look at the database until the status of a service in the database.
// After that the programm will execute the command which bound with the status.
int ControllServer::startWatchermode()
{
    watchermode = true;
    //getService("ControllServer").start();
    // ExecuteCommand::executeCommand("curl " + url + "/service/" + service->getName());
    // ExecuteCommand::executeCommand("curl -d \"name=ControllServer&status=1\" -X POST " + url + "/service/");
    Slog::log("Starting watchermode");
    while (watchermode)
    {
        sleep(1);

        std::string status;
        for (auto &service : allServices)
        {

            status = ExecuteCommand::executeCommand("curl " + url + "/service/status/" + service.second->getName());
            // Status definitions:
            // 0 Service is shutdown
            // 1 Service is online
            // 2 Service should get started
            // 3 Service should get shutdown
            // 4 Service should get restarted
            int temp = Str2Int::conv(status);
            // std::stringstream ss;
            // ss << status;
            // ss >> temp;
            std::cout << "Status " + status + " " << temp << std::endl;
            switch (temp)
            {

            case 0:
                // No status change needed
                break;
            case 1:
                // No status change needed
                break;
            case 2:

                service.second->start();
                if (isRunning(service.second->getName()) == 1)
                {
                    this->updateStatus(service.second->getName(), 1);
                }

                break;
            case 3:
                if (service.second->getName() != "ControllServer")
                {
                    service.second->stop();
                    if (isRunning(service.second->getName()) == 0)
                    {
                        this->updateStatus(service.second->getName(), 0);
                    }
                }
                else
                {
                    stopWatchermode();
                    this->updateStatus(service.second->getName(), 0);
                }
                break;
            case 4:
                // Do not use the restart method or you will
                // not see if the service got shutdown first
                service.second->stop();
                if (isRunning(service.second->getName()) == 0)
                {
                    this->updateStatus(service.second->getName(), 0);
                }
                service.second->start();
                if (isRunning(service.second->getName()) == 1)
                {
                    this->updateStatus(service.second->getName(), 1);
                }

                break;
            default:
                std::cout << "Status does not exist" << std::endl;
                std::cout << "Please make sure that you run the latest version" << std::endl;
            }
        }
        updateServiceList();
    }
    return 0;
}

int ControllServer::stopWatchermode()
{
    watchermode = false;
    return 0;
}

int ControllServer::isRunning(std::string servicename)
{

    if (getService(servicename)->getName() == "NoService")
    {
        std::cout << "Service " + servicename + " does not exist" << std::endl;
        return 2;
    }
    else if (getService(servicename)->getStatus() == "Running")
    {
        std::cout << "Service " + servicename + " is running" << std::endl;
        return 1;
    }
    else
    {
        return 0;
    }
}