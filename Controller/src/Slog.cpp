#include "Slog.h"
Loglevel Slog::loglevel = Loglevel::INFO;
Slog::Slog()
{
}

Slog::~Slog()
{
}

void Slog::init()
{
    
}

void Slog::setLoglevel(Loglevel level)
{
    loglevel = level;
}
Loglevel Slog::getLoglevel()
{
    return loglevel;
}

void Slog::log(std::string message, Loglevel level, std::string messageLabel)
{

    if (loglevel <= level)
    {
        std::string logmessage;
        
        if(messageLabel.compare("") != 0)
        {
            logmessage="["+messageLabel+"]";
        }
        logmessage=logmessage+ "[" + loglevelToString(level) + "]" + message;
        
        std::cout << logmessage << std::endl;
    }


}

// void Slog::log(std::string message, Loglevel level)
// {
//     if (loglevel <= level)
//     {
//         Serial.println("[" + loglevelToString(level) + "]" + message);
//     }
// }

std::string Slog::loglevelToString(Loglevel loglevel)
{
    switch (loglevel)
    {
    case Loglevel::TEMP:
        return "TEMP";
    case Loglevel::LOWLEVELDEBUG:
        return "LDEBUG";
    case Loglevel::DEBUG:
        return "DEBUG";
    case Loglevel::HIGHLEVELDEBUG:
        return "HDEBUG";
    case Loglevel::INFO:
        return "INFO";
    case Loglevel::DEVELOPMENT:
        return "DEVELOPMENT";
    case Loglevel::WARNING:
        return "WARNING";
    case Loglevel::ERROR:
        return "ERROR";
    case Loglevel::CRITICAL:
        return "CRITICAL";
    case Loglevel::DATA:
        return "DATA";
    default:
        return "UNKNOWN";
    }
}