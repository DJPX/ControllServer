#include <iostream>
#include <stdexcept>
#include <stdio.h>
#include <string>
#include "ExecuteCommand.h"
#include <unordered_map>
#include "BasicService.h"

#pragma once

class Service: public BasicService
{
  public:                             
    Service(std::string servicename,bool isPod=true);
    Service(const Service& a);   
    ~Service();                     

    int start();
    int stop();
    int restart();
    std::string getStatus();
    // Maybe not needed
    std::string getServiceAddress();

    int setStartCommand(std::string command);
    int setStopCommand(std::string command);
    int setStatusCommand(std::string command);

  private:

};