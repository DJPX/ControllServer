#include <iostream>
#include <string>

#pragma once
// TEMP should be only used if there is an output needed which gets removed later again
// LOWLEVELDEBUG should be used for really basic events like writing to a file. 
// DEBUG and HIGHLEVELDEBUG should be used for application specific messages
// DEVELOPMENT is used to get messages for debugging. Its level is high enought to only get the relevant outout
enum class Loglevel { TEMP=-1,LOWLEVELDEBUG,DEBUG, HIGHLEVELDEBUG, INFO,DEVELOPMENT, WARNING, ERROR, CRITICAL, DATA, }; 
/**
 * @brief Simple logger which might get replaced later with a framework.
 * 
 */
class Slog
{
  
public:
  Slog();
  ~Slog();
  static void init();
  /**
   * @brief Set the Loglevel to decide which messages should get printed. Only message with the same or higher importance get printed
   * 
   * @param loglevel The new loglevel. (Minimum loglevel required for messages to get pronted.)
   */
  static void setLoglevel(Loglevel loglevel);
  /**
   * @brief Returns the currents loglevel
   * 
   * @return Current loglevel 
   */
  static Loglevel getLoglevel();
  /**
   * @brief Writes a logmessage to the desired output
   * 
   * @param message Message which will printed
   * @param loglevel The loglevel decides if the message will get printed. (Depends on the current loglevel.)
   * @param messageLabel Label which can used additionaly if the message should have a prefix
   */
  static void log(std::string message,Loglevel loglevel,std::string messageLabel="");
  /**
   * @brief Convertes the loglevel to a string
   * 
   * @param loglevel The loglevel which should get converted
   * @return String of the loglevel
   */
  static std::string loglevelToString(Loglevel loglevel);

private:
  /**
   * @brief The current loglevel which decides, which messages get printed
   * 
   */
  static Loglevel loglevel;
};