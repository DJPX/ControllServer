#include <iostream>
#include <stdexcept>
#include <stdio.h>
#include <string>

#pragma once

class ExecuteCommand
{
public:
  // ExecuteCommand();
  // ExecuteCommand(const ExecuteCommand& a);
  // ~ExecuteCommand();

  static std::string executeCommand(std::string command);
  // static std::string executeCommand(std::string command, bool showOutput);
  // static std::string runD(std::string command);

private:
};