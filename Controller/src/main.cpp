#include <iostream>
#include <stdexcept>
#include <stdio.h>
#include <string>
#include "ControllServer.h"
#include "Service.h"
#include "ExecuteCommand.h"

using namespace std;

int main(int argc, char *argv[])
{

   std::string helpString = "Avaible commands:\nstart -> Starts (all) services | start, start (servicename)\nstop  -> Stops (all) services | stop, stop (servicename)\nrestart -> Restart (all) services | restart, restart (servicename)\nwatchermode -> run as programm to check the services | watchermode\nregister -> Adds a service. | register (servicename) (serviceAddress)";
   if (argc<2 | argc> 3 | argv[1] == nullptr)
   {
      cout << argc << endl;
      cout << "No argument -> nothing to do" << endl;
      cout << helpString << endl;
   }
   else
   {
      ControllServer server("http://localhost:9010");
      server.init();

      if (argc == 2)
      {

         string command = argv[1];

         if (command == "start")
         {
            server.startall();
         }
         else if (command == "stop")
         {
            server.stopall();
         }
         else if (command == "restart")
         {
            server.restartall();
         }
         else if (command == "watchermode")
         {
            cout << "Starting Servercontroll in watcher mode." << endl;
            server.startWatchermode();
         }

         else
         {
            cout << "Command not found" << endl;
            cout << helpString << endl;
         }
      }
      else if (argc == 3)
      {
         string command = argv[1];

         if (command == "start")
         {
            server.start(argv[2]);
         }
         else if (command == "stop")
         {
            server.stop(argv[2]);
         }
         else if (command == "restart")
         {
            server.restart(argv[2]);
         }
         else
         {
            cout << "Command not found" << endl;
            cout << helpString << endl;
         }
      }
      else if (argc == 4)
      {
         string command = argv[1];
         if (command == "register")
         {
            server.registerService(argv[2], argv[3]);
         }
         else
         {
            cout << "Command not found" << endl;
            cout << helpString << endl;
         }
      }
      else
      {
         cout << "Command not found" << endl;
         cout << helpString << endl;
      }
   }

   cout << "Exiting now" << endl;
   return 0;
}