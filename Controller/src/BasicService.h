#include <iostream>
#include <stdexcept>
#include <stdio.h>
#include <string>
#include "ExecuteCommand.h"
#include <unordered_map>

#pragma once
/**
 * @brief This class represents a basic service
 *
 */
class BasicService
{
public:
  /**
   * @brief Constroct a new service
   * 
   * @param servicename name of the service
   */
  BasicService(std::string servicename);
  /**
   * @brief Construct a new Basic Service object
   * 
   * @param service reference to an existing service
   */
  BasicService(const BasicService &service);
  /**
   * @brief Destroy the Basic Service object
   * 
   */
  ~BasicService();
  /**
   * @brief Return the status of the service
   * 
   * @return the status of the server 
   */
  std::string getStatus();
  /**
   * @brief Get the name of the server
   * 
   * @return the name of the service 
   */
  std::string getName();

protected:
  /**
   * @brief Runs a specific command for this service which was added before with setCommand.
   * 
   * @param commandtype The command type is the label which was used saving the command
   * @return std::string 
   */
  std::string runCommand(std::string commandtype);
  /**
   * @brief Allowes to set specific commands for the service
   * 
   * @param command The command which should get executed as string
   * @param commandtype The command type under which label the command gets saved
   * @return int 
   */
  int setCommand(std::string command, std::string commandtype);
  std::string servicename;
  // First string is the commandtype and the secound
  // string is the command which should get execute
  /**
   * @brief The map saves the differenc commands which the service has to offer
   *        The key it the command type and the value is the command
   * 
   */
  std::unordered_map<std::string, std::string> commanddata;
};