// Not used at the moment (using sql light)

const mongoose = require('mongoose')
mongoose.Promise = global.Promise


// Data schema
const dataSchema = new mongoose.Schema({
    created: {
        type: String
    },
    data: {
        type: String
    }
})

module.exports = mongoose.model('data',dataSchema)