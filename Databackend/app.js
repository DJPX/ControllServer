// Containes application configuration
const express = require('express')
const sqlite3 = require('sqlite3')
const http = require('http')
const bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
const routes = require('./Routes/DataRoutes')
app.use('/',routes)


module.exports = app;
