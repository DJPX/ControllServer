const sqlite3 = require('sqlite3').verbose();

// open the database
const path = require('path')
console.log(__dirname)
let db = new sqlite3.Database('Services.db', (err) => {
  if (err) {
    console.error(err.message);
  }
  else {
    console.log('Connected to the services database.');
    //db.run("CREATE TABLE services(serviceid INTEGER PRIMARY KEY,servicename TEXT NOT NULL,status INTEGER NOT NULL)", function(err) { 
    db.run("CREATE TABLE services(name TEXT NOT NULL PRIMARY KEY,status INTEGER NOT NULL,address TEXT NOT NULL);", function (err) {

      if (err) {

        if (err.message = "SQLITE_ERROR: table services already exists") {
          console.log("Database already initialised.");
        }
        else {
          console.log(err.message);
        }
      }
      else {
        console.log("Database initialised.");
      }

    });
  }

});

exports.getServiceAddress = async (req,res) =>
{
  db.serialize(
    db.get('SELECT address FROM services WHERE name = ?', [req.params.name], function (err, row) {
      if (err) {
        res.send("Error encountered while displaying");
        console.error(err.message);
      }
      else {
        if (!row) {
          res.send("Service is not registered")
          console.log("Service is not registered")
        } else {
          res.send(`Status: ${row.status}`)
          console.log("Entry displayed successfully");
        }
      }
    })
  )
}


// Get Data
exports.getServicestatus = async (req, res) => {
  db.serialize(() => {
    db.get('SELECT status FROM services WHERE name = ?', [req.params.name], function (err, row) {
      if (err) {
        res.send("Error encountered while displaying");
        console.error(err.message);
      }
      else {
        if (!row) {
          res.send("Service is not registered")
          console.log("Service is not registered")
        } else {
          res.send(`Status: ${row.status}`)
          console.log("Entry displayed successfully");
        }
      }
    })
  })

};

// exports.getServiceID = async (req, res) => {

//   db.serialize(() => {
//     db.get('SELECT id FROM services WHERE name = ?', [req.params.name], function (err, row) {
//       if (err) {
//         res.send("Error encountered while displaying");
//         console.error(err.message);
//       }
//       else {
//         if (!row) {
//           res.send("Service is not registered")
//           console.log("Service is not registered")
//         } else {
//           res.send(`ID: ${row.id}`)
//           console.log("Entry displayed successfully");
//         }
//       }
//     });
//   });
// }

// // Get all Data
// exports.getAllServices = async (req, res) => {
//   console.log(req.params.id)
//   db.serialize(() => {

//     let data="";
//     let sql = `SELECT name, status FROM services ORDER BY name`;

//     db.each(sql, (err, row,data) => {
//       if (err) {
//         throw err;
//       }
//       data += JSON.stringify(row)
//       console.log(data)
//     });
//     console.log(data)

//   });
// };



exports.getAllServices = async (req, res) => {
  db.serialize(() => {
    let sql = `SELECT name, status FROM services ORDER BY name`;

    db.all(sql, [], (err, rows) => {
      if (err) {
        throw err;
      }
      else if (rows.length != 0) {
        let names = rows[0].name;
        for (i = 1; i < rows.length; i++) {
          names += "," + rows[i].name
        }
        res.send(names)
      }
      else {
        res.send("")
      }


    });

  });
};



// Insert
exports.registerService = async (req, res) => {
  db.serialize(() => {
    db.run('INSERT INTO services(name,status,address) VALUES(?,?,?)', [req.body.name, req.body.status,req.body.address], function (err) {

      if (err) {
        if (err = "SQLITE_CONSTRAINT: UNIQUE constraint failed: services.name") {
          res.send("Service alread exists")
          console.log(err.message)
          return console.log("Service already exists")
        }
        res.send("Could not register the service")
        return console.log(err.message);
      }
      console.log("New Service has been added");
      res.send("New Service has been registered sucessfull into the database with name = " + req.body.name + ".");
    });
  });
};
// Update
exports.changeServiceStatus = async (req, res) => {
  db.serialize(() => {
    console.log(req.params.status + " " + req.params.name)
    db.run('UPDATE services SET status = ? WHERE name = ?', [req.params.status, req.params.name], function (err) {
      if (err) {
        res.send("Error encountered while updating");
        return console.error(err.message);
      }
      res.send("Service updated successfully");
      console.log("Service updated successfully");
    });
  });
};
// Delete
exports.removeService = async (req, res) => {
  db.serialize(() => {
    db.run('DELETE FROM services WHERE name = ?', req.params.name, function (err) {
      if (err) {
        res.send("Error encountered while deleting");
        return console.error(err.message);
      }
      else {
        res.send("Service deleted");
        console.log("Service deleted");
      }
    });
  });
};