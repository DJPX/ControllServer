const express = require('express')
const router = express.Router()


const dataController = require ('../Controllers/DataController')
const servicesController = require ('../Controllers/ServiceController')



router.get('/',dataController.baseRoute)

// TODO Add get service address and add service needs to add the service address too
router.get('/service/address/:name',servicesController.getServiceAddress)
router.post('/service',servicesController.registerService)
router.get('/service/status/:name',servicesController.getServicestatus)
router.get('/service/getServices',servicesController.getAllServices)
router.put('/service/update/:name/:status',servicesController.changeServiceStatus)
router.delete('/service/delete/:name',servicesController.removeService)



module.exports = router