#include "Receiver.h"

#ifdef LEDDEBUG
#elif defined(SERIALDEBUG)
#endif

#ifdef LEDDEBUG
LED *Receiver::led = new LED(1, false);
#endif

String Receiver::macaddress = "";
// Used by the receiver method
OnDataReceive Receiver::onDataReceive;
int unsigned long Receiver::timeout = 0;
EasyGPIO *Receiver::transistor = new EasyGPIO(3, true, true);
data_struct Receiver::lastReceivedData;

Receiver::Receiver()
{
}

Receiver::Receiver(String macaddress, OnDataReceive onDataReceive)
{
  this->macaddress = macaddress;
  Receiver::onDataReceive = onDataReceive;
}
Receiver::~Receiver()
{
  delete led;
}

void Receiver::handleMessageEvent(unsigned long activetime, int newstage)
{
  static int stage = 0;
  if (newstage != -1)
  {
    stage = newstage;
  }
  if (activetime != 0)
  {
    timeout = millis() + activetime;
  }
  if (stage == 0 && millis() < timeout)
  {
#ifdef LEDDEBUG
    led->on();
#elif defined(SERIALDEBUG)
    Serial.print("Stage 0");
#endif

    transistor->on();
    stage = 1;
#ifdef SERIALDEBUG
    Serial.print("Stage 1");
#endif
  }
  else if (stage == 1)
  {
    if (millis() >= timeout)
    {
      stage = 2;
    }
  }
  else if (stage == 2)
  {
#ifdef SERIALDEBUG
    Serial.print("Stage 2");
#elif defined(LEDDEBUG)
    led->off();
#endif
    transistor->off();

    stage = 0;
  }
  // else
  // {
  //   stage = 0;
  // }
}

void Receiver::setup()
{
#ifdef SERIALDEBUG
  Serial.println("Starting Setup");
#endif

  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();

  // Init ESP-NOW
  if (esp_now_init() != 0)
  {
#ifdef SERIALDEBUG
    Serial.println("Error initializing ESP-NOW");
#endif

    return;
  }
#ifdef LEDDEBUG
  led = new LED(1, false);
  led->on();
#endif

  transistor = new EasyGPIO(3, true, true);
  transistor->off();

  // Once ESPNow is successfully Init, we will register for recv CB to
  // get recv packer info
  esp_now_set_self_role(ESP_NOW_ROLE_SLAVE);
  esp_now_register_recv_cb(*onDataReceive);
  delay(2000);
#ifdef LEDDEBUG
  led->off();
#elif defined(SERIALDEBUG)
  Serial.println("Setup finished.");
#endif
}





// #include "Receiver.h"

// #ifdef LEDDEBUG
// #elif defined(SERIALDEBUG)
// #endif

// #ifdef LEDDEBUG
// LED *Receiver::led = new LED(1, false);
// #endif

// String Receiver::macaddress = "";
// // Used by the receiver method
// OnDataReceive Receiver::onDataReceive;
// int unsigned long Receiver::timeout = 0;
// GPIO *Receiver::transistor = new GPIO(3, true, true);
// data_struct Receiver::lastReceivedData;

// Receiver::Receiver()
// {
// }

// Receiver::Receiver(String macaddress, OnDataReceive onDataReceive)
// {
//   this->macaddress = macaddress;
//   Receiver::onDataReceive = onDataReceive;
// }
// Receiver::~Receiver()
// {
//   delete led;
// }

// void Receiver::handleMessageEvent(unsigned long activetime, int newstage)
// {
//   static int stage = 0;
//   if (newstage != -1)
//   {
//     stage = newstage;
//   }
//   if (activetime != 0)
//   {
//     timeout = millis() + activetime;
//   }
//   if (stage == 0 && millis() < timeout)
//   {
// #ifdef LEDDEBUG
//     led->on();
// #elif defined(SERIALDEBUG)
//     Serial.print("Stage 0");
// #endif

//     transistor->on();
//     stage = 1;
// #ifdef SERIALDEBUG
//     Serial.print("Stage 1");
// #endif
//   }
//   else if (stage == 1)
//   {
//     if (millis() >= timeout)
//     {
//       stage = 2;
//     }
//   }
//   else if (stage == 2)
//   {
// #ifdef SERIALDEBUG
//     Serial.print("Stage 2");
// #elif defined(LEDDEBUG)
//     led->off();
// #endif
//     transistor->off();

//     stage = 0;
//   }
//   // else
//   // {
//   //   stage = 0;
//   // }
// }

// void Receiver::setup()
// {
// #ifdef SERIALDEBUG
//   Serial.println("Starting Setup");
// #endif

//   // Set device as a Wi-Fi Station
//   WiFi.mode(WIFI_STA);
//   WiFi.disconnect();

//   // Init ESP-NOW
//   if (esp_now_init() != 0)
//   {
// #ifdef SERIALDEBUG
//     Serial.println("Error initializing ESP-NOW");
// #endif

//     return;
//   }
// #ifdef LEDDEBUG
//   led = new LED(1, false);
//   led->on();
// #endif

//   transistor = new GPIO(3, true, true);
//   transistor->off();

//   // Once ESPNow is successfully Init, we will register for recv CB to
//   // get recv packer info
//   esp_now_set_self_role(ESP_NOW_ROLE_SLAVE);
//   esp_now_register_recv_cb(*onDataReceive);
//   delay(2000);
// #ifdef LEDDEBUG
//   led->off();
// #elif defined(SERIALDEBUG)
//   Serial.println("Setup finished.");
// #endif
// }