#define ESP8266
#ifdef UseESP32
#include <WiFi.h>
#elif defined(ESP8266)
#include <ESP8266WiFi.h>
#endif


class ManageWifi
{
  public:                            
    ManageWifi(const char* ssid,const char* password);                  
    ~ManageWifi();                     
    void setup();
    void start();
    void shutdown();

  private:
    const char* ssid;
    const char* password;
};



