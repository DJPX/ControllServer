#include <WiFi.h>

class Webinterface
{
  public:                            
    Webinterface(const char* ssid, const char* password);                  
    ~Webinterface();                     
    void setup();
    void run();
  private:
    const char* ssid;
    const char* password;
    

    // Variable to store the HTTP request
    String header;

    // Auxiliar variables to store the current state
    String homeserverState = "off";
    String homeserverDataserviceState = "off";


    // Current time
    unsigned long currentTime = millis();
    // Previous time
    unsigned long previousTime = 0; 
    // Define timeout time in milliseconds (example: 2000ms = 2s)
    const long timeoutTime = 2000;
};

