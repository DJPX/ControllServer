#include "ManageWifi.h"


ManageWifi::ManageWifi(const char* ssid,const char* password){
  this->ssid=ssid;
  this->password=password;

};
ManageWifi::~ManageWifi(){         
};  

void ManageWifi::setup() {
    WiFi.mode(WIFI_STA);
}

void ManageWifi::start() {
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }
}

void ManageWifi::shutdown()
{
    WiFi.disconnect();

}