#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <espnow.h>
#include <EasyGPIO.h>
#include <LED.h>
#define SHUTDOWNTIME 3000
#define STARTUPTIME 1000
#define LEDDEBUG
#ifdef LEDDEBUG
#elif defined(SERIALDEBUG)
#endif
#pragma once
typedef void (*OnDataReceive)(uint8_t *mac, uint8_t *incomingData, uint8_t len);
typedef struct data_struct
{
  int mode;
  int value;
} data_struct;
class Receiver
{

public:
  Receiver();
  Receiver(String macaddress, OnDataReceive onDataReceive);
  ~Receiver();
#ifdef LEDDEBUG
  static LED *led;
#endif

  static EasyGPIO *transistor;
  static data_struct lastReceivedData;
  static void setup();
  // Active waiting (Since delay causes trouble) in milisecounds
  void waituntil(long unsigned int milisecounds);
  static void handleMessageEvent(unsigned long activetime = 0, int newstage = -1);

private:
  static String macaddress;
  // Used by the receiver method
  static OnDataReceive onDataReceive;

  static int unsigned long timeout;
};
