#!/bin/bash
# Call this file with . filename or source filename to run it in the current bash

# Create/Activate virtualEnviroment
function activateEnviroment {
  ENVIROMENTNAME="client"
  if [ -d "$ENVIROMENTNAME" ]; then
    source "$ENVIROMENTNAME"/bin/activate
  else
    # Install depenencys
    #sudo apt-get install python3-pip
    # sudo apt-get install scrot
    sudo dnf install scrot
    sudo dnf install python3-pip
    pip3 install --user virtualenv 
    python3 -m virtualenv "$ENVIROMENTNAME"
    source "$ENVIROMENTNAME"/bin/activate
    pip3 install pyserial
    pip3 install requests
    #pip3 install pynput
    # Latest version makes trouble for now it is required to stay on this version
    pip3 install Pillow==9.0.0
    pip3 install pyautogui
    pip3 install pygetwindow
    pip3 install pyinstaller
    pip3 install opencv_python

    # gui depedencys
    pip3 install kivy[full]
    pip3 install kivymd
    # Selenium depenencies
    pip3 install selenium
    pip3 install geckodriver-autoinstaller
    # # Tesseract dependencys https://tesseract-ocr.github.io/tessdoc/Installation.html
    # sudo apt install tesseract-ocr -y
    # sudo apt install libtesseract-dev -y
    # # Image to text / detetion depenencies
    # pip3 install pytesseract
    # pip3 install opencv-python
  fi
}



if [ "$#" -ne 1 ]
then
  echo "No action specified please use start, run or stop as parameter"
 # echo "Please run this script with source filename or . filename"
else
  if [ "$1" == "start" ]
  then
    activateEnviroment
  elif [ "$1" == "stop" ]
  then
    deactivate
  elif [ "$1" == "run" ]
  then
    source Client.sh start
    clear
    python3 test.py
  elif [ "$1" == "remove" ]
  then
    rm -r client
  elif [ "$1" == "reinstall" ]
  then
    Client.sh remove
    source Client.sh start


  fi
fi





