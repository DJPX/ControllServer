# from imghdr import tests
from src.device.human_interface_device import Keyboard, Mouse
from src.tests.test_gui import TestGUI
from src.tests.test_login_to import TestLoginTo
from src.tests.test_instructions import TestInscructions
from webbrowser import BaseBrowser
from src.authentification.login.login_to import LoginWithSeleniumBrowser
from src.communication.communication import WebCommunication



baseurl = "http://localhost:8000"


def testServer():
    # Test index
    params = {'username': 'neueruser'}
    testRequest("", "get", params)

    # Create new User
    params = {'username': 'neueruser',
              'email': 'neueruser@email.com', 'password': 'testpassword'}
    testRequest("register", "post", params)

    # Get User in the database
    params = {'username': 'neueruser'}
    testRequest("getUser", "get", params)

    # Login User correct login
    params = {'username': 'neueruser', 'password': 'testpassword'}
    testRequest("login", "get", params)

    # Update password
    params = {'username': 'neueruser', 'password': 'newpw'}
    testRequest("update", "get", params)

    # Get User in the database
    params = {'username': 'neueruser'}
    testRequest("getUser", "get", params)

    # Login User incorrect login (after the password was changed)
    params = {'username': 'neueruser', 'password': 'testpassword'}
    testRequest("login", "get", params)

    # Delete User
    params = {'username': 'neueruser'}
    testRequest("delete", "delete", params)

    # Get User in the database
    params = {'username': 'neueruser'}
    testRequest("getUser", "get", params)


def testRequest(requestype, method, params):
    url = baseurl+"/"+requestype
    webcom = WebCommunication(url, method)
    response = webcom.send_message(params)
    print(requestype + ":"+response.text)
    print()


def testKeyboard():
    Keyboard.type("Hello World")
    Keyboard.press_return()


def test_passed(testname):
    print("[Test] " + testname + " was passed")


def start_test():
    aviable_tests = {
        # "Instructions": TestInscructions()
        # "LoginToBrowser": TestLoginTo()
        "GUI": TestGUI()
    }
    keys = aviable_tests.keys

    for key, value in aviable_tests.items():
        print(key, value)
        value.test()
        test_passed(key)

start_test()
