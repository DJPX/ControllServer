from communication.communication import WebCommunication
from enum import Enum


class Status(Enum):
    OFFLINE = "0"
    ONLINE = "1"
    PREPARESTART = "2"
    PREPARESHUTDOWN = "3"
    PREPARERESTART = "4"

class Service:
    def __init__(self, name,url):
        self.name = name
        self.url = url

    def start(self):
        request_url = self.url+"/service/update/"+self.name+"/"+Status.PREPARESTART.value
        webcom = WebCommunication(request_url, 'put')
        response = webcom.send_message("")
        return response.text

    def stop(self):
        request_url = self.url+"/service/update/"+self.name+"/"+Status.PREPARESHUTDOWN.value
        webcom = WebCommunication(request_url, 'put')
        response = webcom.send_message("")
        return response.text

    def restart(self):
        request_url = self.url+"/service/update/"+self.name+"/"+Status.PREPARERESTART.value
        webcom = WebCommunication(request_url, 'put')
        response = webcom.send_message("")
        return response.text

    def status(self):
        request_url = self.url+"/service/status/"+self.name
        webcom = WebCommunication(request_url, 'get')
        response = webcom.send_message("")
        return response.text
