#!/bin/bash
addLineIfNotExists(){
  file="$1"
  line="$2"

  # Check if line is already in the file
  if grep -qF -- "$line" "$file"; then
    echo "Line already exists in file."
    exit 0
  else
    echo "Line does not exist in file."
    echo "$line" >> $file
    exit 1
  fi

}


NEXTCLOUDPORT=8020
# Nextcloud
# sudo firewall-cmd --zone=public --permanent --add-port ${NEXTCLOUDPORT}/tcp
# sudo firewall-cmd --reload

# Add user for container
# PASSWORD=$(openssl passwd -crypt 'a!"§TGwdsf2Fw§"f')
PASSWORD=adsf2fqwef3
USERNAME=poduser

# Create user with no password
# sudo useradd -m $USERNAME -p "$PASSWORD"
addLineIfNotExists test.sh "$USERNAME ALL=(ALL) NOPASSWD:ALL"
su -c "Your command right here" -s /bin/sh username

# NEXTCLOUD_HOME="/data/nextcloudTest" && \
# sudo mkdir $NEXTCLOUD_HOME 
# cd $NEXTCLOUD_HOME && \
# sudo mkdir -p html apps config data theme
# sudo chown -c $USERNAME $NEXTCLOUD_HOME
# sudo chown -c $USERNAME *


# # Nextcloud pod 
# sudo -n -u $USERNAME 'podman pod create --name nextcloud_server  \
#   --publish 44320:443 --publish ${NEXTCLOUDPORT}:80 --publish 2210:22 \
#   --publish 54322:5432'


# # Postgres Database for Nextcloud
# cd $NEXTCLOUD_HOME && \
# sudo mkdir database
# sudo chown -c $USERNAME database
# sudo -n -u $USERNAME 'podman run -dt --pod nextcloud_server \
#   --name nextcloud_postgres \
#   -e POSTGRES_PASSWORD=gw94kg9wkpgkkfdkgrewög39 \
#   -e POSTGRES_USER=nextcloud \
#   -e POSTGRES_DB=nextcloud \
#   -v $NEXTCLOUD_HOME/database:/var/lib/postgresql/data:Z \
#   -d postgres'

# #-r -s /bin/false# Nextcloud server
# sudo -n -u $USERNAME 'podman run -dt --pod nextcloud_server \
#   --name nextcloud \
#   --restart always \
#   -v $NEXTCLOUD_HOME/html:/var/www/html:Z \
#   -v $NEXTCLOUD_HOME/apps:/var/www/html/custom_apps:Z \
#   -v $NEXTCLOUD_HOME/config:/var/www/html/config:Z \
#   -v $NEXTCLOUD_HOME/data:/var/www/html/data:Z \
#   -v $NEXTCLOUD_HOME/theme:/var/www/html/themes:Z \
# nextcloud'