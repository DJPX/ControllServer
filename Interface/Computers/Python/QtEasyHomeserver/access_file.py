from enum import Enum

# from cryptography.fernet import Fernet
# class Encryption:
#     # TODO implement other encryption methodes
#     def __init__(self,type=None) -> None:
#         self.coding ='utf_16'
#         pass

#     def set_key(self,key:str):
#         import base64
#         self.crypto = Fernet(base64.b64encode(b'data to be encoded'))

#     def encrypt(self,text:str):  
#         return self.crypto.encrypt(text.encode())


#     def decrpyt(self,text:str):
#         return self.crypto.decrypt(text).decode()
import base64
from cryptography.fernet import Fernet
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC

class Encryption:
    def __init__(self, password):
        self.password = password.encode()
        kdf = PBKDF2HMAC(
            algorithm=hashes.SHA256(),
            length=32,
            salt=b'mysalt',
            iterations=100000,
        )
        self.key = base64.urlsafe_b64encode(kdf.derive(self.password))
    
    def encrypt(self, message):
        encoded_message = message.encode()
        cipher = Fernet(self.key)
        encrypted_message = cipher.encrypt(encoded_message)
        return encrypted_message.decode()
    
    def decrypt(self, encrypted_message):
        encoded_message = encrypted_message.encode()
        cipher = Fernet(self.key)
        decrypted_message = cipher.decrypt(encoded_message)
        return decrypted_message.decode()

class FileMode(Enum):
    APPEND = 0
    OVERWRITE = 1
    CREATEANDOPENFILE = 2
    READ = 3


class AccessFile:
    def __init__(self, path,crypto:Encryption = None) -> None:
        self.crypto = crypto
        self.path = path
        self.file = None
        

    # use modes a = append w = overwite exsiting content x create a file or return error if it exists r for readonly
    def open(self, mode:FileMode):
        match mode:
            case FileMode.APPEND:
                self.file = open(self.path, "a")
            case FileMode.OVERWRITE:
                self.file = open(self.path, "w")
            case FileMode.CREATEANDOPENFILE:
                self.file = open(self.path, "x")
            case FileMode.READ:
                self.file = open(self.path, "r")

    def create_file(self)->bool:
        try:
            self.open(FileMode.CREATEANDOPENFILE)
        except Exception:
            return False
        return True

    def does_file_exist(self):
        import os.path
        return os.path.isfile(self.path)

    # Just appends text to the file
    def write(self,text):
        self.open(FileMode.APPEND)
        if self.crypto != None:
            text = self.crypto.encrypt(text)
        self.file.write(text)
        # self.file.close()

    def read(self):
        self.open(FileMode.READ)
        text=self.file.read()
        if self.crypto != None:
            text = self.crypto.decrypt(text)
        # self.file.close()
        return text
    
    def delete(self):
        import os, errno
        try:
            os.remove(self.path)
        except OSError:
            pass


class TestFilesAndEncryption:
    def test_encryption():
        text = "Password12345678"
        crypto = Encryption(text)
        encrypted = crypto.encrypt("This is my secret text")
        print(encrypted)
        print(crypto.decrypt(encrypted))
        if text == crypto.decrypt(encrypted):
            print("Encryption/Decryption failed")
            return True
        else:
            return False

    def test_files():
        file = AccessFile("testfile.txt")

        if file.does_file_exist():
            print("Error on file exist")
            return False

        file.create_file()
        if not file.does_file_exist():
            print("Error on create file")
            return False
        
        file.write("Some text")
        if file.read() != "Some text":
            print("Error on reading or writing file")
            return False

        file.delete()

        if file.does_file_exist():
            print("Error on delete file")
            return False

        # Test encryption
        crypto = Encryption("MyKey")
        file2 = AccessFile("testfile2.txt",crypto)
        text = 'This is a secret message'
        file2.write(text)

        if file2.read() != text:
            print(file2.read())
            print("File encryption/decryption failed")
            file2.delete()
            return False
        file2.delete()
        print("Test succesfull")
        return True


# TestFilesAndEncryption.test_encryption()
# TestFilesAndEncryption.test_files()


