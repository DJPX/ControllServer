

import sys
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import QApplication, QGridLayout, QMainWindow, QTextEdit, QWidget, QPushButton, QVBoxLayout, QLabel, QLineEdit, QHBoxLayout, QLayout, QComboBox
from screens.service_window import ServiceWindow
from screens.servers_window import ServersWindow
from screens.manage_services import ManageServices




class ButtonWidget(QWidget):
    def __init__(self, texts, parent=None):
        QWidget.__init__(self, parent)
        ...
        # Ausprobieren ob das klappt um buttons zu unterscheiden
        # https://doc.qt.io/qtforpython-5/PySide2/QtCore/QSignalMapper.html







class Login(QtWidgets.QWidget):

    switch_window = QtCore.pyqtSignal()

    def __init__(self):
        QtWidgets.QWidget.__init__(self)
        self.setWindowTitle('Login')

        layout = QtWidgets.QVBoxLayout()
        username_layout, self.username_textinput = UsefullFunctions.create_textfield_with_label(
            layout, "Username")
        layout.addLayout(username_layout)

        password_layout, self.password_textinput = UsefullFunctions.create_textfield_with_label(
            layout, "Passwort")
        layout.addLayout(password_layout)

        self.button = QtWidgets.QPushButton('Login')
        self.button.clicked.connect(self.login)

        layout.addWidget(self.button)

        self.setLayout(layout)

    def login(self):
        # TODO check username and password
        # self.username_textinput
        # self.password_textinput
        self.switch_window.emit()


class Controller:

    def __init__(self):
        self.current_screen = None

    def switch_screen(self, next_screen, function_connect_to_next_screen=None):
        if self.current_screen != None:
            self.current_screen.close()
        self.current_screen = next_screen
        if function_connect_to_next_screen != None:
            self.current_screen.switch_window.connect(
                function_connect_to_next_screen)

        self.current_screen.show()

    def show_login(self):
        self.switch_screen(Login(), self.show_servers)

    def show_servers(self):
        self.switch_screen(ServersWindow(), self.show_manage_services)

    def show_manage_services(self, serverurl):
        self.switch_screen(ManageServices(serverurl), self.show_service)

    def show_service(self, url):
        self.switch_screen(ServiceWindow(url))


class UsefullFunctions():
    def create_textfield_with_label(parent, label: str, vertical=False) -> QLayout:
        textinput = QLineEdit()
        if vertical == True:
            layout = QGridLayout()
            layout.setColumnStretch(2, 1)
            layout.setRowStretch(1, 1)
            layout.addWidget(QLabel(label))
            layout.addWidget(textinput)
        else:
            layout = QVBoxLayout()
            layout.addWidget(QLabel(label))
            layout.addWidget(textinput)

        return layout, textinput


def main():
    app = QtWidgets.QApplication(['Homeserverapp', '--no-sandbox'])
    controller = Controller()
    login_not_required = False
    if login_not_required:
        controller.show_login()
    else:
        controller.show_manage_services("")
    sys.exit(app.exec_())


from access_file import Encryption, AccessFile,TestFilesAndEncryption
if __name__ == '__main__':
    main()
    # TestFilesAndEncryption.test_encryption()
    # TestFilesAndEncryption.test_files()

