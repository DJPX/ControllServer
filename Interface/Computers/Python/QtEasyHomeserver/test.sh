#!/bin/bash

# Check if file name was passed as argument
if [ $# -ne 2 ]; then
  echo "Usage: $0 <file> <line>"
  exit 1
fi

file="$1"
line="$2"

# Check if line is already in the file
if grep -qF -- "$line" "$file"; then
  echo "Line already exists in file."
  exit 0
else
  echo "Line does not exist in file."
  exit 1
fi
poduser ALL=(ALL) NOPASSWD:ALL
