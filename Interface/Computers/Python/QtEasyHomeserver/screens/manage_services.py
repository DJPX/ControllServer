from PyQt5 import QtCore, QtWidgets
from enum import Enum

class ServiceStatus(Enum):
    OFFLINE = 0
    ONLINE = 1

from communication.communication import WebCommunication
from dataclasses import dataclass
@dataclass
class Service:
    def __init__(self, name: str, interface_address: str,service_address: str) -> None:
        self.name = name
        self.interface_address = interface_address
        self.serice_address = service_address
        self.gui_layout = None
    
    def start(self):
        webcom = WebCommunication(self.interface_address+'service/update/'+self.name+'/start','put')
        webcom.send_message(None)
        
        ...
         investieren
         https://www.finanzen.net/index/kospi
         https://www.onvista.de/index/UTX-UKRAINIAN-TRADED-EUR-Index-19296112
    
    def stop(self):
        ...

    def restart(self):
        self.stop()
        while(self.get_status != ServiceStatus.OFFLINE):
            import time
            time.sleep(500)
        self.start()

    def get_status(self)->ServiceStatus:
        webcom = WebCommunication(self.interface_address+'/service/status/'+self.name,'get')
        return webcom.send_message(None)


    def set_gui_layout(self,gui_layout):
        self.gui_layout = gui_layout
    
    def get_service_address(self)->str:
        return self.serice_address

class ManageServices(QtWidgets.QWidget):
    switch_window = QtCore.pyqtSignal(str)
    services_address = {}
    services_layout = {}
    last_clicked_button = ""

    def refreshStatus(self,service_name):
        import urllib.request
        if urllib.request.urlopen(self.services_address[service_name]).getcode() == 200:
            self.services_layout[service_name]

    # def create_service_entry(self, name: str, status: ServiceStatus, service_address=None) -> QtWidgets.QLayout:
    def create_service_entry(self, service:Service) -> QtWidgets.QLayout:
        entry = QtWidgets.QHBoxLayout()
        def delete_entry():
            entry.deleteLater()
        # Delete
        delete_button = QtWidgets.QPushButton("X")
        delete_button.clicked.connect(delete_entry)
        entry.addWidget(delete_button)
        # Label with name
        entry.addWidget(QtWidgets.QLabel(service.name))
        # Status
        
        entry.addWidget(QtWidgets.QLabel(service.get_status()))
        # Start
        start_button = QtWidgets.QPushButton(">")
        start_button.clicked.connect(service.start)
        entry.addWidget(start_button)
        # Stop
        stop_button = QtWidgets.QPushButton("II")
        stop_button.clicked.connect(service.stop)
        entry.addWidget(stop_button)
        # Restart
        restart_button = QtWidgets.QPushButton("<=>")
        entry.addWidget(restart_button)
        restart_button.clicked.connect(service.restart)
        entry.addWidget(restart_button)
        if service.service_address != None:
            # Open service
            open_service_button = QtWidgets.QPushButton("Open")
            open_service_button.clicked.connect(self.switch)
            entry.addWidget(open_service_button)
            self.services_address[service.name] = service.service_address

        # self.services_layout[service.name]= Service(name,service_address)
        return entry

    def __init__(self, text):
        QtWidgets.QWidget.__init__(self)
        self.setWindowTitle('Manage Services')

        layout = QtWidgets.QVBoxLayout()

        # self.label = QtWidgets.QLabel(text)
        # layout.addWidget(self.label)


        # Get all services from controller
        webCom=WebCommunication("192.168.178.70:9010/service/getServices","GET")
        for avaible_service in webCom.send_message(""):
            service_data = Service(service,"192.168.178.70:9010")
            # service_status = webCom.send_messageTo("192.168.178.70:9010/service/status/"+service)



        services_layout = QtWidgets.QVBoxLayout()
        service = self.create_service_entry(
            "File Server", ServiceStatus.ONLINE, "192.168.178.70:8010")
        services_layout.addLayout(service)
        service = self.create_service_entry(
            "Home Server", ServiceStatus.ONLINE, "192.168.178.70:8020")
        services_layout.addLayout(service)

        layout.addLayout(services_layout)

        self.button = QtWidgets.QPushButton('Close')
        self.button.clicked.connect(self.close)

        layout.addWidget(self.button)

        self.setLayout(layout)

    def switch(self,var1):
        # self.eve
        # self.mousePressEvent
        # buttonSender = sender()
        self.switch_window.emit("http://192.168.178.70:8010")


