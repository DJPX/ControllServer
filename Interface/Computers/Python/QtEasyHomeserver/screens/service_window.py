from PyQt5.QtWebEngineWidgets import QWebEngineView, QWebEngineSettings
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import QUrl
class ServiceWindow(QtWidgets.QWidget):
    switch_window = QtCore.pyqtSignal(str)

    def __init__(self, url):
        QtWidgets.QWidget.__init__(self)
        self.setWindowTitle('Service')
        self.browser = QWebEngineView()
        self.browser.settings().setAttribute(
            QWebEngineSettings.WebAttribute.Accelerated2dCanvasEnabled, True)
        self.browser.settings().setAttribute(
            QWebEngineSettings.WebAttribute.AllowRunningInsecureContent, True)
        self.browser.load(QUrl(url))

        layout = QtWidgets.QVBoxLayout()

        layout.addWidget(self.browser)

        self.setLayout(layout)