from PyQt5 import QtCore, QtWidgets

class ServersWindow(QtWidgets.QWidget):

    switch_window = QtCore.pyqtSignal(str)

    def create_server_entry(self):
        server_drop_down = QtWidgets.QComboBox()
        server_drop_down.addItem("192.168.178.70:8010")

    def __init__(self):
        QtWidgets.QWidget.__init__(self)
        self.setWindowTitle('Manage Servers')

        layout = QtWidgets.QGridLayout()

        self.server_drop_down = QtWidgets.QComboBox()
        self.server_drop_down.addItem("192.168.178.70:8010")

        layout.addWidget(self.server_drop_down)

        self.button = QtWidgets.QPushButton('Connect to Server')
        self.button.clicked.connect(self.switch)
        layout.addWidget(self.button)

        self.setLayout(layout)

    def switch(self):
        self.switch_window.emit(self.server_drop_down.itemText(
            self.server_drop_down.currentIndex()))