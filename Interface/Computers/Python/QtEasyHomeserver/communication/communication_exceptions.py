# Called when a mode does not exist (Base class for all Mode not found errors)
class ModeDoesNotExist(Exception):
    pass

class TargetDoesNotExists(Exception):
    pass

# Generall exception when something goes wrong in the authentification
class ErrorOnAuthentification(Exception):
    pass

# Used when there are no instructions or no valid instructions
class NoInstructions(ErrorOnAuthentification):
    pass

# Used for a generic error in the communications classes
class CommunicationError(Exception):
    pass

# Error which happend on the connentien build up
class InitCommunicationError(CommunicationError):
    pass

# Used when it is not posible to open the port
class CouldNotOpenPort(CommunicationError):
    pass

