from enum import Enum

class RequesType(Enum):
    UNKNOWN = 0
    GETALLPASSWORDNAMES = 1,
    # Returns a complete password entry
    PASSWORDDATA = 2,
    USERNAME = 3,
    PASSWORD = 4,
    PASSWORDTYPE = 5,
    PASSWORDTARGET = 6

class MessageType(Enum):
    UNKNOWN = 0
    PING = 1
    INIT = 2
    DATA = 3
    REQUEST = 4
    RESPONSE = 5
    ACK = 6
    ACTIVE = 7
    RESET = 8
    RECONNECT = 9

    def message_typ_to_string(message_typ):
        to_string = {
            MessageType.UNKNOWN:"UNKNOWN",
            MessageType.PING:"PING",
            MessageType.INIT:"INIT",
            MessageType.DATA:"DATA",
            MessageType.REQUEST:"REQUEST",
            MessageType.RESPONSE:"RESPONSE",
            MessageType.ACK:"ACK",
            MessageType.ACTIVE:"ACTIVE",
            MessageType.RESET:"RESET",
            MessageType.RECONNECT:"RECONNECT"
        }
        return to_string.get(message_typ,to_string.get(MessageType.UNKNOWN))

    def string_to_message_typ(string):
        to_message_typ = {
            "UNKNOWN":MessageType.UNKNOWN,
            "PING":MessageType.PING,
            "INIT":MessageType.INIT,
            "DATA":MessageType.DATA,
            "REQUEST":MessageType.REQUEST,
            "RESPONSE":MessageType.RESPONSE,
            "ACK":MessageType.ACK,
            "ACTIVE":MessageType.ACTIVE,
            "RESET":MessageType.RESET,
            "RECONNECT":MessageType.RECONNECT
        }
        return to_message_typ.get(string,to_message_typ.get("UNKNOWN"))