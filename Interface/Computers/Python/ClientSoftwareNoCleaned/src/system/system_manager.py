# TODO Check Windows implementation
import os
import platform
import subprocess
import time
import pyautogui
from src.exceptions.exceptions import ImageOpeningError


class SystemManager:
    # Returns the OS-type
    # postfix = Linux
    # nt = Windows
    def get_OS():
        return os.name()
    # Returns the platform
    # Windows or Linux

    def get_platform():
        return str(platform.system())

    # Returns version of operating system
    # Kernelversion on linux
    # Windows build version (guess)
    def get_version():
        return platform.release()

    def execute_command(command, complex=False):
        if (SystemManager.get_platform() == "Linux"):
            # Executes bash commands which using pipes (multiple programm calls)
            # TODO Check for security flaws. (It may can get abused)
            if(complex == True):
                return subprocess.check_output(command, shell=True, text=True)
            # Safer to use this
            else:
                cmd = command.split()
                result = subprocess.run(
                    cmd, stdout=subprocess.PIPE).stdout.decode('utf-8')
                return result
            # return str(subprocess.call(command,shell=True))
        elif (SystemManager.get_platform() == "Windows"):
            return "not implemented"
        else:
            print("Unknown System:"+SystemManager.get_platform())
            return "Unknown System"

    # Used for images or other files which are in the application directory use the normal path (only starting with directorys which are in on the same level as the main)
    def correctPath(path):
        full_path = os.path.abspath(__file__)
        return full_path[:full_path.rindex("/")] + "/../../" + path

    # This function tries to find an image on the screen
    # The accuracyModifer describes how equal the images must match
    # Default timeout is 10 timeunits (1 timeunit = 1 delay + time for one search)
    def scan_for_image(image_src: str, accuracyModifier=0.5, timeout=10, delay=0.1):
        result = None
        activtime = 0
        while activtime < timeout and result is None:
            result = None
            try:
                result = pyautogui.locateOnScreen(
                    image_src, grayscale=True, confidence=accuracyModifier)
            except OSError:
                print(
                    "Problem with the image occured. Please check if it exists and if the programm has permissions to access it.")
                print(image_src)
                raise ImageOpeningError
                return OSError
            time.sleep(delay)
            activtime = activtime+1
            # print("Time:"+str(activtime))
        return result

    # It is possible to use non integer values too like 2.4
    def delay(in_secounds):
        time.sleep(in_secounds)


# TODO create unknow system exception
class WindowManager:
    def __init__(self):
        if (SystemManager.get_platform() == "Windows"):
            import pygetwindow as wmanager
            self.operatingsystem = "Windows"
        elif (SystemManager.get_platform() == "Linux"):
            self.operatingsystem = "Linux"
        else:
            self.operatingsystem = "Unknown"

    def get_all_windowtitles(self):
        if (self.operatingsystem == "Linux"):
            windowtitles = SystemManager.execute_command(
                'wmctrl -l | grep -o "$HOSTNAME.*" | sed "s/$HOSTNAME //g"', True)
            return windowtitles
        elif (self.operatingsystem == "Windows"):
            # TODO Check if import is nessesary
            import pygetwindow as wmanager
            return wmanager.getAllTitles()
        else:
            print("Unknown System:"+SystemManager.get_platform())

    def get_all_window_classes(self):
        if (self.operatingsystem == "Linux"):
            return SystemManager.execute_command('wmctrl -lx | cut -d \' \' -f4 | sed \'/^[[:space:]]*$/d\'', True)
        elif (self.operatingsystem == "Windows"):
            # TODO Check if windows has types too
            return self.get_all_windowtitles()
        else:
            print("Unknown System:"+SystemManager.get_platform())

    def get_windows_with_name(self, applicationname, getClass=False):
        if (getClass):
            return list(filter(lambda applicationwindow: applicationwindow.find(applicationname) != -1, self.getAllWindowClasses().splitlines()))
        else:
            return list(filter(lambda applicationwindow: applicationwindow.find(applicationname) != -1, self.get_all_windowtitles().splitlines()))


# TODO Linux set window by ID

    def set_active_window(self, applicationname, searchInOpenWindows=False, useClass=False):
        if (self.operatingsystem == "Linux"):
            if (searchInOpenWindows == True):
                applicationnames = self.get_windows_with_name(
                    applicationname, getClass=useClass)
                # Maybe useless since it puts all windows with that name in the foregroudn
                for applicationname in applicationnames:
                    if(useClass):
                        SystemManager.execute_command(
                            "wmctrl -x -a "+applicationname)
                    else:
                        SystemManager.execute_command(
                            "wmctrl -a "+applicationname)
            else:
                if(useClass):
                    print(applicationname)
                    SystemManager.execute_command(
                        "wmctrl -x -a "+applicationname)
                else:
                    SystemManager.execute_command("wmctrl -a "+applicationname)
        elif (self.operatingsystem == "Windows"):
            # win = wmanager.get
            pass
        else:
            print("Unknown System:"+SystemManager.get_platform())

    def close_window(self, applicationname):
        if (self.operatingsystem == "Linux"):
            SystemManager.execute_command("wmctrl -c " + applicationname)


# # TODO Check Windows implementation
# import os
# import platform
# import subprocess
# class SystemManager:
#     # Returns the OS-type
#     # postfix = Linux
#     # nt = Windows
#     def getOS():
#         return os.name()
#     # Returns the platform
#     # Windows or Linux
#     def get_platform():
#         return str(platform.system())

#     # Returns version of operating system
#     # Kernelversion on linux
#     # Windows build version (guess)
#     def getVersion():
#         return platform.release()

#     def execute_command(command,complex=False):
#         if (SystemManager.get_platform() == "Linux"):
#             # Executes bash commands which using pipes (multiple programm calls)
#             # TODO Check for security flaws. (It may can get abused)
#             if( complex==True):
#                 return subprocess.check_output(command, shell=True, text=True)
#             # Safer to use this
#             else:
#                 cmd=command.split()
#                 result = subprocess.run(cmd, stdout=subprocess.PIPE).stdout.decode('utf-8')
#                 return result
#             # return str(subprocess.call(command,shell=True))
#         elif (SystemManager.get_platform() == "Windows"):
#             return "not implemented"
#         else:
#             print("Unknown System:"+SystemManager.get_platform())
#             return "Unknown System"


# # TODO create unknow system exception
# class WindowManager:
#     def __init__(self):
#         if (SystemManager.get_platform() == "Windows"):
#             import pygetwindow as wmanager
#             self.operatingsystem="Windows"
#         elif (SystemManager.get_platform() == "Linux"):
#             self.operatingsystem="Linux"
#         else:
#             self.operatingsystem="Unknown"

#     def get_all_windowtitles(self):
#         if (self.operatingsystem == "Linux"):
#             # wmctrl -lx | cut -d' ' -f4 shows programtype
#             windowtitles = SystemManager.execute_command('wmctrl -l | grep -o "$HOSTNAME.*" | sed "s/$HOSTNAME //g"',True)
#             return windowtitles
#         elif (self.operatingsystem == "Windows"):
#             # TODO Check if import is nessesary
#             import pygetwindow as wmanager
#             return wmanager.get_all_titles()
#         else:
#             print("Unknown System:"+SystemManager.get_platform())


#     def get_window_with_name(self,applicationname):
#         return list(filter(lambda applicationwindow: applicationwindow.find(applicationname) != -1, self.get_all_windowtitles().splitlines()))


# # TODO Linux set window by ID
#     def setActiveWindow(self,applicationname,searchInOpenWindows=False):
#         if (self.operatingsystem == "Linux"):
#             if ( searchInOpenWindows == True):
#                 applicationnames=self.get_window_with_name(applicationname)
#                 # Maybe useless since it puts all windows with that name in the foregroudn
#                 for applicationname in applicationnames:
#                     SystemManager.execute_command("wmctrl -a "+applicationname)
#             else:
#                 SystemManager.execute_command("wmctrl -a "+applicationname)
#         elif (self.operatingsystem == "Windows"):
#             # win = wmanager.get
#             pass
#         else:
#             print("Unknown System:"+SystemManager.get_platform())

#     def closeWindow(self,applicationname):
#         if ( self.operatingsystem == "Linux"):
#             SystemManager.execute_command("wmctrl -c " + applicationname)
