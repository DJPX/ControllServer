from ..authentification.instructions.instructions import Instructions,Instructiontype,Instruction,InstructionLabel
from .testclass import TestClass

class TestInscructions(TestClass):

    def __init__(self):
        super().__init__()
        self.instructionset = Instructions("ChangePassword","This instructiontype is used to change the password on proton mail")
        # Used for the test_execute methode
        self.index=0
        self.add(self.test_create_instruction)
        self.add(self.test_add)
        self.add(self.test_execute)
        self.add(self.test_get)
        self.add(self.test_get_highes_index)

    def test_create_instruction(self):
        normal_instruction = self.instructionset.create_instruction(instruction_typ=Instructiontype.SYSTEM,instruction=Instruction.SLEEP,target_info=5)
        assert normal_instruction[InstructionLabel.INSTRUCTION] == Instruction.SLEEP
        assert normal_instruction[InstructionLabel.TARGET] == ''
        assert normal_instruction[InstructionLabel.INSTRUCTIONTYP] == Instructiontype.SYSTEM
        assert normal_instruction[InstructionLabel.TARGETINFO] == 5
        assert normal_instruction[InstructionLabel.PLACEHOLDER] == ''
        assert normal_instruction[InstructionLabel.EMBEDDEDINSTRUCTION] is None
        
        with_embedded_instruction = self.instructionset.create_instruction(instruction_typ=Instructiontype.BROWSER,target='/html/body/div[4]/dialog/form/div[2]/div[3]/div/div/label[1]/div[2]/div/div[1]/input',instruction=Instruction.SEND_KEYS,target_info=0,placeholder="current_password",embedded_instruction=normal_instruction)
        assert with_embedded_instruction[InstructionLabel.INSTRUCTION] == Instruction.SEND_KEYS
        assert with_embedded_instruction[InstructionLabel.TARGET] == '/html/body/div[4]/dialog/form/div[2]/div[3]/div/div/label[1]/div[2]/div/div[1]/input'
        assert with_embedded_instruction[InstructionLabel.INSTRUCTIONTYP] == Instructiontype.BROWSER
        assert with_embedded_instruction[InstructionLabel.TARGETINFO] == 0
        assert with_embedded_instruction[InstructionLabel.PLACEHOLDER] == 'current_password'
        assert with_embedded_instruction[InstructionLabel.EMBEDDEDINSTRUCTION] is not None

        embedded_instruction = with_embedded_instruction[InstructionLabel.EMBEDDEDINSTRUCTION]

        # Test if embeddet instruction is still valid
        assert embedded_instruction[InstructionLabel.INSTRUCTION] == Instruction.SLEEP
        assert embedded_instruction[InstructionLabel.TARGET] == ''
        assert embedded_instruction[InstructionLabel.INSTRUCTIONTYP] == Instructiontype.SYSTEM
        assert embedded_instruction[InstructionLabel.TARGETINFO] == 5
        assert embedded_instruction[InstructionLabel.PLACEHOLDER] == ''
        assert embedded_instruction[InstructionLabel.EMBEDDEDINSTRUCTION] == None


    def test_add(self):
        self.instructionset.add(instruction_typ=Instructiontype.BROWSER,target='https://account.protonmail.com/u/0/mail/account-password',instruction=Instruction.OPEN)
        self.instructionset.add(instruction_typ=Instructiontype.SYSTEM,target='',instruction=Instruction.SLEEP,target_info=5)
        self.instructionset.add(instruction_typ=Instructiontype.BROWSER,target='/html/body/div[1]/div[3]/div/div/div[2]/main/div/section[2]/div/div[1]/div[2]/button',instruction=Instruction.CLICK,target_info=0)
        self.instructionset.add(instruction_typ=Instructiontype.SYSTEM,target='',instruction=Instruction.SLEEP,target_info=2)
        self.instructionset.add(instruction_typ=Instructiontype.BROWSER,target='/html/body/div[4]/dialog/form/div[2]/div[3]/div/div/label[1]/div[2]/div/div[1]/input',instruction=Instruction.SEND_KEYS,target_info=0,placeholder="current_password")
        self.instructionset.add(instruction_typ=Instructiontype.BROWSER,target='/html/body/div[4]/dialog/form/div[2]/div[3]/div/div/label[2]/div[2]/div/div[1]/input',instruction=Instruction.SEND_KEYS,target_info=0,placeholder="new_password")
        self.instructionset.add(instruction_typ=Instructiontype.BROWSER,target='/html/body/div[4]/dialog/form/div[2]/div[3]/div/div/label[3]/div[2]/div/div[1]/input',instruction=Instruction.SEND_KEYS,target_info=0,placeholder="new_password")
        self.instructionset.add(instruction_typ=Instructiontype.BROWSER,target='/html/body/div[4]/dialog/form/div[3]/button[2]',instruction=Instruction.CLICK,target_info=0)

    # TODO methode not implemented at the moment so it is not possible to test it
    # def test_remove(self):
    #     test_value = self.instructionset.get(3)
    #     self.instructionset.remove(6)
    #     try:
    #         testvaule2 = self.instructionset.get(3)
    #     except KeyError:
    #         # Key was deleted
    #         pass

    def print_instructions(self,instruction):
        # print(self.index)
        print("Instruction: "+ str(instruction[InstructionLabel.INSTRUCTION]))
        print("Target: " + str(instruction[InstructionLabel.TARGET]))
        print("Instructiontype: "+ str(instruction[InstructionLabel.INSTRUCTIONTYP]))
        print("Placeholder: "+ str(instruction[InstructionLabel.PLACEHOLDER]))
        print("Targetinfo: "+ str(instruction[InstructionLabel.TARGETINFO]))
        
        self.index +=1


    def test_execute(self):
        # Just use the print_instructions methodes as example and use it on every entry
        self.instructionset.execute(self.print_instructions)


    def test_get(self):
        # Test if with all values filled
        instruction = self.instructionset.get(4)
        assert instruction[InstructionLabel.INSTRUCTION] == Instruction.SEND_KEYS
        assert instruction[InstructionLabel.TARGET] == '/html/body/div[4]/dialog/form/div[2]/div[3]/div/div/label[1]/div[2]/div/div[1]/input'
        assert instruction[InstructionLabel.INSTRUCTIONTYP] == Instructiontype.BROWSER
        assert instruction[InstructionLabel.TARGETINFO] == 0
        assert instruction[InstructionLabel.PLACEHOLDER] == 'current_password'
        assert instruction[InstructionLabel.EMBEDDEDINSTRUCTION] is None

        # Test with only some values are filled
        instruction = self.instructionset.get(1)
        assert instruction[InstructionLabel.INSTRUCTION] == Instruction.SLEEP
        assert instruction[InstructionLabel.TARGET] == ''
        assert instruction[InstructionLabel.INSTRUCTIONTYP] == Instructiontype.SYSTEM
        assert instruction[InstructionLabel.TARGETINFO] == 5
        assert instruction[InstructionLabel.PLACEHOLDER] == ''
        assert instruction[InstructionLabel.EMBEDDEDINSTRUCTION] is None

    def test_get_highes_index(self):
        assert self.instructionset.get_instruction_count() == 8





