import time
from ..authentification.instructions.instructions import Instructions
from ..authentification.login.login_to import LoginWithSeleniumBrowser
from .testclass import TestClass
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException

class TestLoginTo(TestClass):

    def __init__(self):
        super().__init__()
        self.login_browser = LoginWithSeleniumBrowser("proton")
        self.add(self.test_login)

    def test_login(self):
        self.login_browser.login()
        # Wait for the webside to load
        time.sleep(20)
        
        try:
            assert len(self.login_browser.get_driver().find_elements(By.XPATH,"/html/body/div[1]/div[3]/div[3]/header/div[3]/ul/li[5]/button")) > 0
        except NoSuchElementException:
            assert False






