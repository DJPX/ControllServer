from src.gui.loginapp import LoginWrapper
from .testclass import TestClass
from ..authentification.instructions.instructions import Instructions,Instructiontype,Instruction,InstructionLabel
from ..authentification.instructions.instruction_processor import InstructionProcessor
from ..system.system_manager import SystemManager
import threading

# TODO use the Simulated passwordmanager for the GUI test or create to diffrend modes to test them
class TestGUI(TestClass):

    passwordmanager_password = "pass"

    def __init__(self):
        super().__init__()
        self.add(self.prepare_test)

    def prepare_test(self):
        # Contains the part where the image matching tests starts
        test_gui_thread =threading.Thread(target=TestGUI.test_gui)
        test_gui_thread.start()
        self.start_application()

        


    def test_gui():
        timeout=5
        accuracy=0.9
        SystemManager.delay(10)
        result = None
        # Test if application did start
        for x in range(5):
            result = SystemManager.scan_for_image(SystemManager.correctPath( 'images/test/gui/Passwordmanager_GUI.png'),accuracyModifier=accuracy,timeout=timeout,delay=0.1)
            if ( result is not None):
                break
            else:
                SystemManager.delay(10)
        assert(result is not None)

        # Test if the login succesful failed
        for x in range(5):
            result = SystemManager.scan_for_image(SystemManager.correctPath('images/test/gui/Passwordmanager_GUI_Login_Failed.png'),accuracyModifier=accuracy,timeout=timeout,delay=0.1)
            if ( result is not None):
                break
            else:
                SystemManager.delay(10)
        assert(result is not None)

        # Test if login was successfull
        for x in range(5):
            result = SystemManager.scan_for_image(SystemManager.correctPath('images/test/gui/Passwordmanager_GUI_Login_Successful.png'),accuracyModifier=accuracy,timeout=timeout,delay=0.1)
            if ( result is not None):
                break
            else:
                SystemManager.delay(10)
        assert(result is not None)



    def start_application(self):
        # Start gui

        # LoginWrapper.start_app()

        

        
    
        # Use the instructions to simulate a user
        # Use the simulated passwordmanager to test the gui
        self.instructionset = Instructions("Passwordmanager GUI Test","This instructionset is used to test the GUI of the passwordmanager.")
        # # Wait until the Gui is ready 
        # self.instructionset.add(instruction_typ=Instructiontype.SYSTEM,target='',instruction=Instruction.SLEEP,target_info=20)
        # Find the passwordfield and enter wrong password
        self.instructionset.add(instruction_typ=Instructiontype.APP,instruction=Instruction.FIND_IMAGE,target_info='A:0.5,D:0.1,T:10.0',target=SystemManager.correctPath("images/test/gui/Passwordmanager_GUI_Passwordfield.png"),embedded_instruction=Instructions.create_instruction(instruction=Instruction.SEND_KEYS,instruction_typ=Instructiontype.APP,target_info="Wrong Password"))
        # Wait so that the 
        self.instructionset.add(instruction_typ=Instructiontype.SYSTEM,target='',instruction=Instruction.SLEEP,target_info=20)
        # Locate the close button after wrong login
        self.instructionset.add(instruction_typ=Instructiontype.APP,instruction=Instruction.FIND_IMAGE,target_info='A:0.5,D:0.1,T:10.0',target=SystemManager.correctPath("images/test/gui/Passwordmanager_GUI_Dialoge_Close.png"),embedded_instruction=Instructions.create_instruction(instruction=Instruction.CLICK,instruction_typ=Instructiontype.APP))



        # Find passwordfield and enter correct password
        self.instructionset.add(instruction_typ=Instructiontype.APP,instruction=Instruction.FIND_IMAGE,target_info='A:0.5,D:0.1,T:10.0',target=SystemManager.correctPath("images/test/gui/Passwordmanager_GUI_Passwordfield.png"),embedded_instruction=Instructions.create_instruction(instruction=Instruction.SEND_KEYS,instruction_typ=Instructiontype.APP,target_info=TestGUI.passwordmanager_password))
        # Find login button and click on it
        self.instructionset.add(instruction_typ=Instructiontype.APP,instruction=Instruction.FIND_IMAGE,target_info='A:0.5,D:0.1,T:10.0',target=SystemManager.correctPath("images/test/gui/Passwordmanager_GUI_Loginbutton.png"),embedded_instruction=Instructions.create_instruction(instruction=Instruction.CLICK,instruction_typ=Instructiontype.APP))
        
        # Locate close button after login and click on it
        self.instructionset.add(instruction_typ=Instructiontype.APP,instruction=Instruction.FIND_IMAGE,target_info='A:0.5,D:0.1,T:10.0',target=SystemManager.correctPath("images/test/gui/Passwordmanager_GUI_Dialoge_Close.png"),embedded_instruction=Instructions.create_instruction(instruction=Instruction.CLICK,instruction_typ=Instructiontype.APP))
        
        # Get Proton Login (webapplication) and login in to it
        
        # Get steam client (desktop application) and login in to it


        InstructionProcessor.process_instructionset(self.instructionset)
        app_thread =threading.Thread(target=LoginWrapper.start_app)
        app_thread.start()



        # self.instructionset.add(instruction_typ=Instructiontype.BROWSER,target='https://account.protonmail.com/u/0/mail/account-password',instruction=Instruction.OPEN)
        # self.instructionset.add(instruction_typ=Instructiontype.BROWSER,target='/html/body/div[1]/div[3]/div/div/div[2]/main/div/section[2]/div/div[1]/div[2]/button',instruction=Instruction.CLICK,target_info=0)
        # self.instructionset.add(instruction_typ=Instructiontype.SYSTEM,target='',instruction=Instruction.SLEEP,target_info=2)
        # self.instructionset.add(instruction_typ=Instructiontype.BROWSER,target='/html/body/div[4]/dialog/form/div[2]/div[3]/div/div/label[1]/div[2]/div/div[1]/input',instruction=Instruction.SEND_KEYS,target_info=0,placeholder="current_password")
        # self.instructionset.add(instruction_typ=Instructiontype.BROWSER,target='/html/body/div[4]/dialog/form/div[2]/div[3]/div/div/label[2]/div[2]/div/div[1]/input',instruction=Instruction.SEND_KEYS,target_info=0,placeholder="new_password")
        # self.instructionset.add(instruction_typ=Instructiontype.BROWSER,target='/html/body/div[4]/dialog/form/div[2]/div[3]/div/div/label[3]/div[2]/div/div[1]/input',instruction=Instruction.SEND_KEYS,target_info=0,placeholder="new_password")
        # self.instructionset.add(instruction_typ=Instructiontype.BROWSER,target='/html/body/div[4]/dialog/form/div[3]/button[2]',instruction=Instruction.CLICK,target_info=0)
        #     oldPos=pyautogui.position()
        # pyautogui.click(inputfield.left + inputfield.width/2, inputfield.top + inputfield.height/2)
        # Keyboard.type(text)
        # pyautogui.moveTo(oldPos)


    



        

