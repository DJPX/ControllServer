# Called when a mode does not exist (Base class for all Mode not found errors)
class ModeDoesNotExist(Exception):
    pass

# Used when the mode is not regoniced in the EnterBrowser class 
class EnterPasswordModeDoesNotExists(ModeDoesNotExist):
    pass

class TargetDoesNotExists(Exception):
    pass

class EnterPasswordTargetDoesNotExists(TargetDoesNotExists):
    pass

# Generall exception when something goes wrong in the authentification
class ErrorOnAuthentification(Exception):
    pass

# Used when there are no instructions or no valid instructions
class NoInstructions(ErrorOnAuthentification):
    pass

# Called when a there is a problem with matching or executing a picture match
class ImageMatchingError(Exception):
    pass

# Can not access image or it does not exists
class ImageOpeningError(ImageMatchingError):
    pass

# Used for a generic error in the communications classes
class CommunicationError(Exception):
    pass

# Error which happend on the connentien build up
class InitCommunicationError(CommunicationError):
    pass

# Used when it is not posible to open the port
class CouldNotOpenPort(CommunicationError):
    pass

