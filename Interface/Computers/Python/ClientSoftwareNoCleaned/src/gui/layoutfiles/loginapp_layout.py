
login_screen = """
Screen:
    TextField:
        password:True
        hint_text: "Enter passpharse"
        helper_text: "To unlock your device"
        helper_text_mode: "on_focus"
        pos_hint: {'center_x':0.5,'center_y':0.5}
        size_hint_x:None
        width:300
        icon_left: "lock"
        icon_left_color: app.theme_cls.primary_color
    MDRectangleFlatButton
        text: "Login"
        pos_hint: {'center_x':0.5,'center_y':0.4}
        on_release: app.check_login
"""
# Screen:
#     MDTextField:
#         password:True
#         hint_text: "Enter passpharse"
#         helper_text: "To unlock your device"
#         helper_text_mode: "on_focus"
#         pos_hint: {'center_x':0.5,'center_y':0.5}
#         size_hint_x:None
#         width:300
#         icon_right: "lock"
#         icon_right_color: app.theme_cls.primary_color
#     MDRectangleFlatButton
#         text: "Login"
#         pos_hint: {'center_x':0.5,'center_y':0.4}
#         on_release: app.check_login
# """

screen_helper = """

ScreenManager:
    ConnectScreen:
    LoginScreen:
    PasswordsScreen:
    OptionsScreen:

<ConnectScreen>:
    name: 'connect'
    Label:
        id: connecting
        text:"Connecting"
        markup:True
        size_hint_x:None

<LoginScreen>:
    name: 'login'
    MDTextField:
        id: passphrase
        password: True
        hint_text: "Enter passpharse"
        helper_text: "To unlock your device"
        helper_text_mode: "on_focus"
        pos_hint: {'center_x':0.5,'center_y':0.5}
        size_hint_x:None
        width:300
        icon_right: "lock"
        icon_right_color: app.theme_cls.primary_color
    MDRectangleFlatButton
        text: "Login"
        pos_hint: {'center_x':0.5,'center_y':0.4}
        on_release: root.check_login()


<PasswordsScreen>:
    name: 'passwords'

    MDNavigationLayout:
        MDNavigationDrawer:
            id: nav_drawer
            BoxLayout:
                orientation: 'vertical'




    # Contains password page
    ScrollView:
        size: self.size
        GridLayout:
            id: passwordContainer
            cols:1
            row_force_default:True
            row_default_height:40
            size_hint_y: None
            height: self.minimum_height

            BoxLayout:
                orientation: 'horizontal'
                spacing:10
                Label:
                    id: labelPasswordname
                    text:"     Passwordname"
                    markup:True
                    size_hint_x:None
                Label:
                    id: labelPasswordtype
                    # text:"Passwordtype"
                    markup:True
                    size_hint_x: None
                Label:
                    id: labelEnterUsername
                    text:"Enter username"
                    markup:True
                    size_hint_x: None
                # Label:
                #     id: labelLogin
                #     text:"Start login"
                #     markup:True
                #     size_hint_x:None
                # Label:
                #     id: labelEditLogin
                #     text:"Edit login"
                #     markup:True
                #     size_hint_x:None
                # Label:
                #     id: labelUpdatePassword
                #     text:"Update password"
                #     markup:True
                #     size_hint_x:None
                Label:
                    size_hint_x:5

<OptionsScreen>:
    name: 'options'
    MDLabel:
        text: 'Upload some files'
        halign: 'center'
    MDRectangleFlatButton:
        text: 'Back'
        pos_hint: {'center_x':0.5,'center_y':0.2}
        on_press: root.manager.current = 'menu'
"""


navbar = """
MDNavigationLayout:
    ScreenManager:
        Screen:
            BoxLayout:
                orientation: 'vertical'
                MDToolbar:
                    title: 'Demo Application'
                    left_action_items: [["menu",lambda x: nav_drawer.set_state('toggle')]]
                    elevation: 20
                Widget:
        MDNavigationDrawer:
            id: nav_drawer
            BoxLayout:
                orientation: 'vertical'
"""


# navbar="""
# Screen:
#     MDNavigationLayout:
#         ScreenManager:
#             Screen:
#                 BoxLayout:
#                     orientation: 'vertical'
#                     MDToolbar:
#                         title: 'Demo Application'
#                         left_action_items: [["menu",lambda x: nav_drawer.set_state('toggle')]]
#                         elevation: 20
#                     Widget:
#         MDNavigationDrawer:
#             id: nav_drawer
#             BoxLayout:
#                 orientation: 'vertical'
#                 Image:
#                     source: 'avatar.png'

#                 MDLabel:
#                     text: '  DJPX'
#                     font_style: 'Subtitle1'
#                     size_hint_y: None
#                     height: self.texture_size[1]
#                 MDLabel:
#                     text: '   djpx@email.com'
#                     font_style: 'Caption'
#                     size_hint_y: None
#                     height: self.texture_size[1]
#                 ScrollView:
#                     MDList:
#                         OneLineIconListItem:
#                             text: 'Profile'
#                             IconLeftWidget:
#                                 icon: 'face-profile-woman'
#                         OneLineIconListItem:
#                             text: 'Upload'
#                             IconLeftWidget:
#                                 icon: 'file-upload'
#                         OneLineIconListItem:
#                             text: 'Logout'
#                             IconLeftWidget:
#                                 icon: 'logout'
# """