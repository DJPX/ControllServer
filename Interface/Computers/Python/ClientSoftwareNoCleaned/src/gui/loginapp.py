# from msilib.schema import Icon
import imp
from tokenize import String
from kivymd.app import MDApp
from kivy.uix.screenmanager import Screen, ScreenManager
from kivy.lang import Builder
# from kivy.core.window import Windowfrom
from kivymd.uix.dialog import MDDialog
from kivymd.uix.button import MDFlatButton
# Can not impot file since it does not reconize the folder
# from layoutfiles.loginapp_layout import screen_helper
# Using variable instead
screen_helper = """

ScreenManager:
    ConnectScreen:
    LoginScreen:
    PasswordsScreen:
    OptionsScreen:

<ConnectScreen>:
    name: 'connect'
    MDRectangleFlatButton
        id: connectButton
        text: "Connect"
        pos_hint: {'center_x':0.5,'center_y':0.5}
        on_release: root.connect_device()
    


<LoginScreen>:
    name: 'login'
    MDTextField:
        id: passphrase
        password: True
        hint_text: "Enter passpharse"
        helper_text: "To unlock your device"
        helper_text_mode: "on_focus"
        pos_hint: {'center_x':0.5,'center_y':0.5}
        size_hint_x:None
        width:300
        icon_right: "lock"
        icon_right_color: app.theme_cls.primary_color
    MDRectangleFlatButton
        text: "Login"
        pos_hint: {'center_x':0.5,'center_y':0.4}
        on_release: root.check_login()


<PasswordsScreen>:
    name: 'passwords'
    MDNavigationLayout:
        MDNavigationDrawer:
            id: nav_drawer
            BoxLayout:
                orientation: 'vertical'




    # Contains password page
    ScrollView:
        size: self.size
        GridLayout:
            id: passwordContainer
            cols:1
            row_force_default:True
            row_default_height:40
            size_hint_y: None
            height: self.minimum_height

            BoxLayout:
                orientation: 'horizontal'
                spacing:10
                Label:
                    id: labelPasswordname
                    text:"     Passwordname"
                    markup:True
                    size_hint_x:None
                Label:
                    id: labelPasswordtype
                    # text:"Passwordtype"
                    markup:True
                    size_hint_x: None
                Label:
                    id: labelEnterUsername
                    text:"Enter username"
                    markup:True
                    size_hint_x: None
                # Label:
                #     id: labelLogin
                #     text:"Start login"
                #     markup:True
                #     size_hint_x:None
                # Label:
                #     id: labelEditLogin
                #     text:"Edit login"
                #     markup:True
                #     size_hint_x:None
                # Label:
                #     id: labelUpdatePassword
                #     text:"Update password"
                #     markup:True
                #     size_hint_x:None
                Label:
                    size_hint_x:5

<OptionsScreen>:
    name: 'options'
    MDLabel:
        text: 'Upload some files'
        halign: 'center'
    MDRectangleFlatButton:
        text: 'Back'
        pos_hint: {'center_x':0.5,'center_y':0.2}
        on_press: root.manager.current = 'menu'
"""


from .screens.login import LoginScreen
from .screens.passwords import PasswordsScreen
from .screens.options import OptionsScreen
from .screens.connect import ConnectScreen

# TODO Create password entry widget class



    



class LoginApp(MDApp):
    def build(self):
        self.load_appstyle()
        # self.theme_cls.primary_palette = 'Red'
        screen = Builder.load_string(screen_helper)
        return screen

    #Lamda function defined in screen_helper string
    def navigation_draw(self):
        print("Navigation")

    def load_appstyle(self):
        self.theme_cls.primary_palette='Green'
        self.theme_cls.primary_hue = "A700"
        self.theme_cls.theme_style='Dark'

    def set_shared_message_storage(self,shared_message_storage):
        self.shared_message_storage = shared_message_storage

    # does not work
    #  def change_screen(self,name):
    #     # LoginWrapper.get_screenmanager().current = 'passwords'
    #     LoginWrapper.get_screenmanager().current = name

from ..communication.passwordmanager_communication import PasswordmanagerProtocol
from multiprocessing import Process, Manager
class LoginWrapper:
    manager = Manager()
    shared_message_storage = PasswordmanagerProtocol.shared_message_storage
    app = LoginApp()
    screenmanager = ScreenManager()

    def start_app():

        # screenManager = LoginWrapper.get_screenmanager()
        # screenManager.add_widget(ConnectScreen(name='connect'))
        # screenManager.add_widget(LoginScreen(name='login'))
        # screenManager.add_widget(PasswordsScreen(name='passwords'))
        # screenManager.add_widget(OptionsScreen(name='options'))
        LoginWrapper.app.set_shared_message_storage(LoginWrapper.shared_message_storage)
        LoginWrapper.app.run()

    # def start_message_listener():
    #     message_listener = PasswordmanagerCommunication.


        

    def get_app():
        return LoginWrapper.app

    def get_screenmanager():
        return LoginWrapper.screenmanager


