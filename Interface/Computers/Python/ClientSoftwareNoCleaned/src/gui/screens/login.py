
from kivy.uix.screenmanager import Screen
from kivymd.uix.button import MDFlatButton
from kivymd.uix.dialog import MDDialog
from src.device.passwordmanager.passwordmanager import Passwordmanager
# From connect screen
from ...communication.passwordmanager_communication import PasswordmanagerCommunication,ConnectionType
class LoginScreen(Screen):

    def __init__(self, **kw):
        super().__init__(**kw)
        self.login = False

    def check_login(self,obj="None"):
        passphrase = self.ids.passphrase
        if (passphrase.text == ""):
            self.login = False
            check_string = 'Please enter the passphrase'
        else:
            # # Do the job of the connect screen
            # if PasswordmanagerCommunication.init(mode=ConnectionType.SERIAL) == True:
                # if (Passwordmanager.unlock(passphrase.text)):
                #     check_string = 'Device was unlocked successful.'
                #     self.login = True
                # else:
                #     check_string = 'Unlocking failed. Wrong password!'
                #     self.login = False
            # else:
            #     closeButton = MDFlatButton(text='Close',on_release=self.close_dialog)
            #     self.dialog = MDDialog(title='Connection',text="Connection failed. Please check the connection of your devices.",size_hint=(0.7,1)
            #     ,buttons=[closeButton])
            #     self.dialog.open()



            if (Passwordmanager.unlock(passphrase.text)):
                check_string = 'Device was unlocked successful.'
                self.login = True
            else:
                check_string = 'Unlocking failed. Wrong password!'
                self.login = False

        closeButton = MDFlatButton(text='Close',on_release=self.close_dialog)
        self.dialog = MDDialog(title='Login',text=check_string,size_hint=(0.7,1)
        ,buttons=[closeButton])
        self.dialog.open()

    def close_dialog(self,obj):
        self.dialog.dismiss()
        if (self.login is True):
            self.parent.current = 'passwords'