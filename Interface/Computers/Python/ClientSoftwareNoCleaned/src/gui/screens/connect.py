
from kivy.uix.screenmanager import Screen
from kivymd.uix.button import MDFlatButton
from kivymd.uix.dialog import MDDialog
from ...communication.passwordmanager_communication import PasswordmanagerCommunication,ConnectionType

class ConnectScreen(Screen):

    def __init__(self, **kw):
        super().__init__(**kw)

    def connect_device(self,obj="None"):
        # TODO Change label from Connect to Connecting
        # self.parent.get_running_app().current.ids["connectButton"].text = "Connecting"
        

        if PasswordmanagerCommunication.init(mode=ConnectionType.SERIAL) == True:
            self.parent.current = 'login'
        else:
            closeButton = MDFlatButton(text='Close',on_release=self.close_dialog)
            self.dialog = MDDialog(title='Connection',text="Connection failed. Please check the connection of your devices.",size_hint=(0.7,1)
            ,buttons=[closeButton])
            self.dialog.open()
            # self.parent.get_running_app().current.ids["connectButton"].text = "Connect"

    def close_dialog(self,obj):
        self.dialog.dismiss()