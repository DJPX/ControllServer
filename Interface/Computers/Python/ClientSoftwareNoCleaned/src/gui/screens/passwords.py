from ast import Pass
from tokenize import String
from kivy.uix.screenmanager import Screen
from kivymd.uix.dialog import MDDialog
from kivymd.uix.button import MDFlatButton
from kivymd.uix.selectioncontrol import MDSwitch
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.popup import Popup
from kivy.uix.textinput import TextInput
from src.authentification.login.login_to import LoginToApplication,LoginWithSeleniumBrowser
from src.authentification.update_password.update_password import UpdatePassword
from src.communication.passwordmanager_communication import PasswordmanagerCommunication

from src.device.passwordmanager.passwordmanager import Passwordmanager, LoginData

class PasswordsScreen(Screen):

    def __init__(self, **kw):
        super().__init__(**kw)
        self.passwordEntrys = {}

    # Load passwordnames form the device
    def load_passwords(self):
        passwordnames = Passwordmanager.get_all_passwordnames()
        if(passwordnames != None):
            if passwordnames != "":
                passwordnames = passwordnames.split(':')
                for passwordname in passwordnames:
                    self.create_and_append_password_entry(passwordname)
        else:
            print("Something went wrong or there are no passwords")
            # TODO error handling


    
    def on_enter(self):
        self.load_passwords()
        layout = FloatLayout(size_hint_y=None)
        layout.add_widget(Button(text="Add new password",size_hint_y=None,pos_hint={'center_x':0.5,'center_y':0.5},on_release=self.create_password_entry_dialog))
        self.add_widget(layout)

    
    def close_dialog(self,obj):
        # Search in the parents for a dialog element
        currentDialog=obj.parent
        while (str(type(currentDialog)).find("kivymd.uix.dialog.MDDialog") == -1):
            currentDialog = currentDialog.parent
        # Close the dialog
        currentDialog.dismiss()
        self.remove_widget(currentDialog)


    def create_and_append_password_entry(self,passwordname):
        
        self.ids[passwordname]= self.create_password_entry(passwordname)
        self.ids.passwordContainer.add_widget(self.ids[passwordname])



    def create_password_entry(self,passwordname):
        layout = BoxLayout(orientation= 'horizontal',spacing=10)
        # Save IDs of buttons
        passwordentry= {
            "Layout":layout,
            "Passwordname":Label(text=passwordname,markup=True,size_hint_x=None),
            "Spacer":Label(size_hint_x=5),
            "EnterUsername":MDSwitch(pos_hint= {'center_x': .5, 'center_y': .5},size_hint_x=None),
            "HiddenInformations": Label(text="Passwordname:"+passwordname,opacity=0,size_hint_x=None),
            "Login": Button(text="Login",size_hint_x=None,on_release=self.login),
            "Edit": Button(text="Edit",size_hint_x=None,on_release=self.edit_password_entry),
            "Update": Button(text="Update",size_hint_x=None,on_release=self.update_dialog),
            "Delete": Button(text="Delete",size_hint_x=None,on_release=self.delete_dialog)
            # "Spacer3":Label(size_hint_x=5)
        }
        # Add buttons to the screen
        for key in passwordentry:
            if (key != "Layout"):
                layout.add_widget(passwordentry[key])
        self.passwordEntrys[passwordname]=passwordentry
        return layout

    def find_parent_type_of_widget(self,parentType,widget):
        searchedParent=widget.parent
        while (str(type(searchedParent)).find(parentType) == -1):
            searchedParent = searchedParent.parent
        return searchedParent

    def login(self,obj):
        for entry in obj.parent.children:
            if(str(type(entry)).find("kivy.uix.label.Label") != -1):
                if(entry.text.find("Passwordname:") != -1):
                    passwordname= entry.text[entry.text.find(":")+1:]
        enterUsername = self.passwordEntrys[passwordname]
        enterUsername = enterUsername["EnterUsername"]

        weblogin_login =LoginWithSeleniumBrowser(passwordname=passwordname) #url=login_data.target,mode=1,password=login_data.password,username=login_data.username)
        weblogin_login.login()


    def update_dialog(self,obj):
        for entry in obj.parent.children:
            if(str(type(entry)).find("kivy.uix.label.Label") != -1):
                if(entry.text.find("Passwordname:") != -1):
                    passwordname= entry.text[entry.text.find(":")+1:]
                    self.create_dialog("Update entry "+passwordname,"Do you want to update the password?",self.update_entry).open()

    def update_entry(self,obj):
        dialog=self.find_parent_type_of_widget("kivymd.uix.dialog.MDDialog",obj)
        passwordname= dialog.title[13:]
        # Update passwort on target and passwordmanager
        update_password = UpdatePassword()
        # Only update password when the change was successful
        if update_password.update_password(passwordname) == 0:
            Passwordmanager.update_password(passwordname,)


        dialog.dismiss()

    def delete_dialog(self,obj):
        # Get password name
        for entry in obj.parent.children:
            if(str(type(entry)).find("kivy.uix.label.Label") != -1):
                if(entry.text.find("Passwordname:") != -1):
                    passwordname= entry.text[entry.text.find(":")+1:]
                    self.create_dialog("Delete entry "+passwordname,"Do you want to remove the entry?",self.delete_entry).open()
            
    def delete_entry(self,obj):

        dialog=self.find_parent_type_of_widget("kivymd.uix.dialog.MDDialog",obj)
        passwordname= dialog.title[13:] 
        # Request to passwordmanager 
        # TODO why does deleating on the python side not work
        if ( Passwordmanager.delete_login(passwordname) == True):
            # Delete gui element
            self.ids[passwordname].parent.remove_widget(self.ids[passwordname])
        else:
            print("Could not delete password")
        dialog.dismiss()


    def create_dialog(self,title:String,text:String,executeOnOK,buttonList=None):
        if(buttonList == None):
            buttonList = [MDFlatButton(text='Ok',on_release=executeOnOK),MDFlatButton(text='Cancel',on_release=self.close_dialog)]
        else:
            buttonList.append(MDFlatButton(text='Ok',on_release=executeOnOK))
            buttonList.append(MDFlatButton(text='Cancel',on_release=self.close_dialog))
        
        dialog = MDDialog(title=title,text=text,size_hint=(0.7,1),buttons=buttonList)
        return dialog

    def print_id(self,obj):
        print("ID:")


    def create_password_entry_dialog(self,obj):
        layout = GridLayout(cols = 2, padding = 10,row_force_default=True,row_default_height=40)
        # Instantiate the modal popup and display
        popup = Popup(title ='Edit passwordentry',
                    content = layout)

        self.create_input("Passwordname","","passwordname",layout)
        self.create_input("Username","","username",layout)
        # TODO Is it ok to show password here? Are there other ways?
        self.create_input("Password","","password",layout)
        # TODO Drop down
        # Maybe change it to accounttyp
        self.create_input("Passwordtype","Website","passwordtype",layout)
        self.create_input("Passwordtarget","","passwordtarget",layout)

        # Bottom Buttons
        layout.add_widget(Button(text = "Save", on_release=self.save_new_password,pos_hint= {'center_x': .5, 'center_y': .5}))
        layout.add_widget(Button(text = "Close",on_release=popup.dismiss,pos_hint= {'center_x': .5, 'center_y': .5}))

        popup.open()

    # On button press - Create a popup dialog with a label and a close button
    def edit_password_entry(self, obj):

        for entry in obj.parent.children:
            if(str(type(entry)).find("kivy.uix.label.Label") != -1):
                if(entry.text.find("Passwordname:") != -1):
                    passwordname= entry.text[entry.text.find(":")+1:]
        layout = GridLayout(cols = 2, padding = 10,row_force_default=True,row_default_height=40)

        # Instantiate the modal popup and display
        popup = Popup(title ='Edit passwordentry',
                    content = layout)

        # Load data form the passwordmanager
        login_data = Passwordmanager.get_login_data(passwordname)
        

        self.create_input("Passwordname",passwordname,"passwordname",layout)
        self.create_input("Username",login_data["Username"],"username",layout)
        # TODO Is it ok to show password here? Are there other ways?
        self.create_input("Password",login_data["Password"],"password",layout)
        # TODO Drop down
        # Maybe change it to accounttyp
        self.create_input("Passwordtype",login_data["Type"],"passwordtype",layout)
        self.create_input("Passwordtarget",login_data["Target"],"passwordtarget",layout)

        # Bottom Buttons
        layout.add_widget(Button(text = "Save", on_release=self.save_data,pos_hint= {'center_x': .5, 'center_y': .5}))
        layout.add_widget(Button(text = "Close",on_release=popup.dismiss,pos_hint= {'center_x': .5, 'center_y': .5}))

        popup.open()

    def save_new_password(self,obj):
        self.create_and_append_password_entry(self.ids["passwordname"].text)
        self.save_data(obj)

    def save_data(self,obj):
        # Send save request to passwordmanager by using the popup ids
        print(self.ids["passwordname"].text)
        print(self.ids["username"].text)
        print(self.ids["password"].text)
        print(self.ids["passwordtype"].text)
        print(self.ids["passwordtarget"].text)
        self.find_parent_type_of_widget("kivy.uix.popup.Popup",obj).dismiss()

        # Request to passwortmanager
        Passwordmanager.save_login(self.ids["passwordname"].text,self.ids["username"].text,self.ids["password"].text,self.ids["passwordtype"].text,self.ids["passwordtarget"].text)




    # This methode creates and lable with an input field and assinges an ID to the imput field
    # After creating it is appends it to the given layout
    def create_input(self,labeltext:String,inputvalue:String,id:String,layout):
        layout.add_widget(Label(text = labeltext,pos_hint= {'center_x': .5, 'center_y': .5}))
        # Save the id for the inputfield
        self.ids[id]= TextInput(text=inputvalue, multiline=False,pos_hint= {'center_x': .5, 'center_y': .5})
        layout.add_widget(self.ids[id])
        # layout.add_widget(TextInput(text=inputvalue, multiline=False,pos_hint= {'center_x': .5, 'center_y': .5}))
