import hashlib,binascii

# crypt = hashlib.md5()
# crypt.update(b"Hello")
# print(crypt.hexdigest())
# print(crypt.digest_size)

# crypt = hashlib.sha256()
# crypt.update(b"Hello")
# print(crypt.hexdigest())
# print(crypt.digest_size)

# print(hashlib.algorithms_available)


# dk = hashlib.pbkdf2_hmac("sha256",b'password',b'salt', 100000)
# print(binascii.hexlify(dk))

class Hash:
    def __init__(self):
        self.loggedIn=False

    def list_algorithms():
        for hashtype in hashlib.algorithms_available:
                print(hashtype)

    def hash(algorithmName:str,password:str,salt:str,iterations:int):
        for hashtype in hashlib.algorithms_available:
            if ( hashtype == algorithmName):
                return  binascii.hexlify(hashlib.pbkdf2_hmac(algorithmName,password.encode('utf-8'),salt.encode('utf-8'), iterations))
        raise TypeError("Unknown algorithm")
            
Hash.list_algorithms()
print(Hash.hash("md5","password","salt",10).decode('utf-8'))
        