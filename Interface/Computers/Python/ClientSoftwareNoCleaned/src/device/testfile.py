from enum import Enum
class Status(Enum):
        Error = -2 # This is used when the device has trouble or is damaged but still can send messages
        UNKNOWN = -1 # Unknown status is used if there is a none valid status used or if there are erros 
        NOTREADY = 0 # This status is used when the device is not ready for a connection
        READY = 1 # This indicates that the device is ready for the connection
        INIT = 2 # This status indicates that the device still needs to get initiated
        LOCKED = 3 # Status shows that device needs to get unlocked before it can used
        UNLOCKED = 4 # Status shows that device is unlocked
        CONNECTED = 5 # This status indiates that the device is no connected and ready to be used

class MessageType(Enum):
    UNKNOWN = -1
    PING = 0
    INIT = 1
    DATA = 2
    REQUEST = 3
    RESPONSE = 4
    ACK = 5
    ACTIVE = 6

    def message_typ_to_string(message_typ):
        to_string = {
            MessageType.UNKNOWN:"UNKNOWN",
            MessageType.PING:"PING",
            MessageType.INIT:"INIT",
            MessageType.DATA:"DATA",
            MessageType.REQUEST:"REQUEST",
            MessageType.RESPONSE:"RESPONSE",
            MessageType.ACK:"ACK",
            MessageType.ACTIVE:"ACTIVE"
        }
        return to_string.get(message_typ,to_string.get(MessageType.UNKNOWN))

    def string_to_message_typ(string):
        to_message_typ = {
            "UNKNOWN":MessageType.UNKNOWN,
            "PING":MessageType.PING,
            "INIT":MessageType.INIT,
            "DATA":MessageType.DATA,
            "REQUEST":MessageType.REQUEST,
            "RESPONSE":MessageType.RESPONSE,
            "ACK":MessageType.ACK,
            "ACTIVE":MessageType.ACTIVE
        }
        return to_message_typ.get(string,to_message_typ.get("UNKNOWN"))
    
    
        

    def evaluteMessage(message):
        # Test if it is a message
        if (message.find("[") == 0 and message.find("]") != -1):
            messagetyp =MessageType.string_to_message_typ(message[1:message.find("]")])

            if (messagetyp == MessageType.UNKNOWN):
                print("Messagetyp not avaible/Unknown")
            elif (messagetyp == MessageType.PING):
                pass
            elif (messagetyp == MessageType.INIT):
                pass
            elif (messagetyp == MessageType.DATA):
                pass
            elif (messagetyp == MessageType.REQUEST):
                pass
            elif (messagetyp == MessageType.RESPONSE):
                pass
            elif (messagetyp == MessageType.ACK):
                pass
            
    
MessageType.testForPing("[PING]asdlkfjelafjkds")
class MessageData:
    def __init__(self,status,statusnumber) -> None:
        self.status=status
        self.statusnumber
# print(type(MessageType.message_typ_to_string(MessageType.PING)))
# print(type(MessageType.string_to_message_typ(MessageType.PING)))