import secrets
import json
import string

from ...communication.protocol import PasswordmanagerProtocol
from ...communication.messagetype import MessageType
from ...communication.passwordmanager_communication import PasswordmanagerCommunication
from enum import Enum
import time


class LoginData():
    def __init__(self, passwordname, username, password, passwordtype, passwordtarget) -> None:
        self.passwordname = passwordname
        self.username = username
        self.password = password
        self.passwordtype = passwordtype
        self.target = passwordtarget


class RequestType(Enum):
    UNKNOWN = 0
    GETALLPASSWORDNAMES = 1
    # Returns a complete password entry
    PASSWORDDATA = 2
    USERNAME = 3
    PASSWORD = 4
    PASSWORDTYPE = 5
    PASSWORDTARGET = 6
    SAVEPASSWORDDATA = 7
    UPDATEPASSWORDDATA = 8
    UPDATEPASSWORD = 9
    DELETEPASSWORD = 10


class Passwordmanager():
    password = "pass"
    connected = False

    def __init__(self) -> None:
        pass

    def unlock(password) -> bool:
        if PasswordmanagerProtocol.shared_message_storage["unlocked"] == False:
            PasswordmanagerProtocol.shared_message_storage["Password"] = password

            # Create answer message
            document = {
                "MessageType": MessageType.INIT.value,
                "MessageNumber": PasswordmanagerProtocol.shared_message_storage["currentMessageNumber"],
                "Stage": 3,
                "DeviceData": PasswordmanagerProtocol.shared_message_storage["Password"],
                "LastMessageNumber": PasswordmanagerProtocol.shared_message_storage["lastMessageNumber"],
            }
            PasswordmanagerCommunication.protocol.protocol(document, True)
            PasswordmanagerProtocol.shared_message_storage["unlocked"] = None

        PasswordmanagerCommunication.start(password)
        # Wait for the result
        while PasswordmanagerProtocol.shared_message_storage["unlocked"] == None:
            time.sleep(0.001)
        test = PasswordmanagerProtocol.shared_message_storage["unlocked"]
        return PasswordmanagerProtocol.shared_message_storage["unlocked"]
    # TODO Message number is no relaible
    # def wait_for_response(message_number):
    #     # Wait for response
    #     timeout = 10
    #     delay = 0.01
    #     current_timeout = 0
    #     result = None
    #     message = None
    #     from ...gui.loginapp import LoginWrapper
    #     try:
    #         message = PasswordmanagerProtocol.shared_message_storage[
    #             "received_messagebuffer"][message_number]
    #     except(KeyError):
    #         message = None

    #     while(message == None and current_timeout < timeout):
    #         time.sleep(delay)
    #         current_timeout += delay
    #         try:
    #             message = PasswordmanagerProtocol.shared_message_storage[
    #                 "received_messagebuffer"][message_number]
    #         except(KeyError):
    #             message = None
    #     if timeout > current_timeout:
    #         result = LoginWrapper.shared_message_storage[message_number]

    #         # If message is a string converti it to json
    #         if (isinstance(result, str) == True):
    #             result = json.loads(result)
    #         return result
    #     else:
    #         return "timeout"

    def wait_for_response(request_type):
        # Wait for response
        timeout = 10
        delay = 0.10
        current_timeout = 0
        result = None
        message = None
        from ...gui.loginapp import LoginWrapper

        print(PasswordmanagerProtocol.shared_message_storage)
        print(
            PasswordmanagerProtocol.shared_message_storage["received_messagebuffer"+str(request_type)])
        # for key in PasswordmanagerProtocol.shared_message_storage["received_messagebuffer"]:
        #     print(key)

        while(message == None and current_timeout < timeout):
            # print(PasswordmanagerProtocol.shared_message_storage)
            time.sleep(delay)
            current_timeout += delay
            message = PasswordmanagerProtocol.shared_message_storage["received_messagebuffer"+str(
                request_type)]
        if timeout > current_timeout:
            result = message

            # If message is a string converti it to json
            if (isinstance(result, str) == True):
                result = json.loads(result)
            return result
        else:
            return "timeout"

    def get_password(passwordname):
        request = {
            "MessageType": MessageType.REQUEST.value,
            "MessageNumber": 0,
            "RequestType": RequestType.PASSWORD.value,
            "Passwordname": passwordname,
            "LastMessageNumber": 0
        }
        PasswordmanagerCommunication.protocol.protocol(
            message=request, send=True)
        result = Passwordmanager.wait_for_response(RequestType.PASSWORD.value)

        if(result != None and result != "timeout"):
            return result["Password"]
        else:
            return None

    def get_login_data(passwordname):
        request = {
            "MessageType": MessageType.REQUEST.value,
            "MessageNumber": 0,
            "RequestType": RequestType.PASSWORDDATA.value,
            "Passwordname": passwordname,
            "LastMessageNumber": 0
        }

        PasswordmanagerCommunication.protocol.protocol(
            message=request, send=True)
        result = Passwordmanager.wait_for_response(RequestType.PASSWORDDATA.value)

        if(result != None and result != "timeout"):
            return result
        else:
            return None

    def is_connected() -> bool:
        return PasswordmanagerCommunication.connection

    def save_login(passwordname, username, password, passwordtype, passwordtarget):
        request = {
            "MessageType": MessageType.REQUEST.value,
            "MessageNumber": 0,
            "RequestType": RequestType.SAVEPASSWORDDATA.value,
            "Passwordname": passwordname,
            "UserName": username,
            "Password": password,
            "PasswordType": passwordtype,
            "PasswordTarget": passwordtarget,
            "LastMessageNumber": 0
        }
        PasswordmanagerCommunication.protocol.protocol(
            message=request, send=True)
        result = Passwordmanager.wait_for_response(RequestType.SAVEPASSWORDDATA.value)

        if(result != None and result != "timeout"):
            return result["RequestSuccessful"]
        else:
            return None

    def delete_login(passwordname):
        print("Debug remove passworddata: " + passwordname)
        request = {
            "MessageType": MessageType.REQUEST.value,
            "MessageNumber": 0,
            "RequestType": RequestType.DELETEPASSWORD.value,
            "Passwordname": passwordname,
            "LastMessageNumber": 0
        }
        PasswordmanagerCommunication.protocol.protocol(
            message=request, send=True)
        result = Passwordmanager.wait_for_response(RequestType.DELETEPASSWORD.value)

        if(result != None and result != "timeout"):
            return result["RequestSuccessful"]
        else:
            return None

    def generate_password():
        alphabet = string.ascii_letters + string.digits
        password = ''.join(secrets.choice(alphabet)
                           for i in range(20))  # for a 20-character password
        print(password)
        print("Debug generated password: " + password)
        return password

    def update_password(passwordname, new_password):
        print("Debug update password" + passwordname +
              " with new password " + new_password)
        request = {
            "MessageType": MessageType.REQUEST.value,
            "MessageNumber": 0,
            "RequestType": RequestType.UPDATEPASSWORD.value,
            "Passwordname": passwordname,
            "LastMessageNumber": 0
        }
        PasswordmanagerCommunication.protocol.protocol(
            message=request, send=True)
        result = Passwordmanager.wait_for_response(RequestType.UPDATEPASSWORD.value)
        if(result != None and result != "timeout"):
            return result["RequestSuccessful"]
        else:
            return None

    def get_all_passwordnames():
        request = {
            "MessageType": MessageType.REQUEST.value,
            "MessageNumber": 0,
            "RequestType": RequestType.GETALLPASSWORDNAMES.value,
            "LastMessageNumber": 0
        }
        PasswordmanagerCommunication.protocol.protocol(
            message=request, send=True)
        result = Passwordmanager.wait_for_response(RequestType.GETALLPASSWORDNAMES.value)
        if(result != None and result != "timeout"):
            return result["Passwordname"]
        else:
            return None

# import secrets
# import json
# import string
# from ...communication.messagetype import MessageType
# from ...communication.passwordmanager_communication import PasswordmanagerCommunication, ConnectionType


# class LoginData():
#     def __init__(self, passwordname, username, password, passwordtype, passwordtarget) -> None:
#         self.passwordname = passwordname
#         self.username = username
#         self.password = password
#         self.passwordtype = passwordtype
#         self.target = passwordtarget


# class Passwordmanager():
#     password = "pass"
#     sim_data = {}
#     connected = False

#     def __init__(self) -> None:
#         PasswordmanagerCommunication.init(ConnectionType.SERIAL)
#         Passwordmanager.connected = PasswordmanagerCommunication.init_complete

#         # Passwordmanager.sim_data["proton"]=  LoginData("proton","TestPasswordmanager@protonmail.com","V6Dcm7gahCCLcqh2","webside","https://account.protonmail.com/login")
#         # Passwordmanager.sim_data["steam"]=  LoginData("steam","TestPasswordmanager@protonmail.com",Passwordmanager.generate_password(),"application","")
#         # Passwordmanager.sim_data["gmail"]=  LoginData("gmail","passmanagertestemail@gmail.com","j4ZCHWvT7j3nnCL","webside","https://accounts.google.com")
#         pass

#     def unlock(password)->bool:
#         if(password == Passwordmanager.password):
#             return True
#         else:
#             return False
#         # Passwordmanager.password = password
#         # request = {
#         #     "MessageType": MessageType.REQUEST.value,
#         #     "MessageNumber": 0,
#         #     "Password": password,
#         #     "LastMessageNumber": 0
#         # }
#         # PasswordmanagerCommunication.protocol.protocol(message=request,send=True)

#     def get_password(passwordname):
#         message = PasswordmanagerCommunication.get_password_data(passwordname)
#         return message["Password"]
#         # request = {
#         #     "MessageType": MessageType.REQUEST.value,
#         #     "MessageNumber": 0,
#         #     "Passwordname": passwordname,
#         #     "LastMessageNumber": 0
#         # }
#         # PasswordmanagerCommunication.protocol.protocol(message=request,send=True)

#     def get_login_data(passwordname):
#         message = PasswordmanagerCommunication.get_password_data(passwordname)
#         Passwordmanager.sim_data[passwordname] = LoginData(
#             message["Passwordname"], message["Username"], message["Password"], message["PasswordType"], message["PasswordTarget"])

#         # request = {
#         #     "MessageType": MessageType.REQUEST.value,
#         #     "MessageNumber": 0,
#         #     "Passwordname": passwordname,
#         #     "LastMessageNumber": 0
#         # }
#         # PasswordmanagerCommunication.protocol.protocol(message=request,send=True)

#     # def check_password(password) -> bool:
#     #     if(password == Passwordmanager.password):
#     #         return True
#     #     else:
#     #         return False

#     def is_connected() -> bool:
#         return Passwordmanager.connected

#     # in the real version ever value except password is send
#     # or create a secound methode without password
#     def get_login(passwordname) -> LoginData:
#         print("Debug get password: " + passwordname)
#         return Passwordmanager.sim_data[passwordname]

#     def save_login(passwordname, username, password, passwordtype, passwordtarget):
#         print("Debug save passworddata")
#         print(passwordname)
#         print(username)
#         print(password)
#         print(passwordtype)
#         print(passwordtarget)

#         Passwordmanager.sim_data[passwordname] = LoginData(
#             passwordname, username, password, passwordtype, passwordtarget)

#     def remove_login(passwordname):
#         print("Debug remove passworddata: " + passwordname)
#         del Passwordmanager.sim_data[passwordname]
#         return 0

#     def generate_password():
#         alphabet = string.ascii_letters + string.digits
#         password = ''.join(secrets.choice(alphabet)
#                            for i in range(20))  # for a 20-character password
#         print(password)
#         print("Debug generated password: " + password)
#         return password

#     def update_password(passwordname, new_password):
#         print("Debug update password" + passwordname +
#               " with new password " + new_password)
#         Passwordmanager.get_login(passwordname).password = new_password

#     def get_all_passwordnames():
#         passwords = []
#         for key in Passwordmanager.sim_data:
#             passwords.append(key)
#         return passwords
