import secrets
import json
import string
from ...communication.messagetype import MessageType
from ...communication.passwordmanager_communication import PasswordmanagerCommunication, ConnectionType


class LoginData():
    def __init__(self, passwordname, username, password, passwordtype, passwordtarget) -> None:
        self.passwordname = passwordname
        self.username = username
        self.password = password
        self.passwordtype = passwordtype
        self.target = passwordtarget


class PasswordmanagerSim():
    password = "pass"
    sim_data = {}
    connected = False

    def __init__(self) -> None:
        PasswordmanagerCommunication.init(ConnectionType.SERIAL)
        PasswordmanagerSim.connected = PasswordmanagerCommunication.init_complete

        # PasswordmanagerSim.sim_data["proton"]=  LoginData("proton","TestPasswordmanager@protonmail.com","V6Dcm7gahCCLcqh2","webside","https://account.protonmail.com/login")
        # PasswordmanagerSim.sim_data["steam"]=  LoginData("steam","TestPasswordmanager@protonmail.com",PasswordmanagerSim.generate_password(),"application","")
        # PasswordmanagerSim.sim_data["gmail"]=  LoginData("gmail","passmanagertestemail@gmail.com","j4ZCHWvT7j3nnCL","webside","https://accounts.google.com")
        pass

    def unlock(password)->bool:
        if(password == PasswordmanagerSim.password):
            return True
        else:
            return False
        # PasswordmanagerSim.password = password
        # request = {
        #     "MessageType": MessageType.REQUEST,
        #     "MessageNumber": 0,
        #     "Password": password,
        #     "LastMessageNumber": 0
        # }
        # PasswordmanagerCommunication.protocol.protocol(message=request,send=True)

    def get_password(passwordname):
        message = PasswordmanagerCommunication.get_password_data(passwordname)
        return message["Password"]
        # request = {
        #     "MessageType": MessageType.REQUEST,
        #     "MessageNumber": 0,
        #     "PasswordName": passwordname,
        #     "LastMessageNumber": 0
        # }
        # PasswordmanagerCommunication.protocol.protocol(message=request,send=True)

    def get_login_data(passwordname):
        message = PasswordmanagerCommunication.get_password_data(passwordname)
        PasswordmanagerSim.sim_data[passwordname] = LoginData(
            message["Passwordname"], message["Username"], message["Password"], message["PasswordType"], message["PasswordTarget"])

        # request = {
        #     "MessageType": MessageType.REQUEST,
        #     "MessageNumber": 0,
        #     "PasswordName": passwordname,
        #     "LastMessageNumber": 0
        # }
        # PasswordmanagerCommunication.protocol.protocol(message=request,send=True)

    # def check_password(password) -> bool:
    #     if(password == PasswordmanagerSim.password):
    #         return True
    #     else:
    #         return False

    def is_connected() -> bool:
        return PasswordmanagerSim.connected

    # in the real version ever value except password is send
    # or create a secound methode without password
    def get_login(passwordname) -> LoginData:
        print("Debug get password: " + passwordname)
        return PasswordmanagerSim.sim_data[passwordname]

    def save_login(passwordname, username, password, passwordtype, passwordtarget):
        print("Debug save passworddata")
        print(passwordname)
        print(username)
        print(password)
        print(passwordtype)
        print(passwordtarget)

        PasswordmanagerSim.sim_data[passwordname] = LoginData(
            passwordname, username, password, passwordtype, passwordtarget)

    def remove_login(passwordname):
        print("Debug remove passworddata: " + passwordname)
        del PasswordmanagerSim.sim_data[passwordname]
        return 0

    def generate_password():
        alphabet = string.ascii_letters + string.digits
        password = ''.join(secrets.choice(alphabet)
                           for i in range(20))  # for a 20-character password
        print(password)
        print("Debug generated password: " + password)
        return password

    def update_password(passwordname, new_password):
        print("Debug update password" + passwordname +
              " with new password " + new_password)
        PasswordmanagerSim.get_login(passwordname).password = new_password

    def get_all_passwordnames():
        passwords = []
        for key in PasswordmanagerSim.sim_data:
            passwords.append(key)
        return passwords
