import string
import pyautogui


from pynput.keyboard import Key, Controller
import time

class Keyboard:
    controller = Controller()
    # TODO Add Key press and release functions


    def type(text):
        pyautogui.write(text,interval=0.01)
        pyautogui.press()

    def press_return():
        pyautogui.press("return")

    # Key can be either be an char or a special value
    def press_key(key:string):
        Keyboard.controller.press(key)

    # Key can be either be an char or a special value
    def release_key(key:string):
        Keyboard.controller.release(key)

    def press_and_release_key(key,delay:float=0.1):
        Keyboard.press_key(key)
        time.sleep(delay)
        Keyboard.release_key(key)


from enum import Enum
class ClickType(Enum):
    LEFT = 'left'
    RIGHT = 'right'
    MIDDLE = 'middle'
    DOUBLECLICK = 'double'


from pynput.mouse import Button, Controller as MouseController


class Mouse:
    controller = MouseController()
    # TODO Add mouse press and release functions
    def move(x_pos,y_pos,relative:bool = False):
        if(relative == True):
            pyautogui.moveRel(x_pos,y_pos)
        else:
            pyautogui.moveTo(x_pos,y_pos)
    
    def click(type:ClickType=ClickType.LEFT):
        if( ClickType.LEFT == type):
            pyautogui.click(button='left')
        elif( ClickType.RIGHT == type):
            pyautogui.click(button='right')
        elif( ClickType.MIDDLE == type):
            pyautogui.click(button='middle')
        elif( ClickType.DOUBLECLICK == type):
            pyautogui.doubleClick()

    def move_and_click(x_pos:int,y_pos:int,type:ClickType=ClickType.LEFT):
        if( ClickType.LEFT == type):
            pyautogui.click(x=x_pos, y=y_pos)
        elif( ClickType.RIGHT == type):
            pyautogui.click(button='right',x=x_pos, y=y_pos)
        elif( ClickType.MIDDLE == type):
            pyautogui.click(button='middle',x=x_pos, y=y_pos)
        elif( ClickType.DOUBLECLICK == type):
            #TODO test if it works
            pyautogui.doubleClick(x=x_pos, y=y_pos)

    def press(type:ClickType):
        if( ClickType.LEFT == type):
            Mouse.controller.press(Button.left)
        elif( ClickType.RIGHT == type):
            Mouse.controller.press(Button.right)
        elif( ClickType.MIDDLE == type):
            Mouse.controller.press(Button.middle)
        else:
            print("Not supported")

    def release(type:ClickType):
        if( ClickType.LEFT == type):
            Mouse.controller.release(Button.left)
        elif( ClickType.RIGHT == type):
            Mouse.controller.release(Button.right)
        elif( ClickType.MIDDLE == type):
            Mouse.controller.release(Button.middle)
        else:
            print("Not supported")

    # Negative values scroll down
    def scroll_up(value:int):
        Mouse.scroll(0,value)

    # Negative values scroll left
    def scroll_right(value:int):
        Mouse.scroll(value,0)

    def scroll(horizontal_scroll:int,vertical_scroll:int):
        # Scroll up two steps
        Mouse.controller.scroll(horizontal_scroll, vertical_scroll)
