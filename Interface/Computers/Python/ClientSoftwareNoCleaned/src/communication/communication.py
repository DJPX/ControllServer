import time
from ..exceptions.exceptions import InitCommunicationError
import multiprocessing
from abc import ABC, abstractmethod
# from logging.config import stop_listening
import string
import serial
import serial.tools.list_ports
import requests
import json

from src.communication.messagetype import MessageType


class Communication(ABC):

    @abstractmethod
    def send_message(self, label, message):
        pass

    def send_method(self, messagetype: MessageType, message):
        self.send_message(
            MessageType.message_typ_to_string(messagetype, message))

    @abstractmethod
    def send_json_message(self, json):
        pass

# This class is used to update credntials or informations directly on a server with a rest api


class WebCommunication(Communication):
    def __init__(self, url, methode):
        self.methode = methode
        self.url = url

    # def startListening(self,process_message):
    #     skip

    def send_message_to(self, message, url, methode):
        if url is None:
            url = self.url

        if methode is None:
            methode = self.methode
        try:
            if methode == "post":
                response = requests.post(url, data=message)
            elif methode == "get":
                response = requests.get(url, data=message)
            elif methode == "put":
                response = requests.put(url, data=message)
            elif methode == "delete":
                response = requests.delete(url, data=message)
            else:
                response = None
                print("Methode: " + methode * " is not supported.")
        except requests.exceptions.ConnectionError:
            print("Error while trying to connect to " + url + ".")
        return response

    def send_message(self, message):
        # response = requests.post(self.url, data = message)
        response = self.send_messageTo(message, self.url, self.methode)
        return response

    def set_url(self, url):
        self.url = url
        return self

    def get_url(self):
        return self.url

    def set_methode(self, methode):
        self.methode = methode
        return self

    def get_methode(self):
        return self.methode


class SerialCommunication(Communication):
    def __init__(self, port, baudrate, bytesize, timeout, stopbits=serial.STOPBITS_ONE):
        self.port = port
        self.baudrate = baudrate
        self.bytesize = bytesize
        self.timeout = timeout
        self.stopbits = stopbits
        self.open_port()
        self.run = multiprocessing.Value('b',False)
        # try:
        #     self.serialPort = serial.Serial(port = self.port, baudrate=self.baudrate, bytesize=self.bytesize, timeout=self.timeout, stopbits=self.stopbits)
        # except:
        #     # print("Could not open port.")
        #     raise InitCommunicationError("Could not create communication.")

    # Try to open port
    def open_port(self):
        try:
            self.serialPort = serial.Serial(port=self.port, baudrate=self.baudrate,
                                            bytesize=self.bytesize, timeout=self.timeout, stopbits=self.stopbits)
        except:
            # print("Could not open port.")
            raise InitCommunicationError(
                "Could not create communication on port:" + self.port)

    def __message_listener(self, process_message,run:multiprocessing.Value,shared_message_storage=None):
        serial_string = ""                           # Used to hold data coming over UART
        run.value  = True
        stop_listening = False
        while(run.value):
            # Wait until there is data waiting in the serial buffer
            if(self.serialPort.in_waiting > 0):
                # Read data out of the buffer until a carraige return / new line is found
                serial_string = self.serialPort.readline()
                #TODO maybe use UTF-8 instead of ascii?
                # Error UnicodeDecodeError: 'ascii' codec can't decode byte 0xc3 in position 71: ordinal not in range(128)
                json_string = serial_string.decode('Ascii')
                # Print the contents of the serial data
                

                d = "}"
                json_documents = [e+d for e in json_string.split(d) if e]
                print("Recieved message: " + ''.join(json_documents))
                for json_document in json_documents:
                    stop_listening = process_message(
                        json_document)
                    if(stop_listening == True):
                        run.value  = False
                        print("Stop listen")
                        return 0
            time.sleep(0.1)

    def start_listening(self, process_message,shared_message_storage=None):
        self.run.value  = True
        if shared_message_storage == None:
            self.process = multiprocessing.Process(target=self.__message_listener, args=(
                process_message,self.run), name='Message_Listener')
        else:
            self.process = multiprocessing.Process(target=self.__message_listener, args=(
                process_message,self.run,shared_message_storage), name='Message_Listener')
        self.process.start()

    def get_exitcode(self):
        return self.process.exitcode

    def stop_listening(self):
        self.run.value  = False

    def is_running(self)->bool:
        return self.run.value

    def send_message(self, messagetype: MessageType, message: str):
        print(type(messagetype))
        if("<class 'str'>" == type(messagetype)):
            bytemessage = "["+messagetype+"]"+message+"\n"
            self.serialPort.write(bytemessage.encode('utf-8'))
            self.serialPort.write(b""+message+" \r\n")
        elif("<class 'MessageType'>" == type(messagetype)):
            self.send_message(
                MessageType.message_typ_to_string(messagetype), message)

    def send_json_message(self, json_message: str):
        print(json_message)
        self.serialPort.write(json_message.encode('ascii'))
        self.serialPort.flush()

    def list_ports():
        ports = [comport.device for comport in serial.tools.list_ports.comports()]
        return ports
