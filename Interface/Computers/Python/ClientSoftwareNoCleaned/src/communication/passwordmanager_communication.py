from .communication import Communication, SerialCommunication
from .messagetype import MessageType
import time
import multiprocessing
from enum import Enum
import time
from .protocol import PasswordmanagerProtocol
from ..exceptions.exceptions import InitCommunicationError
import json


class ConnectionType(Enum):
    UNKNOWN = -1
    SERIAL = 0
    WLAN = 1
    BLUETOOTH = 2

# TODO Implement diffrend connectiontypes
# TODO Add encryption mode to ensure an encrypted communication between


class PasswordmanagerCommunication:
    protocol = None
    # SerialCommunication("None", "115200", 8, 2)
    serial_comunication: SerialCommunication = None
    found_passwordmanager_port = False
    process_message = None
    mode = None
    connection = False
    loggedIn = False
    # init_complete = False
    

    def set_process_message(process_message):
        if process_message != None:
            PasswordmanagerCommunication.process_message = process_message

    # Define the timeout as float value. If no timeout is needed use -1. Default timeout is 10 secounds
    # Delay defines how long the program should wait for the next scan
    def wait_for_avaible_port(timeout: float = 5, delay: float = 0.1):
        current_timeout = 0
        while(PasswordmanagerCommunication.found_passwordmanager_port != True):
            try:

                # if (PasswordmanagerCommunication.serial_comunication == None):
                port = PasswordmanagerCommunication.find_port()
                # Test if there is a port
                if port != None:
                    print("Found port to use:"+port)
                    PasswordmanagerCommunication.serial_comunication = SerialCommunication(
                        port, "115200", 8, 2)
                    PasswordmanagerCommunication.protocol = PasswordmanagerProtocol(
                        PasswordmanagerCommunication.serial_comunication)
                    # PasswordmanagerCommunication.serial_comunication.start_listening(PasswordmanagerCommunication.__message_listener,shared_message_storage)
                    PasswordmanagerCommunication.found_passwordmanager_port = True
                    return True
                else:
                    print("Found no port to use.")
            except InitCommunicationError:
                PasswordmanagerCommunication.connection = False
                raise InitCommunicationError("Could not create communication.")

            time.sleep(delay)
            current_timeout += delay
            if current_timeout >= timeout and timeout != -1:
                return False
            print("Waiting for device")
        return False

    # Returns boolean, it indecates it the message listner should still run or stop
    def __message_listener(message,shared_message_storage=None):
        if(PasswordmanagerProtocol.shared_message_storage["successful"] and PasswordmanagerCommunication.process_message != None):
            if(shared_message_storage == None):
                PasswordmanagerCommunication.process_message(message)
            else:
                PasswordmanagerCommunication.process_message(message,shared_message_storage)
        else:
            # if (PasswordmanagerCommunication.init_complete == False):
                PasswordmanagerCommunication.protocol.protocol(message=message,send=False,shared_message_storage=shared_message_storage)
                # if PasswordmanagerProtocol.shared_message_storage["successful"]:
                    # PasswordmanagerCommunication.init_complete = True
            # else:
                # print("Protocol complete.")

    # Returns true if connection was succesfull or false if it failed
    def init(mode: ConnectionType,process_message=None) -> bool:
        PasswordmanagerCommunication.mode = mode
        if (PasswordmanagerCommunication.mode == ConnectionType.SERIAL):
            if(PasswordmanagerCommunication.wait_for_avaible_port()):
                # PasswordmanagerCommunication.serial_comunication.start_listening(PasswordmanagerCommunication.__message_listener)
                PasswordmanagerCommunication.serial_comunication.start_listening(PasswordmanagerCommunication.__message_listener,PasswordmanagerProtocol.shared_message_storage)
                PasswordmanagerCommunication.protocol = PasswordmanagerProtocol(
                    PasswordmanagerCommunication.serial_comunication)
        PasswordmanagerCommunication.set_process_message(process_message)
        
        if PasswordmanagerCommunication.is_connection_avaible():
        #     PasswordmanagerCommunication.init_complete = True
            return True
        else:
            return False
        #     PasswordmanagerCommunication.init_complete = False
        # return PasswordmanagerCommunication.init_complete

    # Returns True if the protocol was able to start and false if not
    def start(device_password:str,process_message=None) -> bool:
        # Reset protocol status
        PasswordmanagerProtocol.shared_message_storage["successful"] = None

        # PasswordmanagerCommunication.protocol.password = device_password
        PasswordmanagerProtocol.shared_message_storage["Password"]= device_password
        PasswordmanagerCommunication.set_process_message(process_message)

        if PasswordmanagerCommunication.is_connection_avaible():
            PasswordmanagerCommunication.protocol.protocol(None)
            return True
        else:
            return False

            # # Wait until the protocol got executet (succesfull or failed)
            # while(PasswordmanagerProtocol.shared_message_storage["successful"] == None):
            #     time.sleep(0.001)

            # if return_value == -1:
            #     return False
            # return PasswordmanagerProtocol.shared_message_storage["unlocked"]
            

    def stop():
        if PasswordmanagerCommunication.is_connection_avaible():
            PasswordmanagerCommunication.serial_comunication.stop_listening()

    def is_connection_avaible() -> bool:
        if PasswordmanagerCommunication.serial_comunication != None:
            return True
        else:
            print("No serial connection available.")
            return False

    # Send message as string (used for json messages)
    def send_json_message(json_message):
        print("Sendmessage:"+json_message)
        if PasswordmanagerCommunication.is_connection_avaible():
            PasswordmanagerCommunication.serial_comunication.send_json_message(
                json_message)

    def login(password):
        pass

    # Returns True when it finds the passwordmanager
    # Additial when used with the start_listening it stops the listener when it returns true
    def test_for_ping(message: str):
        print("Scanning device")
        PasswordmanagerCommunication.found_passwordmanager_port = True
        json_document = json.loads(message)

        if json_document["MessageType"] == MessageType.PING.value:
            print("Found device")
            PasswordmanagerCommunication.found_passwordmanager_port = True
        else:
            print("Found no device")
            PasswordmanagerCommunication.found_passwordmanager_port = False
        return PasswordmanagerCommunication.found_passwordmanager_port

    # Test if device on the other side is interessed to conect

    # def test_ports(testport, found_ping, protocoll_queue: multiprocessing.Queue):
    def test_ports(testport, found_ping):
        PasswordmanagerCommunication.serial_comunication = SerialCommunication(
            testport, "115200", 8, 2)
        PasswordmanagerCommunication.found_passwordmanager_port = False
        PasswordmanagerCommunication.serial_comunication.start_listening(
            PasswordmanagerCommunication.test_for_ping)
        PasswordmanagerCommunication.protocol = PasswordmanagerProtocol(
            PasswordmanagerCommunication.serial_comunication)
        while found_ping.value == False:
            from datetime import datetime
            PasswordmanagerCommunication.protocol.protocol(None)
            found_ping.value = (
                PasswordmanagerCommunication.serial_comunication.is_running())
            # serial_comunication.send_message(MessageType.PING, "Test")
            time.sleep(0.01)
        # PasswordmanagerCommunication.serial_comunication.serialPort.__del__()

    def find_port():
        print("Start scanning ports.")

        com_ports = SerialCommunication.list_ports()

        print("Found "+str(len(com_ports)) + " Ports.")
        print(com_ports)

        for port in com_ports:
            # For debugging so it does not interfere with the serial output/flasher
            if port != "/dev/ttyUSB0":
                print(f'{port} gets scanned.')
                found_ping = multiprocessing.Value('i')
                proc = multiprocessing.Process(target=PasswordmanagerCommunication.test_ports, args=(
                    port, found_ping), name='Find_Passwordmanager')

                proc.start()
                proc.join(timeout=5)
                proc.terminate()
                if proc.exitcode is None:
                    print(f'{proc} reached timeout. No answer.')
                if proc.exitcode == 0:
                    print(f'{proc} received answer from passwortmanager.')
                    print('Found port '+port)
                    return port
        return None

    def get_password_data(passwordname):
        # Create answer message
        document = {
                "MessageType": MessageType.REQUEST.value,
                "MessageNumber": 0,
                "Request": "GetPasswordData",
                "PasswordName": passwordname,
        }
        # Send data
        messagenumber = PasswordmanagerCommunication.protocol(document,True)
        # Wait for the answer
        message = PasswordmanagerCommunication.protocol.get_message_from_buffer(messagenumber)
        while  message == None:
            time.sleep(0.01)
            message = PasswordmanagerCommunication.protocol.get_message_from_buffer(messagenumber)
        return message

