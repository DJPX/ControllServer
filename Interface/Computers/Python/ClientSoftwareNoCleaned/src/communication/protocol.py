from .communication import Communication
from .messagetype import MessageType
import json
from enum import Enum


class DeviceStatus(Enum):
    ERROR = -2
    UNKNOWN = -1
    NOTCONNECTED = 0
    INIT = 1
    CONNECTED = 2
    NOTREADY = 3
    READY = 4


class Device:
    def __init__(self, device_name:str, device_type:str, status:DeviceStatus, connection_type:str, stage=0):
        self.device_name = device_name
        self.device_type = device_type
        self.status = status
        self.connection_type = connection_type
        self.stage = stage
        self.device_data = ""

import multiprocessing

class PasswordmanagerProtocol:
    # TODO enable multiple connections with this protocoll (for example index+ConnectionID)
    shared_message_storage = multiprocessing.Manager().dict()
    shared_message_storage["currentMessageNumber"]=0
    shared_message_storage["lastMessageNumber"]=0
    shared_message_storage["unlocked"]=None
    # None means that the protocol waas not executed
    shared_message_storage["successful"]=None
    # Saves the received messages
    shared_message_storage["received_messagebuffer"]={}
    # Devices in shared memory (problems with infinity loops or stages)
        # PasswordmanagerProtocol.shared_message_storage["this_device"]=Device("", "", DeviceStatus.UNKNOWN, "Serial", 0)
        # PasswordmanagerProtocol.shared_message_storage["connected_device"]=Device( "", "", DeviceStatus.UNKNOWN, "Serial", 0)
        # self.this_device = PasswordmanagerProtocol.shared_message_storage["this_device"]
        # self.connected_device = PasswordmanagerProtocol.shared_message_storage["connected_device"]

    def __init__(self, communication: Communication):
        self.password = "NoPassword"
        self.communication = communication
        
        
        self.this_device = Device("", "", DeviceStatus.UNKNOWN, "Serial", 0)
        self.connected_device = Device(
            "", "", DeviceStatus.UNKNOWN, "Serial", 0)


        

    def set_password(self, password):
        self.password = password

    def get_message_from_buffer(self, messagenumber: int):
        message = PasswordmanagerProtocol.shared_message_storage["received_messagebuffer"][messagenumber]
        if message != None:
            del PasswordmanagerProtocol.shared_message_storage["received_messagebuffer"][messagenumber]
        return message

    def protocol(self, message, send: bool = True, shared_message_storage=None):
        print("Executing protocol.")
        # TODO maybe not needed
        document = None
        # If message is a string converti it to json
        if (isinstance(message, str) == True):
            message = json.loads(message)
        
        # Send custom message when init protocol was succesful
        if PasswordmanagerProtocol.shared_message_storage["successful"]:
            PasswordmanagerProtocol.shared_message_storage["currentMessageNumber"] = PasswordmanagerProtocol.shared_message_storage["currentMessageNumber"]+1
            if send == True:
                # Setting correct messagenumber
                message["MessageNumber"] = PasswordmanagerProtocol.shared_message_storage["currentMessageNumber"]
                if message["MessageType"] == MessageType.REQUEST.value:
                    # Reserve space for incomming respones message
                    # PasswordmanagerProtocol.shared_message_storage["received_messagebuffer"][message["MessageNumber"]] = "NA"
                    PasswordmanagerProtocol.shared_message_storage["received_messagebuffer"+str(message["RequestType"])] = None
                elif message["MessageType"] == MessageType.ACK.value:
                    pass
                document = message
                # return PasswordmanagerProtocol.shared_message_storage["currentMessageNumber"]
            else:
                # Save message in the message buffer with the accourding messagenumber
                # self.received_messagebuffer[message["MessageNumber"]-1] = message
                PasswordmanagerProtocol.shared_message_storage["received_messagebuffer"+str(message["Response"])] = message
                return 0

        # First ping message
        # and (self.this_device.status == DeviceStatus.UNKNOWN or self.this_device.status == DeviceStatus.NOTCONNECTED):
        elif message == None:
            # Setting stage
            self.this_device.stage = 1
            PasswordmanagerProtocol.shared_message_storage["currentMessageNumber"] += 1
            document = {
                "MessageType": MessageType.PING.value,
                "MessageNumber": PasswordmanagerProtocol.shared_message_storage["currentMessageNumber"],
                "Stage": self.this_device.stage,
                "Device": "PasswordmanagerClient",
                "ConnectionType": self.this_device.connection_type,
                "LastMessageNumber": PasswordmanagerProtocol.shared_message_storage["lastMessageNumber"],
            }
        else:
            # TODO Message counter does not work (because of seperated ping protocol)
            if message["LastMessageNumber"] != PasswordmanagerProtocol.shared_message_storage["currentMessageNumber"] and False:
                # TODO implement
                # Error not the currect message number
                # Reset connection
                return None
            else:
                PasswordmanagerProtocol.shared_message_storage["currentMessageNumber"] += 1
                PasswordmanagerProtocol.shared_message_storage["lastMessageNumber"] = message["LastMessageNumber"]
                self.connected_device.stage = message["Stage"]
                # Ping stage
                if message["MessageType"] == MessageType.PING.value:
                    # Set data
                    self.this_device.stage = self.connected_device.stage+1
                    self.connected_device.status = DeviceStatus.NOTCONNECTED

                    # Response pingmessage 1
                    if message["Stage"] == 1:
                        # Extract data
                        self.connected_device.connection_type = message["ConnectionType"]
                        self.connected_device.device_type = message["Device"]
                        self.connected_device.stage = message["Stage"]

                        allow_connection = False
                        if self.this_device.connection_type == self.connected_device.connection_type:
                            allow_connection = True

                        # Create answer message
                        document = {
                            "MessageType": MessageType.PING.value,
                            "MessageNumber": PasswordmanagerProtocol.shared_message_storage["lastMessageNumber"],
                            "Stage": self.this_device.stage,
                            "Device": "PasswordmanagerClient",
                            "ConnectionType": self.this_device.connection_type,
                            "AllowConnection": allow_connection,
                            "LastMessageNumber": PasswordmanagerProtocol.shared_message_storage["lastMessageNumber"],
                        }
                        # TODO does not send secound response to passwordmanager maybe some problem with entering the methode?
                    # Response pingmessage 2
                    elif message["Stage"] == 2:
                        # Extract data
                        self.connected_device.connection_type = message["ConnectionType"]
                        self.connected_device.device_type = message["Device"]
                        self.connected_device.stage = message["Stage"]

                        allow_connection = False
                        if self.this_device.connection_type == self.connected_device.connection_type:
                            allow_connection = True

                        # Create answer message
                        document = {
                            "MessageType": MessageType.PING.value,
                            "MessageNumber": PasswordmanagerProtocol.shared_message_storage["currentMessageNumber"],
                            "Stage": self.this_device.stage,
                            "AllowConnection": allow_connection,
                            "LastMessageNumber": PasswordmanagerProtocol.shared_message_storage["lastMessageNumber"],
                        }
                    # Response pingmessage 3
                    elif message["Stage"] == 3:
                        if message["AllowConnection"] == True:
                            self.this_device.status = DeviceStatus.CONNECTED
                            self.this_device.stage = 1

                        self.connected_device.stage = message["Stage"]

                        # Create answer message
                        document = {
                            "MessageType": MessageType.INIT.value,
                            "MessageNumber": PasswordmanagerProtocol.shared_message_storage["currentMessageNumber"],
                            "Stage": self.this_device.stage,
                            "DeviceStatus": self.this_device.status.value,
                            "LastMessageNumber": PasswordmanagerProtocol.shared_message_storage["lastMessageNumber"],
                        }
                # Init stage
                elif message["MessageType"] == MessageType.INIT.value:
                    self.this_device.stage = self.connected_device.stage + 1
                    # Response initmessage 1
                    if message["Stage"] == 1:
                        # Set data


                        self.connected_device.stage = message["Stage"]

                        # Create answer message
                        document = {
                            "MessageType": MessageType.INIT.value,
                            "MessageNumber": PasswordmanagerProtocol.shared_message_storage["currentMessageNumber"],
                            "Stage": self.this_device.stage,
                            "DeviceStatus": self.this_device.status.value,
                            "LastMessageNumber": PasswordmanagerProtocol.shared_message_storage["lastMessageNumber"],
                        }
                    # Response initmessage 2
                    elif message["Stage"] == 2:
                        # Set data
                        self.this_device.status = DeviceStatus.CONNECTED
                        self.connected_device.stage = message["Stage"]

                        if self.connected_device.device_type =="PasswordmanagerDevice":
                            self.this_device.device_data = PasswordmanagerProtocol.shared_message_storage["Password"]

                        # Create answer message
                        document = {
                            "MessageType": MessageType.INIT.value,
                            "MessageNumber": PasswordmanagerProtocol.shared_message_storage["currentMessageNumber"],
                            "Stage": self.this_device.stage,
                            "DeviceData": self.this_device.device_data,
                            "LastMessageNumber": PasswordmanagerProtocol.shared_message_storage["lastMessageNumber"],
                        }
                    # Response initmessage 3
                    elif message["Stage"] == 3:

                        # Set data according to DeviceData
                        self.connected_device.status = DeviceStatus.READY
                        self.this_device.status = DeviceStatus.READY
                        self.connected_device.stage = message["Stage"]
                        self.connected_device.device_data = message["DeviceData"]

                        # Process device date
                        # TODO
                        # message["DeviceData"]
                        if self.connected_device.device_type =="PasswordmanagerDevice":
                            self.this_device.device_data = PasswordmanagerProtocol.shared_message_storage["Password"]


                        # Create answer message
                        document = {
                            "MessageType": MessageType.INIT.value,
                            "MessageNumber": PasswordmanagerProtocol.shared_message_storage["currentMessageNumber"],
                            "Stage": self.this_device.stage,
                            "DeviceData": self.this_device.device_data,
                            "LastMessageNumber": PasswordmanagerProtocol.shared_message_storage["lastMessageNumber"],
                        }

                    # Response initmessage 4
                    elif message["Stage"] == 4:
                        # Process device date
                        # TODO
                        # message["DeviceData"]

                        # Set data according to DeviceData
                        self.connected_device.status = DeviceStatus.READY
                        self.this_device.status = DeviceStatus.READY
                        self.connected_device.stage = message["Stage"]
                        self.connected_device.device_data = message["DeviceData"]


                        # Create answer message
                        document = {
                            "MessageType": MessageType.INIT.value,
                            "MessageNumber": PasswordmanagerProtocol.shared_message_storage["currentMessageNumber"],
                            "Stage": self.this_device.stage,
                            "DeviceData": "",
                            "LastMessageNumber": PasswordmanagerProtocol.shared_message_storage["lastMessageNumber"],
                        }

                        if self.connected_device.device_data == "unlocked":
                            PasswordmanagerProtocol.shared_message_storage["unlocked"]=True
                        else:
                            PasswordmanagerProtocol.shared_message_storage["unlocked"]=False
                            


                        # Protocol was successful
                        PasswordmanagerProtocol.shared_message_storage["successful"] = True
                    # Process initmessage 5
                    elif message["Stage"] == 5:
                        

                        # Set data according to DeviceData
                        self.connected_device.status = DeviceStatus.READY
                        self.this_device.status = DeviceStatus.READY
                        self.connected_device.stage = message["Stage"]
                        self.connected_device.device_data = message["DeviceData"]

                        # Protocol was successful
                        PasswordmanagerProtocol.shared_message_storage["successful"] = True

                        document=None

                        if self.connected_device.device_data == "unlocked":
                            PasswordmanagerProtocol.shared_message_storage["unlocked"]=True
                        else:
                            PasswordmanagerProtocol.shared_message_storage["unlocked"]=False

        if document != None:
            # Send data
            json_string = json.dumps(document)
            self.communication.send_json_message(json_string)
            return PasswordmanagerProtocol.shared_message_storage["currentMessageNumber"]
