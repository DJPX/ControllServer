
from abc import ABC
from selenium import webdriver
import geckodriver_autoinstaller

# imports for instruction processor
from selenium import webdriver
import geckodriver_autoinstaller
import time
from selenium.webdriver.common.by import By
from ..instructions.instruction_processor import InstructionProcessor
from ..login.login_to import LoginWithSeleniumBrowser
from ..instructions.instructions import InstructionLabel,Instruction, Instructions,Instructiontype

from src.device.passwordmanager.passwordmanager import Passwordmanager
from abc import ABC, abstractmethod

class UpdatePassword(ABC):
    @abstractmethod
    def update_password(self,passwordname):
        pass

    def set_passwords(self,current_password,new_password):
        self.current_passwords = current_password
        self.new_password = new_password

    # Returns the old and new password
    def get_passwords(self,passwordname):
        # Send request to passwordmanager
        return Passwordmanager.get_password(passwordname),Passwordmanager.generate_password()




class UpdateExternalPassword(UpdatePassword):


    # 
    def __init__(self,driver=None):
        if(driver == None):
            try:
                from selenium.webdriver.firefox.options import Options
                
                geckodriver_autoinstaller.install()
                options = Options()
                # options.add_argument("--headless")
                driver = webdriver.Firefox(options=options)
            except Exception: # Get the real exception when the browser is not found
                print("Browser not found")
        self.driver = driver
        

    def update_password(self,passwordname):
        current_password,new_password = self.get_passwords(passwordname)
        # Change password
        placeholders= {
            "current_password":current_password,
            "new_password":new_password
        }
        update_instructions = UpdatePassword.get_instructions(passwordname)
        # Test if instructions are avaible, or if they do not exit 
        if (update_instructions == None):
            return 1
        else:
            login= LoginWithSeleniumBrowser(passwordname,self.driver)
            login.login()
        
            # Test if request was processed succesful
            if (InstructionProcessor.process_instructionset(instructionset=update_instructions,placeholders=placeholders,driver=self.get_driver()) == 0):
                Passwordmanager.update_password(new_password)
                return 0
            # Request was not successful do not update password
            else:
                print("Error while trying to update the password")
                return 1

    def get_driver(self):
        return self.driver

    def set_driver(self,driver):
        self.driver = driver

    def set_passwords(self,current_password,new_password):
        self.current_passwords = current_password
        self.new_password = new_password

    # Returns the old and new password
    # def get_passwords(self,passwordname):
    #     # Send request to passwordmanager
    #     return Passwordmanager.get_password(passwordname),Passwordmanager.generate_password()
        

    # def init_browser(self):
    #     try:
    #         from selenium.webdriver.firefox.options import Options
            
    #         geckodriver_autoinstaller.install()
    #         options = Options()
    #         # options.add_argument("--headless")
    #         self.driver = webdriver.Firefox(options=options)
    #     except Exception: # Get the real exception when the browser is not found
    #         print("Browser not found")

    def close(self):
        try:
            self.driver.close()
        except:
            pass

    def get_instructions(passwordname):
        
        if ( Passwordmanager.get_login_data(passwordname)["Target"] == 'https://account.protonmail.com/login'):
            instructionset = Instructions("Update","Used to update "+passwordname)
            instructionset.add(instruction_typ=Instructiontype.SYSTEM,target='',instruction=Instruction.SLEEP,target_info=10)
            instructionset.add(instruction_typ=Instructiontype.BROWSER,target='https://account.protonmail.com/u/0/mail/account-password',instruction=Instruction.OPEN)
            instructionset.add(instruction_typ=Instructiontype.SYSTEM,target='',instruction=Instruction.SLEEP,target_info=5)
            instructionset.add(instruction_typ=Instructiontype.BROWSER,target='/html/body/div[1]/div[3]/div/div/div[2]/main/div/section[2]/div/div[1]/div[2]/button',instruction=Instruction.CLICK,target_info=0)
            instructionset.add(instruction_typ=Instructiontype.SYSTEM,target='',instruction=Instruction.SLEEP,target_info=2)
            instructionset.add(instruction_typ=Instructiontype.BROWSER,target='/html/body/div[4]/dialog/form/div[2]/div[3]/div/div/label[1]/div[2]/div/div[1]/input',instruction=Instruction.SEND_KEYS,target_info=0,placeholder="current_password")
            instructionset.add(instruction_typ=Instructiontype.BROWSER,target='/html/body/div[4]/dialog/form/div[2]/div[3]/div/div/label[2]/div[2]/div/div[1]/input',instruction=Instruction.SEND_KEYS,target_info=0,placeholder="new_password")
            instructionset.add(instruction_typ=Instructiontype.BROWSER,target='/html/body/div[4]/dialog/form/div[2]/div[3]/div/div/label[3]/div[2]/div/div[1]/input',instruction=Instruction.SEND_KEYS,target_info=0,placeholder="new_password")
            instructionset.add(instruction_typ=Instructiontype.BROWSER,target='/html/body/div[4]/dialog/form/div[3]/button[2]',instruction=Instruction.CLICK,target_info=0)
            return instructionset
        else:
            print("Unknown login target")
            return None

    # def update_password(self):
    #     self.scan_emails()
    #     # Connect to passwordmanager and and let it create a new password
    #     new_password = "aldsjf239fwp"

    # Passwordsource is the webside or name of the service this is used to find the correct email
    def scan_emails(self,passwordsource):
        return "Email content"

    def reset_password(self,emailcontent):
        pass
        # Use the headless browser to reset the password
        # try:
        #     from selenium.webdriver.firefox.options import Options
        #     geckodriver_autoinstaller.install()
        #     options = Options()
        #     options.add_argument("--headless")

        #     driver = webdriver.Firefox(options=options)

        #     password_reseturl = 'https://pythonbasics.org'

        #     driver.get(password_reseturl)
        #     print(driver.page_source)
        # finally:
        #     try:
        #         driver.close()
        #     except:
        #         pass

from ...communication.communication import WebCommunication
class UpdateInternalPassword(UpdatePassword):

    def __init__(self,driver=None):
        if(driver == None):
            try:
                from selenium.webdriver.firefox.options import Options
                
                geckodriver_autoinstaller.install()
                options = Options()
                # options.add_argument("--headless")
                driver = webdriver.Firefox(options=options)
            except Exception: # Get the real exception when the browser is not found
                print("Browser not found")
        self.driver = driver



    
    def update_password(self):
        login_data = Passwordmanager.get_login_data()
        # Update password
        params = {'username': login_data["Username"], 'password': login_data["Password"]}

        url = Passwordmanager.get_login_data()["Target"]+"/update"
        webcom = WebCommunication(url, "get")
        response = webcom.send_message(params)
        print("Changed password:" + ":"+response.text)
        # TODO test if request was sucessfull

