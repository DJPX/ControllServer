from abc import ABC, abstractmethod
from tokenize import String
from src.authentification.instructions.instruction_processor import InstructionProcessor
from src.authentification.instructions.instructions import Instruction,InstructionLabel,Instructiontype
from src.system.system_manager import WindowManager
from src.system.system_manager import SystemManager
import pyautogui
import time
from src.device.human_interface_device import Keyboard

## Seleniumbrowser dependencys
from re import A
from selenium import webdriver
import geckodriver_autoinstaller
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.firefox.options import Options as FirefoxOptions
import time
from src.exceptions.exceptions import EnterPasswordModeDoesNotExists, EnterPasswordTargetDoesNotExists, NoInstructions
from selenium.webdriver.common.by import By


from src.device.passwordmanager.passwordmanager import Passwordmanager,LoginData


class LoginTo(ABC):
    def __init__(self,username,password):
        self.username = username
        self.password = password

    @abstractmethod
    def login(self):
        # Override password and username after login to erase them
        self.password = "Pasword1234"
        self.username = "User4680"
        pass


    @abstractmethod
    def find_loginfield(self,target):
        pass

    # # The accuracyModifer describes how equal the images must match 
    # def scanForImage(self,imageSrc,accuracyModifier=0.5):
    #     result = None
    #     timeout=10
    #     activtime=0
    #     while activtime < timeout and result is None:
    #         result=pyautogui.locateOnScreen(imageSrc,grayscale=True,confidence=accuracyModifier)
    #         time.sleep(0.1)
    #         activtime=activtime+1
    #         #print("Time:"+str(activtime))
    #     return result

    def insert_to_inputfield(self,inputfield,text):
        oldPos=pyautogui.position()
        pyautogui.click(inputfield.left + inputfield.width/2, inputfield.top + inputfield.height/2)
        Keyboard.type(text)
        pyautogui.moveTo(oldPos)


from ...communication.communication import WebCommunication
class LoginToInternalService(LoginTo):
    def login(self):
        params = {'username': self.username, 'password': self.password}

        url = Passwordmanager.get_login_data()["Target"]+"/login"
        webcom = WebCommunication(url, "get")
        response = webcom.send_message(params)
        print("login" + ":"+response.text)
        print()


# Static values which are used to show how it would work if it is finished
identifiersSource = {
    "gmail": {"username": "identifierId",
              "password": '//*[@id ="password"]/div[1]/div / div[1]/input',
              "nextbutton": 'identifierNext',
              "nextbutton2" : '//*[@id ="passwordNext"]/*[1]'
              #   "URL":""
              },
    # "steam": {
    #     "username": 'input_username',
    #     "password": 'input_password',
    #         "nextbutton": '//*[@id="login_btn_signin"/button'
    # },
    "proton": {
        "username": 'username',
        "password": 'password',
        "nextbutton": '/html/body/div[1]/div[3]/div/div/main/div[2]/form/button'
    },
    "Ebaykleinanzeigen": {
        "username": 'input_username',
        "password": 'input_password',
            "nextbutton": '[@id="login_btn_signin"/button'
    }
    ,
    "dropbox": {
        "username": '/html/body/div[12]/div[1]/div[2]/div/div/div[3]/div/div[1]/div[2]/div/div/form/div[1]/div[1]/div[2]/input',
        "password": '/html/body/div[12]/div[1]/div[2]/div/div/div[3]/div/div[1]/div[2]/div/div/form/div[1]/div[2]/div[2]/input',
        "nextbutton": '/html/body/div[12]/div[1]/div[2]/div/div/div[3]/div/div[1]/div[2]/div/div/form/div[2]/button/div'
    }
}

# class LoginToBrowser(LoginTo):
#     def __init__(self,username,password,browsername):
#         self.username = username
#         self.password = password
#         self.browsername = browsername

#     def set_browsername(self,browsername):
#         self.browsername = browsername
#         return self

#     def get_browsername(self):
#         return self.browsername

#     def login(self):
#         WindowManager.setActiveWindow(self.browsername)

#     def set_webside(self,websideaddress):
#         self.websideaddress = websideaddress
#         return self

#     def get_webside(self):
#         return self.websideaddress

#     def find_loginfield(self,target):
#         if "gmail" in self.url:
#             label = "google"
#         elif "steam" in self.url:
#             label = "steam"
#         elif "t-online" in self.url:
#             label = "t-online"
#         elif "kleinanzeigen" in self.url:
#             label = "Ebaykleinanzeigen"
#         else:
#             print("Not found!")
#             label = "NA"

#         # Currently not used maybe later required
#         if (target == "password"):
#             pass
#         elif (target == "username"):
#             pass
#         elif (target == "nextbutton"):
#             pass
#         elif (target == "nextbutton2"):
#             pass
#         else:
#             raise EnterPasswordTargetDoesNotExists(
#                 "The mode: " + target + "  does not exist.")
from ..instructions.instructions import Instructions

class LoginWithSeleniumBrowser(LoginTo):
    def __init__(self, passwordname,driver=None):
        self.driver=None
        self.passwordname = passwordname
        self.instructionset = LoginWithSeleniumBrowser.get_instructions(self.passwordname)
        
        if ( driver != None):
            self.driver = driver
        else:
            self.driver = self.get_driver()

    # TODO not implemented will later used to find field with an AI or a dom-tree searcher
    # Targets can be the following
    # password: Returns the passwordfield
    # username: Returns the usernamefield or emailfield
    # nextbutton: Returns the button to confirm the username to get to the password page or the login button
    # nextbutton2: Returns the login button if the username and passwordfield were not on the same page. Else it is not used
    def find_loginfield(self, target:str):
        # Get the right field or button according to to label
        if (target == "password"):
            pass
        elif (target == "username"):
            pass
        elif (target == "nextbutton"):
            pass
        elif (target == "nextbutton2"):
            pass
        else:
            raise EnterPasswordTargetDoesNotExists(
                "The target: " + target + "  does not exist.")


    def get_driver(self):
        if ( self.driver == None):
            geckodriver_autoinstaller.install()
            self.options = FirefoxOptions()
            self.options.add_argument("--headless")
            self.driver = webdriver.Firefox()
        return self.driver

    def set_driver(self,driver):
        self.driver = driver

    def login(self):
        login_data = Passwordmanager.get_login(self.passwordname)
           
        placeholders= {
            "username":login_data["Password"],
            "password":login_data["Username"]
        }

        if(self.instructionset != None):
            InstructionProcessor.process_instructionset(self.instructionset,placeholders,driver=self.get_driver())
        else:
            # raise NoInstructions("Can not process instructionsset None. Please make sure that only valid instructions are used.")
            print("Can not process instructionsset None. Please make sure that only valid instructions are used.")


    # Loads the instructions form the passwortmanager database or for an internet database
    # TODO Currently only a dummy methode
    def get_instructions(passwordname):       
        if ( Passwordmanager.get_login(passwordname)["Target"] == 'https://account.protonmail.com/login'):
            login_instructions = Instructions("Login","This instructions are used to login to the proton email service")
            login_instructions.add(instruction_typ=Instructiontype.BROWSER,target='https://account.protonmail.com/login',instruction=Instruction.OPEN)
            login_instructions.add(instruction_typ=Instructiontype.SYSTEM,target='',instruction=Instruction.SLEEP,target_info=5)
            login_instructions.add(instruction_typ=Instructiontype.BROWSER,target='username',instruction=Instruction.SEND_KEYS,target_info=0,placeholder="username")
            login_instructions.add(instruction_typ=Instructiontype.BROWSER,target='password',instruction=Instruction.SEND_KEYS,target_info=0,placeholder="password")
            login_instructions.add(instruction_typ=Instructiontype.BROWSER,target='/html/body/div[1]/div[3]/div/div/main/div[2]/form/button',instruction=Instruction.CLICK,target_info=0)
            return login_instructions
        return None
        
        



class LoginWithSeleniumBrowserOld(LoginTo):
    def __init__(self, url, username, password,mode,driver=None):
        self.url = url
        self.username = username
        self.password = password
        self.mode = mode
        
        if ( driver == None):
            geckodriver_autoinstaller.install()
            self.options = FirefoxOptions()
            self.options.add_argument("--headless")
            self.driver = webdriver.Firefox()
        else:
            self.driver = driver

    def get_driver(self):
        return self.driver

    def set_driver(self,driver):
        self.driver = driver

    def login(self):
        self.driver.get(self.url)
        self.enter_login(self.mode)  

    def find_loginfield(self, target:String):
        label="None"

        if "google" in self.url:
            label = "gmail"
        elif "steam" in self.url:
            label = "steam"
        elif "proton" in self.url:
            label = "proton"
        elif "kleinanzeigen" in self.url:
            label = "Ebaykleinanzeigen"
        elif "dropbox" in self.url:
            label = "dropbox"
        else:
            print("Not found!")

        # Currently not used maybe later required
        if (target == "password"):
            pass
        elif (target == "username"):
            pass
        elif (target == "nextbutton"):
            pass
        elif (target == "nextbutton2"):
            pass
        else:
            raise EnterPasswordTargetDoesNotExists(
                "The mode: " + target + "  does not exist.")


        if (identifiersSource[label][target].startswith('/')):
            return self.driver.find_elements(by=By.XPATH, value=identifiersSource[label][target])
        else:
            return self.driver.find_elements(by=By.ID, value=identifiersSource[label][target])

    # TODO add diffrend modes
    # Mode 1: just a password and username/email field
    # Mode 2: Enter username and press enter, wait for redirection and enter then the password
    # Combined mode with 1 or 2 with additional login step (two factor authentification)

    def enter_login(self, mode):
        time.sleep(5)
        if (mode == 1):
            # self.find_loginfield("username").send_keys(self.username)
            # self.username = "NA"
            # self.find_loginfield("password").send_keys(self.password)
            # self.password = "NA"
            # self.find_loginfield("nextbutton").click()
            login_attribut_types = ["username","password","nextbutton"]
            for login_attribut_type in login_attribut_types:
                if (login_attribut_type == "username"):
                    login_attribut = self.username
                elif (login_attribut_type == "password"):
                    login_attribut = self.password
                    time.sleep(5)
                try:
                    if ("button" in login_attribut_type): 
                        self.find_loginfield(login_attribut_type).click()
                    else:
                        self.find_loginfield(login_attribut_type).send_keys(login_attribut)

                except AttributeError:
                    if ("button" in login_attribut_type):
                        self.find_loginfield(login_attribut_type)[0].click()
                    else:
                        fieldlog = self.find_loginfield(login_attribut_type)[0]
                        fieldlog.send_keys(login_attribut)

        elif (mode == 2):
            login_attribut_types = ["username","nextbutton","password","nextbutton2"]
            for login_attribut_type in login_attribut_types:
                if (login_attribut_type == "username"):
                    login_attribut = self.username
                elif (login_attribut_type == "password"):
                    login_attribut = self.password
                    time.sleep(5)
                try:
                    if ("button" in login_attribut_type): 
                        self.find_loginfield(login_attribut_type).click()
                    else:
                        self.find_loginfield(login_attribut_type).send_keys(login_attribut)

                except AttributeError:
                    if ("button" in login_attribut_type):
                        self.find_loginfield(login_attribut_type)[0].click()
                    else:
                        fieldlog = self.find_loginfield(login_attribut_type)[0]
                        fieldlog.send_keys(login_attribut)
            
            # self.find_loginfield("nextbutton").click()
            # time.sleep(5)
            # passWordBox = self.driver.find_element_by_xpath('//*[@id ="password"]/div[1]/div / div[1]/input')
            # passWordBox.send_keys(self.password)
  
            # nextButton = self.driver.find_elements(by=By.XPATH, value='//*[@id ="passwordNext"]')
            # nextButton[0].click()

            # passpwwordfield=self.find_loginfield("password")
            # passpwwordfield.send_keys(self.password)
            self.username = "NA"
            self.password = "NA"
            # try:
            #     self.find_loginfield("nextbutton2").click()
            # except AttributeError:
            #     self.find_loginfield("nextbutton2")[0].click()

        elif (mode == 3):
            #TODO 2Factorauthentification is not implemented at the moment
            pass
        else:
            raise EnterPasswordModeDoesNotExists(
                "The mode: " + mode + "  does not exist.")




from ...system.system_manager import WindowManager

class LoginToApplication(LoginTo):
    # useWindowClass is an option which enables to use the windowclass instead of the windowtitle
    def __init__(self,username,password,applicationname,useWindowClass=False):
        self.username = username
        self.password = password
        self.applicationname=applicationname

    def login(self):
        applicationWindows = WindowManager.get_windows_with_name(self.applicationname)
        for window in applicationWindows:
            WindowManager.set_active_window(window)
            # print(self.findLoginField())

    def find_loginfield(self,target):
        # SystemManager.scanForImage
        ...
        # return pyautogui.locateOnScreen('../images/Loginfields/'+self.applicationname+"/window.img")

    def login_to(self):
        if (SystemManager.scan_for_image('images/Loginfields/Steam/window.png') is not None):
            self.insert_to_inputfield(SystemManager.scan_for_image('images/Loginfields/Steam/username.png',accuracyModifier=0.8),"passwordmanagertest")
            self.insert_to_inputfield(SystemManager.scan_for_image('images/Loginfields/Steam/password.png',accuracyModifier=0.8),"mein13\"$")
            Keyboard.press_return()



