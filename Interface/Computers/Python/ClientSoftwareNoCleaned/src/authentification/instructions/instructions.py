# Version alloes the parser later to decide if it can read them or not 
# (the instructions are normaly saved in a database in the cloud, or on the passwordmanager. That enables that all instructions can be taken with the software) 
# TODO maybe introduce an save result propertie? And save it in the as object varible "lastresult" and when the instruction does not return anything then just set it none
from ctypes.wintypes import tagRECT


VERSION = "0.0.1"

from enum import Enum
class Instructiontype(Enum):
    SYSTEM = 1
    BROWSER = 2
    APP = 3

class Instruction(Enum):
    EMPTY = 0
    SLEEP = 1
    OPEN = 2
    CLICK = 3
    SEND_KEYS = 4
    MOVE_TO = 5
    FIND_IMAGE = 6

class InstructionLabel(Enum):
    INSTRUCTION = 0
    INSTRUCTIONTYP = 1
    TARGET = 2
    TARGETINFO = 3
    PLACEHOLDER = 4
    # Used when more then one instruction is needed
    # e.g. search for image and perform an action with it
    EMBEDDEDINSTRUCTION = 5
     


# This class is used for the auto login and password updater related stuff
class Instructions():
    def __init__(self,instruction_type,additional_informations="NA"):
        self.index = 0
        self.saved_instructions={}
        # This entry is usfull for the instruction interpreter
        self.saved_instructions["Info"]={
            "Version": VERSION,
            "Instructiontype":instruction_type,
            "Additionalinformations":additional_informations
        }

    def get_instruction_count(self):
        return self.index

    # instruction_typ: typ does name the catogory how they are used. It is possible to use system, browser and app
    # instruction: What should be done with it? 
    # executetion_mode: defines what # Not needed?
    # target: Defines where the target is located (xPath and  id are supported)
    # target_info: Defines additional information like when there are more targets located with the target parameter. Then use the specified index
    # placeholder: This is used when the instruction needs an additional parameter to execute it but this parameter is dynamic (e.g passwords which can get changed)
    def add(self,instruction_typ,instruction,target,target_info="",placeholder="",embedded_instruction=None):
        self.saved_instructions[self.index] = self.create_instruction(instruction_typ,instruction,target,target_info,placeholder,embedded_instruction)
        self.index+=1

    def create_instruction(self,instruction_typ,instruction,target="",target_info="",placeholder="",embedded_instruction=None) -> dict:
        instruction={
                        InstructionLabel.INSTRUCTIONTYP:instruction_typ,
                        InstructionLabel.INSTRUCTION:instruction,
                        InstructionLabel.TARGET:target,
                        InstructionLabel.TARGETINFO:target_info,
                        InstructionLabel.PLACEHOLDER:placeholder,
                        InstructionLabel.EMBEDDEDINSTRUCTION:embedded_instruction
        }
        return instruction


    # TODO Reevaluate if this methode makes sense
    # def remove(self,intruction_number):
    #     del self.saved_instructions[intruction_number]
    #     pass

    # Use an index (number) to get the instruction with the same index
    # Or use "Info" to get the informations about this instruction set
    # If the index does not exits it raises and KeyError Exception
    def get(self,instruction_number):
        if instruction_number in self.saved_instructions:
            return self.saved_instructions[instruction_number]
        else:
            raise KeyError("The index does not exist in the instructionset")

    def execute(self,use_function):
        for i in range(self.index):
            try:
                use_function(self.get(i))
            except KeyError:
                # Do noting since the key does not exits
                pass