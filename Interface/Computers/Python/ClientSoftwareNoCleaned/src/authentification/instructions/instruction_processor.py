from sre_constants import NOT_LITERAL
from ..instructions.instructions import Instructions,Instructiontype,Instruction,InstructionLabel
from selenium import webdriver
import geckodriver_autoinstaller
import time
from selenium.webdriver.common.by import By
from selenium import webdriver
from ...device.human_interface_device import Mouse,Keyboard,ClickType
from ...system.system_manager import SystemManager
# TODO Check if it makes sense to change this to a singleton

class InstructionProcessor(object):
    # Singelton
    # def __new__(cls):
    #     if ( not hasattr(cls, 'instance')):
    #         cls.instance = super(InstructionProcessor, cls).__new__(cls)
    #         return cls.instance

    def __init__(self):
        pass

        

    # def close(self):
    #     try:
    #         self.driver.close()
    #     except:
    #         pass

    def decide_source_type(target:str):
        if (target.startswith('/')):
            return By.XPATH
        else:
            return By.ID

    def process_instructionset(instructionset:Instructions,placeholders=None,driver:webdriver=None) -> int:        
        if ( instructionset != None):
            try:
                if(driver == None):
                    try:
                        from selenium.webdriver.firefox.options import Options     
                        geckodriver_autoinstaller.install()
                        options = Options()
                        # options.add_argument("--headless")
                        driver = webdriver.Firefox(options=options)
                    except Exception: # TODO Get the real exception when the browser is not found
                        print("Browser not found")
                        return 1
                instruction_count = instructionset.get_instruction_count()
                # instruction_count = 8
                if ( placeholders != None ):
                    for i in range(instruction_count):
                        if ( instructionset.get(i)[InstructionLabel.PLACEHOLDER] != None and placeholders[instructionset.get(i)[InstructionLabel.PLACEHOLDER]] ):
                            instructionset.get(i)[InstructionLabel.PLACEHOLDER] = placeholders[instructionset.get(i)[InstructionLabel.PLACEHOLDER]]
                # instructionset.execute(InstructionProcessor.process_instruction)  
                for i in range(instructionset.get_instruction_count()):
                    InstructionProcessor.process_instruction(instructionset.get(i),driver)
                # Password was changed successfull
                return 0
            # Something did got wrong 
            # TODO Add custom exceptions
            except ModuleNotFoundError:
                return 1
        else:
            print("No instructions were passed")
            return 1

    # The driver option is only nessesary when the browser is used
    def process_instruction(instruction,driver:webdriver=None):
        if(driver == None):
            try:
                from selenium.webdriver.firefox.options import Options
                
                geckodriver_autoinstaller.install()
                options = Options()
                # options.add_argument("--headless")
                driver = webdriver.Firefox(options=options)
            except Exception: # Get the real exception when the browser is not found
                print("Browser not found")
        if ( instruction[InstructionLabel.INSTRUCTIONTYP] == Instructiontype.SYSTEM):
            if ( instruction[InstructionLabel.INSTRUCTION] == Instruction.SLEEP):
                time.sleep(instruction[InstructionLabel.TARGETINFO])
        elif ( instruction[InstructionLabel.INSTRUCTIONTYP] == Instructiontype.BROWSER):
            if( instruction[InstructionLabel.INSTRUCTION] == Instruction.OPEN):
                driver.get(instruction[InstructionLabel.TARGET])
            elif (instruction[InstructionLabel.INSTRUCTION] == Instruction.CLICK or instruction[InstructionLabel.INSTRUCTION] == Instruction.SEND_KEYS ):
                requested_element = driver.find_elements(by=InstructionProcessor.decide_source_type(instruction[InstructionLabel.TARGET]), value=instruction[InstructionLabel.TARGET])
                if ( instruction[InstructionLabel.INSTRUCTION] == Instruction.CLICK ):
                    # TODO check for type error
                    if ( instruction[InstructionLabel.TARGETINFO] == None ):
                        requested_element.click()
                    else:
                        requested_element[instruction[InstructionLabel.TARGETINFO]].click()
                if ( instruction[InstructionLabel.INSTRUCTION] == Instruction.SEND_KEYS ):
                    # TODO check for type error
                    if ( instruction[InstructionLabel.TARGETINFO] == None):
                        requested_element.send_keys(InstructionLabel.PLACEHOLDER)
                    else:
                        requested_element[instruction[InstructionLabel.TARGETINFO]].send_keys(instruction[InstructionLabel.PLACEHOLDER])
        elif ( instruction[InstructionLabel.INSTRUCTIONTYP] == Instructiontype.APP):
            if( instruction[InstructionLabel.INSTRUCTION] == Instruction.OPEN):
                pass
            elif (instruction[InstructionLabel.INSTRUCTION] == Instruction.CLICK ):
                if ( instruction[InstructionLabel.TARGET] != None):
                    x_pos,y_pos = InstructionProcessor.string_to_pos(instruction[InstructionLabel.TARGET])
                    Mouse.move_and_click(type=instruction[InstructionLabel.TARGETINFO],x_pos=x_pos,y_pos=y_pos)
                else:
                    Mouse.click(type=instruction[InstructionLabel.TARGETINFO])

                    
            elif (instruction[InstructionLabel.INSTRUCTION] == Instruction.MOVE_TO ):
                x_pos,y_pos = InstructionProcessor.string_to_pos(instruction[InstructionLabel.TARGET])
                if ( instruction[InstructionLabel.TARGETINFO] != None):
                    Mouse.move(x_pos=x_pos,y_pos=y_pos,relative=instruction[InstructionLabel.TARGETINFO])
                else:
                    Mouse.move(x_pos=x_pos,y_pos=y_pos)

            elif (instruction[InstructionLabel.INSTRUCTION] == Instruction.SEND_KEYS ):
                if ( instruction[InstructionLabel.TARGET] != None):
                    x_pos,y_pos = InstructionProcessor.string_to_pos(instruction[InstructionLabel.TARGET])
                    Mouse.move(x_pos=x_pos,y_pos=y_pos)
                    Keyboard.type(instruction[InstructionLabel.TARGETINFO])
                else:
                    Keyboard.type(instruction[InstructionLabel.TARGETINFO])

            elif (instruction[InstructionLabel.INSTRUCTION] == Instruction.FIND_IMAGE ):
                accuracyModifier,timeout,delay = InstructionProcessor.string_to_image_scan_data(instruction[InstructionLabel.TARGETINFO])
                
                x_pos,y_pos = SystemManager.scan_for_image(instruction[InstructionLabel.TARGET],accuracyModifier=accuracyModifier,timeout=timeout,delay=delay)
                instruction[InstructionLabel.EMBEDDEDINSTRUCTION][InstructionLabel.TARGET]=str(x_pos)+":"+str(y_pos)
                InstructionProcessor.process_instruction(instruction[InstructionLabel.EMBEDDEDINSTRUCTION])


    def string_to_image_scan_data(image_data_string:str)->tuple[float,float,float,bool]:
        # Seperate string in relevant informaitons
        accuracyModifier=""
        timeout=""
        delay=""
        informations = image_data_string.split()
            
        for information in informations:
            print(information)
            # Evaluate the data
            identifier = information.partition(':')[0]
            match (identifier): 
                case 'A':
                    accuracyModifier = float(information.partition(':')[2])
                case 'T':
                    timeout = float(information.partition(':')[2])
                case 'D':
                    delay = float(information.partition(':')[2])
                case 'W':
                    wait = bool(information.partition(':')[2])

        return accuracyModifier,timeout,delay

    def string_to_pos(pos_string:str)->tuple[int,int]:
        x_pos = pos_string.partition(':')[1]
        y_pos = pos_string.partition(':')[2]
        print("X:",x_pos," Y: ",y_pos)
        return x_pos,y_pos