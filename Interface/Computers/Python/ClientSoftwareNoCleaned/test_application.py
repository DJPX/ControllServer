
from src.tests.test_instructions import TestInscructions
from src.tests.test_login_to import TestLoginTo
from src.tests.testclass import TestClass


class TestApplication(TestClass):

    def __init__(self):
        super().__init__()
        # Add here all test classes
        login = TestLoginTo()
        self.add(login.test)

if __name__ == '__main__':
    test_app = TestApplication()
    test_app.test()