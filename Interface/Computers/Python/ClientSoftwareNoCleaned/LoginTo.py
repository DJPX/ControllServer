from abc import ABC, abstractmethod
from email.mime import application
from ClientSoftwareWorking.System.SystemManager import WindowManager
import pyautogui

class LoginTo(ABC):
    def __init__(self,username,password):
        self.username = username
        self.password = password

    @abstractmethod
    def login(self):
        pass

    @abstractmethod
    def findLoginField(self):
        pass



class LoginToBrowser(LoginTo):
    def __init__(self,username,password,browsername):
        self.username = username
        self.password = password
        self.browsername = browsername

    def setBrowsername(self,browsername):
        self.browsername = browsername
        return self

    def getBrowsername(self):
        return self.browsername

    def login(self):
        WindowManager.setActiveWindow(self.browsername)

    def setWebside(self,websideaddress):
        self.websideaddress = websideaddress
        return self

    def getWebside(self):
        return self.websideaddress

    def findLoginField(self):
        pass


class LoginToApplication(LoginTo):
    def __init__(self,username,password,applicationname):
        self.username = username
        self.password = password
        self.applicationname=applicationname

    def login(self):
        applicationWindows = WindowManager.getWindowsWithName(self.applicationname)
        for window in applicationWindows:
            WindowManager.setActiveWindow(window)
            print(self.findLoginField())

    def findLoginField(self):
        return pyautogui.locateOnScreen('images/Loginfields/'+self.applicationname+".img")