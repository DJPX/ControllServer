import serial.tools.list_ports
import time
from multiprocessing import Process
# from PasswordManagerCommunication import PasswordManagerCommunication
from SerialCommunication import SerialCommunication

foundPasswordmanagerPort = False


def testForPing(message):
    if message == "[PING]":
        foundPasswordmanagerPort = True
        print("Found passwortmanager")
        return 0


def testPorts(testport):
    serialComunication = SerialCommunication(testport, "115200", 8, 2)
    while True:
        serialComunication.startListening(testForPing)
        serialComunication.sendMessage("[PING]")
        time.sleep(0.1)


def findPort():
    print("Start scanning ports.")

    # comPorts=SerialCommunication.listPorts()
    comPorts = [comport.device for comport in serial.tools.list_ports.comports()]

    print("Found "+str(len(comPorts)) + " Ports.")

    for port in comPorts:
        print(f'{port} gets scanned.')
        proc = Process(target=testPorts, args=(
            port,), name='Find_Passwordmanager')
        proc.start()
        proc.join(timeout=5)
        proc.terminate()
        if proc.exitcode is None:
            print(f'{proc} reached timeout. No answer.')
        if proc.exitcode == 0:
            print(f'{proc} received answer from passwortmanager.')
            return port
    return "None"


if __name__ == '__main__':
    port = findPort()
    print(port)
    while port == "None":
        if port != "None":
            pManager = PasswordManagerCommunication("mode")
            pManager.login("Admin")
        else:
            port = findPort()
            time.sleep(1)
