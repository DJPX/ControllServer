from __future__ import annotations
from kivy.lang import Builder
from kivymd.uix.screen import MDScreen
from shared_app_resources import SharedAppResources
login_screen='''
GridLayout:
    padding: "10dp"
    cols : 1

    BoxLayout:


        MDLabel:
            id: welcome_label
            text: "WELCOME"
            font_size: 40
            halign: 'center'
            size_hint_y: None
            height: self.texture_size[1]
            padding_y: 15
            pos_hint: {"center_x": 0.5,"center_y": 0.5}

    GridLayout:
        cols: 1
        halign: 'center'
        FloatLayout:
            padding: "1dp"
            halign: 'center'
            pos_hint: {"center_x": 0.5,"center_y": 0.5}
            MDTextField:
                id: user
                hint_text: "username"
                icon_right: "account"
                size_hint_x: None
                width: 200
                font_size: 18
                pos_hint: {"center_x": 0.5,"center_y": 0.5}
                halign: 'center'
        FloatLayout:
            padding: "1dp"
            halign: 'center'
            pos_hint: {"center_x": 0.5,"center_y": 0.5}
            MDTextField:
                id: password
                hint_text: "password"
                icon_right: "eye-off"
                size_hint_x: None
                width: 200
                font_size: 18
                pos_hint: {"center_x": 0.5,"center_y": 0.5}
                password: True
                halign: 'center'
        FloatLayout:
            padding: "1dp"
            halign: 'center'
            MDRoundFlatButton:
                text: "LOG IN"
                font_size: 12
                pos_hint: {"center_x": 0.5,"center_y": 0.8}
                on_press: root.parent.check_login()  
                halign: 'center'  


         

'''
from typing import TYPE_CHECKING
from MyScreenManager import MyScreenManager

class LoginScreen(MDScreen):

    def __init__(self,screenmanager:MyScreenManager,url:str, **kw):
        super().__init__(**kw)
        self.url = url
        self.screenmanager = screenmanager
        self.add_widget(Builder.load_string(login_screen))
        self.login = True
        # layout = MDGridLayout()
        # layout.cols = 1
        # # Create label
        # MDLabel:
        #     id: welcome_label
        #     text: "WELCOME"
        #     font_size: 40
        #     halign: 'center'
        #     size_hint_y: None
        #     height: self.texture_size[1]
        #     padding_y: 15


        # # Create input fields

        # self.width
       
    def start_server_if_not_avaible(self):
        from wakeonlan import send_magic_packet
        send_magic_packet('88.d7.f6.c4.f9.e1')

    
    def check_login(self):
        from server.controll_server import ControllServer
        server = ControllServer(self.url)
        if server.is_server_running == False:
            self.start_server_if_not_avaible()
            
        if self.login == True:
            self.screenmanager.change_screen("services")
        return self.login