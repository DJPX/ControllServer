from tokenize import String
from kivy.uix.screenmanager import Screen
from kivymd.uix.dialog import MDDialog
from kivymd.uix.button import MDFlatButton
from kivymd.uix.selectioncontrol import MDSwitch
from kivymd.uix.gridlayout import MDGridLayout
from kivymd.uix.boxlayout import MDBoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.popup import Popup
from kivy.uix.textinput import TextInput
from kivymd.uix.label import MDLabel

from server.controll_server import ControllServer
from MyScreenManager import MyScreenManager
from kivy.lang import Builder

import kivy.graphics.texture  

# class ServiceEntry()

class ServicesScreen(Screen):

    def __init__(self,screenmanager:MyScreenManager,url:str, **kw):
        super().__init__(**kw)
        self.screenmanager = screenmanager
        self.server = ControllServer(url)
        layout = MDBoxLayout(orientation = 'vertical')
        screenlabel = MDLabel(text="Services")
        screenlabel.halign = "center"
        layout.add_widget(screenlabel)
        self.services = self.server.get_all_servicenames()

        if self.services == None:
            noservice_label = MDLabel(text="Could not connect to service controller")
            noservice_label.halign = "center"
            layout.add_widget(noservice_label)
        if self.services == "ControllServer" :
            noservice_label = MDLabel(text="No services")
            noservice_label.halign = "center"
            layout.add_widget(noservice_label)
        else:
            self.services = self.services.split(",")
            for service in self.services:
                if service != "ControllServer":
                    layout.add_widget(self.create_service_entry(service))
        



        self.add_widget(layout)

    def create_service_entry(self,servicename:str):
        status = self.server.status(servicename)
        layout = MDGridLayout(cols=5)
        servicename_label = Label(text=servicename,markup=True)
        servicename_label.size_hint_min_x =200
        #.bind(texture=ServicesScreen.__calculate_height_callback)
        layout.add_widget(servicename_label)
        status_text = Label(text=status,markup=True)
        status_text.size_hint_min_x =200
        layout.add_widget(status_text)
        layout.add_widget(Button(text="Start",size_hint_x=None,on_release=self.start_service))
        layout.add_widget(Button(text="Stop",size_hint_x=None,on_release=self.stop_service))
        layout.add_widget(Button(text="Restart",size_hint_x=None,on_release=self.restart_service))
        return layout

    def start_service(self,obj):
        gridlayout=self.find_parent_type_of_widget("kivymd.uix.gridlayout.MDGridLayout",obj)
        self.server.start(gridlayout.children[4].text)

    def stop_service(self,obj):
        gridlayout=self.find_parent_type_of_widget("kivymd.uix.gridlayout.MDGridLayout",obj)
        self.server.stop(gridlayout.children[4].text)
    
    def restart_service(self,obj):
        gridlayout=self.find_parent_type_of_widget("kivymd.uix.gridlayout.MDGridLayout",obj)
        self.server.restart(gridlayout.children[4].text)
    def find_parent_type_of_widget(self,parentType,widget):
        searchedParent=widget.parent
        while (str(type(searchedParent)).find(parentType) == -1):
            searchedParent = searchedParent.parent
        return searchedParent
    # # Load services
    # def load_services(self):
    #     self.services = self.server.get_all_servicenames()
    #     if(self.services != None):
    #         if self.services != "":
    #             self.services = self.services.split(',')
    #             for service in self.services:
    #                 self.create_and_append_service_entry(service)
    #     else:
    #         print("Something went wrong or there are no services")
    #         # TODO error handling


    
    # def on_enter(self):
    #     self.load_services()
    #     layout = FloatLayout(size_hint_y=None)
    #     # layout.add_widget(Button(text="Add new password",size_hint_y=None,pos_hint={'center_x':0.5,'center_y':0.5},on_release=self.create_service_entry_dialog))
    #     self.add_widget(layout)

    
    # def close_dialog(self,obj):
    #     # Search in the parents for a dialog element
    #     currentDialog=obj.parent
    #     while (str(type(currentDialog)).find("kivymd.uix.dialog.MDDialog") == -1):
    #         currentDialog = currentDialog.parent
    #     # Close the dialog
    #     currentDialog.dismiss()
    #     self.remove_widget(currentDialog)


    # def create_and_append_service_entry(self,servicename):      
    #     self.ids[servicename]= self.create_service_entry(servicename)
    #     self.ids.passwordContainer.add_widget(self.ids[servicename])



    # def create_service_entry(self,servicename):
    #     layout = BoxLayout(orientation= 'horizontal',spacing=10)
    #     # Save IDs of buttons
    #     serviceentry= {
    #         "Layout":layout,
    #         "Servicename":Label(text=servicename,markup=True,size_hint_x=None),
    #         "Spacer":Label(size_hint_x=5),
    #         "Status":MDSwitch(pos_hint= {'center_x': .5, 'center_y': .5},size_hint_x=None),
    #         "Start": Button(text="Start",size_hint_x=None,on_release=self.server.start(servicename)),
    #         "Stop": Button(text="Stop",size_hint_x=None,on_release=self.server.stop(servicename)),
    #         "Restart": Button(text="Restart",size_hint_x=None,on_release=self.server.restart(servicename)),
    #         # "Spacer3":Label(size_hint_x=5)
    #     }
    #     # Add buttons to the screen
    #     for key in serviceentry:
    #         if (key != "Layout"):
    #             layout.add_widget(serviceentry[key])
    #     self.services[servicename]=serviceentry
    #     return layout

    # def find_parent_type_of_widget(self,parentType,widget):
    #     searchedParent=widget.parent
    #     while (str(type(searchedParent)).find(parentType) == -1):
    #         searchedParent = searchedParent.parent
    #     return searchedParent

    # def user_request_status_change(self,obj):
    #     ...
