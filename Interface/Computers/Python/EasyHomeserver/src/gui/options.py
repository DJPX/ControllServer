

options_screen='''

BoxLayout:
    orientation : 'horizontal'

    MDLabel:
        id: welcome_label
        halign: 'center'
        size_hint_y: None
        height: self.texture_size[1]
        padding_y: 15
        pos_hint: {"center_x": 0.5,"center_y": 0.5} 

'''
from kivy.lang import Builder
from kivymd.uix.screen import MDScreen
from MyScreenManager import MyScreenManager
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.label import MDLabel
from kivymd.uix.button import MDRectangleFlatButton

from enum import Enum
class OptionType(Enum):
    TEXTFIELD=1,
    CHECKBOX=2,
    DROPDOWN=3,
    PASSWORDFIELD=4

from dataclasses import dataclass
@dataclass
class Option():
    def __init__(self,optionname:str,value:str,optiontype:OptionType,on_change_callback=None) -> None:
        self.optionname = optionname
        self.value = value
        self.optiontype = optiontype
        self.on_change_callback = on_change_callback

from functools import partial
class OptionScreen(MDScreen):
    def __init__(self,screenmanager:MyScreenManager, **kw):
        super().__init__(**kw)
        self.layout = MDBoxLayout(orientation = 'vertical')
        self.optionfile_path = "demofile.txt"
        self.optionlist = {
        "Serveraddress":Option("Serveraddress","NA",OptionType.TEXTFIELD),
        "Serverlogin":Option("Serverlogin","NA",OptionType.TEXTFIELD),
        "Serverpassword":Option("Serverpassword","NA",OptionType.PASSWORDFIELD),
        "Controlleraddress":Option("Controlleraddress","NA",OptionType.TEXTFIELD),
        "Controllerlogin":Option("Controllerlogin","NA",OptionType.TEXTFIELD),
        "Controllerpassword":Option("Controllerpassword","NA",OptionType.PASSWORDFIELD),
        # "Controllerpassword":Option("Controllerpassword","NA",OptionType.PASSWORDFIELD,partial(self.textinput_callback, "SADGFSHD", 'my keSFDy')),
        }
        self.load_options()
        save_button = MDRectangleFlatButton(text="Save")
        save_button.bind(on_press=self.save_options)
        # button = Button(text='Hello world', font_size=14)

        save_button.halign = "center"
        
        self.add_widget(self.layout)
        self.add_widget(save_button)

    def save_options(self,button:MDRectangleFlatButton):
        
        elements= button.parent.children[1].children
        # delete content of config file
        config_file = open(self.optionfile_path, 'r+')
        config_file.truncate(0)
        config_file = open(self.optionfile_path, "w")
        for element in elements:
            self.optionfile_path
            config_file.write(element.children[1].text+":"+element.children[0].text+"\n")
        print("Config saved")


    def on_checkbox_Active(self, checkboxInstance, isActive):
        if isActive:
            print("Checkbox Checked")
        else:
            print("Checkbox unchecked")
    
    def textinput_callback(self,value, key, *largs):
        print(value)
        print(key)

    def on_text(self,instance, value):
        print('The widget', instance, 'have:', value)

    def load_options(self):
        try:
            config_file = open(self.optionfile_path, "rt")
            for line in config_file.readline():
                optionname,value = line.split(':')
                self.optionlist[optionname].value=value

        except:
            print("Config file does not exist, creating one")
            self.create_config()
        for key in self.optionlist:
            self.layout.add_widget( self.create_option(self.optionlist[key]))

        
        

        


    def create_config(self):
        config_file = open(self.optionfile_path, "wt")
        for optionname in self.optionlist:
            config_file.write(optionname+':'+self.optionlist[optionname].value)


    
    def create_option(self,option:Option):
        return self.create_option_with_variables(option.optionname,option.value,option.optiontype,option.on_change_callback)

    def create_option_with_variables(self,optionname:str,value:str,type:OptionType,callback_on_change):
        layout = MDBoxLayout(orientation = 'horizontal')
        optionlabel = MDLabel(text=optionname)
        optionlabel.halign = "center"
        # optionlabel.height= optionlabel.font_size+2
        layout.add_widget(optionlabel)
        
        match type:
            case OptionType.CHECKBOX:
                from kivy.uix.checkbox import CheckBox
                checkBox = CheckBox()
                checkBox.bind(active = callback_on_change)
                if(value== "active"):
                    checkBox.active = True
                else:
                    checkBox.active = False
                layout.add_widget(checkBox)
            case OptionType.PASSWORDFIELD | OptionType.TEXTFIELD:
                from kivy.uix.textinput import TextInput
                
                textinput = TextInput(text=value)
                textinput.multiline=False
                textinput.halign = "center"
                if callback_on_change != None:
                    textinput.bind(text=callback_on_change)
                    # textinput.bind(on_text_validate= callback_on_change)
                    textinput.bind(text_validate_unfocus= callback_on_change)
                # textinput.height =optionlabel.font_size+2
                if type == OptionType.PASSWORDFIELD: 
                    textinput.password=True
                
                layout.add_widget(textinput)
            case OptionType.DROPDOWN:
                ...
            case _:
                print("Type is not supported")
            
        return layout
            
            

    


            
            






    

