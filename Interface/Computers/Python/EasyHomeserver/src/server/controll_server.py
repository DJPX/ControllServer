from service.service import Service
from service.service import Status

from communication.communication import WebCommunication
class ControllServer:
    def __init__(self,url):
        self.url = url
        self.avaible_services = {}

    def register_service(self,servicename:str):
        params = {'name': servicename, 'status': Status.OFFLINE.value}
        request_url = self.url+"/service"
        webcom = WebCommunication(request_url, 'post')
        response = webcom.send_message(params)
        return response.text

    def start(self,servicename:str):
        if self.check_before_action(servicename):
            self.avaible_services[servicename].start()

    def stop(self,servicename:str):
        if self.check_before_action(servicename):
            self.avaible_services[servicename].stop()

    def restart(self,servicename:str):
        if self.check_before_action(servicename):
            self.avaible_services[servicename].restart()

    def status(self,servicename:str):
        if self.check_before_action(servicename):
            return self.avaible_services[servicename].status()
        else:
            return "Status "+ servicename+ " does not exists."

    def start_all(self):
        self.check_before_action()
        for service  in self.avaible_services:
            self.start(service)

    def stop_all(self):
        self.check_before_action()
        for service  in self.avaible_services:
            self.stop(service)

    def restart_all(self):
        self.check_before_action()
        for service  in self.avaible_services:
            self.restart(service)

    def status_all(self):
        self.check_before_action()
        statusreport =""
        for service  in self.avaible_services:
            statusreport +=service +" "+ self.status(service)+"\n"
        return statusreport

    def is_running(self,servicename:str):
        self.check_before_action(servicename)
    
    def is_server_running(self):
        if(self.get_all_servicenames() == None):
            return False
        else:
            return True
        

    # Updates the services and it checks optional if service exisits
    def check_before_action(self,servicename:str="")->bool:
        self.update_avaible_services()
        # Check if service exists
        if servicename != "":
            if servicename in self.avaible_services:
                # Service is avaible action can get executed
                return True
            else:
                return False


    
    def get_all_servicenames(self):
        request_url = self.url+"/service/getServices"
        webcom = WebCommunication(request_url,'get')
        response = webcom.send_message("")
        if response != 'Error Could not connect':
            return response.text
        else:
            return None

    def update_avaible_services(self):
        # Reset avaible_services
        self.avaible_services = {}
        services = self.get_all_servicenames()
        if services != None:
            services.split(",")
            for service in services:
                self.avaible_services[service] = Service(service,self.url)
        return services        


        