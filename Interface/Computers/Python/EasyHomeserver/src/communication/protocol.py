# from .communication import Communication
# from .messagetype import MessageType
# import json
# from enum import Enum


# class DeviceStatus(Enum):
#     ERROR = -2
#     UNKNOWN = -1
#     NOTCONNECTED = 0
#     INIT = 1
#     CONNECTED = 2
#     NOTREADY = 3
#     READY = 4


# class Device:
#     def __init__(self, device_name, device_type, status, connection_type, stage=0):
#         self.device_name = device_name,
#         self.device_type = device_type,
#         self.status = status,
#         self.connection_type = connection_type,
#         self.stage = stage,
#         self.device_data = ""


# class PasswordmanagerProtocol:

#     def __init__(self, communication: Communication):
#         self.password = "NoPassword"
#         self.communication = communication
#         # None means that the protocol waas not executed
#         self.successful = None
#         self.currentMessageNumber = 0
#         self.lastMessageNumber = 0
#         self.this_device = Device("", "", DeviceStatus.UNKNOWN, "Serial", 0)
#         self.connected_device = Device(
#             "", "", DeviceStatus.UNKNOWN, "Serial", 0)
#         self.received_messagebuffer={}

#     def set_password(self,password):
#         self.password = password

#     def get_message_from_buffer(self,messagenumber:int):
#         message = self.received_messagebuffer[messagenumber]
#         if message != None:
#             del self.received_messagebuffer[messagenumber]
#         return message


#     def protocol(self, message,send:bool=True,shared_message_storage=None):
#         # Send custom message when init protocol was succesful
#         if self.successful:
#             self.currentMessageNumber = self.currentMessageNumber+1
#             if send == True:
#                 message["MessageNumber"]= self.currentMessageNumber
#                 if message["MessageType"] == MessageType.REQUEST.value:
#                     # Reserve space for incomming respones message
#                     if shared_message_storage != None:
#                         shared_message_storage[self.currentMessageNumber] == None
#                 elif message["MessageType"] == MessageType.ACK.value:
#                     pass
#                 return self.currentMessageNumber
#             else:
#                 #Save message in the message buffer with the accourding messagenumber
#                 self.received_messagebuffer[message["MessageNumber"]-1]=message

#         # First ping message
#         # and (self.this_device.status == DeviceStatus.UNKNOWN or self.this_device.status == DeviceStatus.NOTCONNECTED):
#         if message == None:
#             self.currentMessageNumber += 1
#             document = {
#                 "MessageType": MessageType.PING.value,
#                 "MessageNumber": self.currentMessageNumber,
#                 "Stage": self.this_device.stage,
#                 "Device": "PasswordmanagerClient",
#                 "ConnectionType": self.this_device.connection_type,
#                 "LastMessageNumber": self.lastMessageNumber
#             }
#         else:
#             if message["LastMessageNumber"] != self.currentMessageNumber:
#                 # TODO implement
#                 # Error not the currect message number
#                 # Reset connection
#                 return None
#             else:
#                 self.currentMessageNumber += 1
#                 self.lastMessageNumber = message["LastMessageNumber"]
#                 self.connected_device.stage = message["Stage"]

#                 # Ping stage
#                 if message["MessageType"] == MessageType.PING.value:
#                     # Set data
#                     self.this_device.stage = self.connected_device.stage+1
#                     self.connected_device.status = DeviceStatus.NOTCONNECTED

#                     # Response pingmessage 1
#                     if message["Stage"] == 1:
#                         # Extract data
#                         self.connected_device.connection_type = message["ConnectionType"]
#                         self.connected_device.device_name = message["Device"]
#                         self.connected_device.stage = message["Stage"]

#                         allow_connection = False
#                         if self.this_device.connection_type == self.connected_device.connection_type:
#                             allow_connection = True

#                         # Create answer message
#                         document = {
#                             "MessageType": MessageType.PING.value,
#                             "MessageNumber": self.lastMessageNumber,
#                             "Stage": self.this_device.stage,
#                             "Device": "PasswordmanagerClient",
#                             "ConnectionType": self.this_device.connection_type,
#                             "AllowConnection": allow_connection,
#                             "LastMessageNumber": self.lastMessageNumber
#                         }
#                     # Response pingmessage 2
#                     elif message["Stage"] == 2:
#                         # Extract data
#                         self.connected_device.connection_type = message["ConnectionType"]
#                         self.connected_device.device_name = message["Device"]
#                         self.connected_device.stage = message["Stage"]

#                         allow_connection = False
#                         if self.this_device.connection_type == self.connected_device.connection_type:
#                             allow_connection = True

#                         # Create answer message
#                         document = {
#                             "MessageType": MessageType.PING.value,
#                             "MessageNumber": self.currentMessageNumber,
#                             "Stage": self.this_device.stage,
#                             "AllowConnection": allow_connection,
#                             "LastMessageNumber": self.lastMessageNumber
#                         }
#                     # Response pingmessage 3
#                     elif message["Stage"] == 3:
#                         if message["AllowConnection"] == True:
#                             self.this_device.status = DeviceStatus.CONNECTED
#                             self.this_device.stage = 1

#                         self.connected_device.stage = message["Stage"]

#                         # Create answer message
#                         document = {
#                             "MessageType": MessageType.INIT.value,
#                             "MessageNumber": self.currentMessageNumber,
#                             "Stage": self.this_device.stage,
#                             "DeviceStatus": self.this_device.status.value,
#                             "LastMessageNumber": self.lastMessageNumber
#                         }
#                 # Init stage
#                 elif message["MessageType"] == MessageType.INIT.value:
#                     self.this_device.stage = self.connected_device.stage + 1
#                     # Response initmessage 1
#                     if message["Stage"] == 1:
#                         # Set data
#                         if message["AllowConnection"] == True:
#                             self.this_device.status = DeviceStatus.CONNECTED

#                         self.connected_device.stage = message["Stage"]

#                         # Create answer message
#                         document = {
#                             "MessageType": MessageType.INIT.value,
#                             "MessageNumber": self.currentMessageNumber,
#                             "Stage": self.this_device.stage,
#                             "DeviceStatus": self.this_device.status.value,
#                             "LastMessageNumber": self.lastMessageNumber
#                         }
#                     # Response initmessage 2
#                     elif message["Stage"] == 2:
#                         # Set data
#                         self.this_device.status = DeviceStatus.CONNECTED
#                         self.connected_device.stage = message["Stage"]

#                         # Create answer message
#                         document = {
#                             "MessageType": MessageType.INIT.value,
#                             "MessageNumber": self.currentMessageNumber,
#                             "Stage": self.this_device.stage,
#                             "DeviceData": "",
#                             "LastMessageNumber": self.lastMessageNumber
#                         }
#                     # Response initmessage 3
#                     elif message["Stage"] == 3:
#                         # Process device date
#                         # TODO
#                         # message["DeviceData"]

#                         # Set data according to DeviceData
#                         self.connected_device.status = DeviceStatus.READY
#                         self.this_device.status = DeviceStatus.READY
#                         self.connected_device.stage = message["Stage"]
#                         self.connected_device.device_data = message["DeviceData"]

#                         # Create answer message
#                         document = {
#                             "MessageType": MessageType.INIT.value,
#                             "MessageNumber": self.currentMessageNumber,
#                             "Stage": self.this_device.stage,
#                             "DeviceData": "",
#                             "LastMessageNumber": self.lastMessageNumber
#                         }

#                         # Protocol was successful
#                         self.successful = True
#                     # Response initmessage 4
#                     elif message["Stage"] == 4:
#                         # Process device date
#                         # TODO
#                         # message["DeviceData"]

#                         # Set data according to DeviceData
#                         self.connected_device.status = DeviceStatus.READY
#                         self.this_device.status = DeviceStatus.READY
#                         self.connected_device.stage = message["Stage"]
#                         self.connected_device.device_data = message["DeviceData"]
                        

#                         # Create answer message
#                         document = {
#                             "MessageType": MessageType.INIT.value,
#                             "MessageNumber": self.currentMessageNumber,
#                             "Stage": self.this_device.stage,
#                             "DeviceData": "",
#                             "LastMessageNumber": self.lastMessageNumber
#                         }

#                         # Protocol was successful
#                         self.successful = True
#         # Send data
#         json_string = json.dumps(document)
#         self.communication.send_json_message(json_string)
#         return self.currentMessageNumber