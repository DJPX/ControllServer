from kivy.clock import Clock
# from kivy.uix.screenmanager import ScreenManager, Screen
from kivymd.uix.screenmanager import MDScreenManager
from kivymd.uix.screen import MDScreen
from kivymd.uix.label import MDLabel
from kivymd.uix.textfield import MDTextField
from kivymd.uix.gridlayout import MDGridLayout
class FirstScreen(MDScreen):
    pass


class SecondScreen(MDScreen):
    pass


class ThirdScreen(MDScreen):
    pass


class FourthScreen(MDScreen):
    pass
from kivy.uix.button import Button
from kivy.lang import Builder
from kivymd.app import MDApp



class OptionScreen(MDScreen):

    def add_option(self,type):
        self.add_widget()

from typing import Dict
class MyScreenManager(MDScreenManager):
    
    def __init__(self, **kwargs):
        super(MyScreenManager, self).__init__(**kwargs)
        
        self.managed_screens: Dict[str,bool]={}
        # self.add_screen("_first_screen_")
        # Clock.schedule_once(self.screen_switch_one, 2)


    def add_screen(self,name:str,screen:MDScreen=None):
        if(screen != None):
            screen.name=name
            self.add_widget(screen)
        self.managed_screens[name]=screen
        

    def delete_screen(self,name):
        try:
            del self.managed_screens[name]
            self.remove_widget(name)
        except:
            print("Key does not exist.")

    def change_screen(self,name:str):
        try:
            if self.managed_screens[name] != None:
                # print("Screen does not exist")
                # return False
                self.current = name
            else:
                self.current_screen = self.managed_screens[name]
        except:
            print("Screen does not exists")

            

#     def screen_switch_one(self, dt):
#         self.change_screen( '_first_screen_')
#         # Clock.schedule_once(self.change_screen( '_Login_'), 2)
#         Clock.schedule_once(self.screen_switch_two, 2)
        

#     def screen_switch_two(self, dt):
#         self.change_screen( '_Login_')
#         # self.change_screen( '_second_screen_')
#         # self.ids.first_screen.ids.first_screen_label.text = "Hi I'm The Fifth Screen"
#         # Clock.schedule_once(self.screen_switch_three, 2)

#     def screen_switch_three(self, dt):
#         self.change_screen( '_third_screen_')
#         Clock.schedule_once(self.screen_switch_four, 2)

#     def screen_switch_four(self, dt):
#         self.change_screen( '_fourth_screen_')
#         Clock.schedule_once(self.screen_switch_one, 2)

# class SharedAppResources():
#     screen_manager = MyScreenManager()


# from gui.services import ServicesScreen
