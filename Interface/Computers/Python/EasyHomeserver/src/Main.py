# from kivy.lang import Builder
# from kivymd.app import MDApp
# screen_helper = """

# ScreenManager:
#     LoginScreen:
#     ServicesScreen:

    


# <LoginScreen>:
#     name: 'login'
#     MDTextField:
#         id: passphrase
#         password: True
#         hint_text: "Enter passpharse"
#         helper_text: "To unlock your device"
#         helper_text_mode: "on_focus"
#         pos_hint: {'center_x':0.5,'center_y':0.5}
#         size_hint_x:None
#         width:300
#         icon_right: "lock"
#         icon_right_color: app.theme_cls.primary_color
#     MDRectangleFlatButton
#         text: "Login"
#         pos_hint: {'center_x':0.5,'center_y':0.4}
#         on_release: root.check_login()


# <ServicesScreen>:
#     name: 'passwords'
#     MDNavigationLayout:
#         MDNavigationDrawer:
#             id: nav_drawer
#             BoxLayout:
#                 orientation: 'vertical'




#     # Contains password page
#     ScrollView:
#         size: self.size
#         GridLayout:
#             id: passwordContainer
#             cols:1
#             row_force_default:True
#             row_default_height:40
#             size_hint_y: None
#             height: self.minimum_height

#             BoxLayout:
#                 orientation: 'horizontal'
#                 spacing:10
#                 Label:
#                     id: labelServicename
#                     text:"     Servicename"
#                     markup:True
#                     size_hint_x:None
#                 Label:
#                     id: labelPasswordtype
#                     markup:True
#                     size_hint_x: None
#                 Label:
#                     size_hint_x:5
# """
# # from gui.login import LoginScreen
# # from gui.services import ServicesScreen
# from kivy.uix.dropdown import DropDown
# from kivy.uix.button import Button
# from kivy.base import runTouchApp
# from  access_app_data import AccessAppData


# class HomeServerApp(MDApp):
#     def build(self):
#         AccessAppData.init_data()
#         runTouchApp(self.service_screen())
#         # self.load_appstyle()

  
#         # screen = Builder.load_string(screen_helper)
#         # return screen

#     def service_screen(self):
#         avaible_servers = DropDown()
#         for managed_server in AccessAppData.managed_servers:
#             btn = Button(text =AccessAppData.managed_servers[managed_server].name, size_hint_y = None, height = 40)
#             btn.bind(on_release = lambda btn: avaible_servers.select(btn.text))
#             avaible_servers.add_widget(btn)
#         mainbutton = Button(text ='change server', size_hint =(None, None), pos =(350, 300))
#         mainbutton.bind(on_release = avaible_servers.open)
#         avaible_servers.bind(on_select = lambda instance, x: setattr(mainbutton, 'text', x))
#         return mainbutton


#         return avaible_servers



#     def load_appstyle(self):
#         self.theme_cls.primary_palette='Green'
#         self.theme_cls.primary_hue = "A700"
#         self.theme_cls.theme_style='Dark'



# from server.controll_server import ControllServer
# if __name__ == '__main__':
#     server = ControllServer("http://localhost:9010")
#     server.register_service("Testservice")
#     print(server.get_all_servicenames())
#     # print(server.status_all())
#     # server.start_all()
#     # print(server.status_all())
#     # server.restart_all()
#     # print(server.status_all())
#     # server.stop_all()
#     # print(server.status_all())

#     app = HomeServerApp()
#     app.run()
from kivymd.app import MDApp
from MyScreenManager import MyScreenManager
from gui.login import LoginScreen
from gui.services import ServicesScreen
from gui.options import OptionScreen
class HomeserverApp(MDApp):
    
    def build(self):
        self.theme_cls.theme_style = "Dark"
        self.theme_cls.primary_palette = "Green"
        # return MyScreenManager()
        self.screen_manager = MyScreenManager()
        self.screen_manager.add_screen('_Login_',LoginScreen(self.screen_manager,"http://localhost:9010"))
        self.screen_manager.add_screen('services',ServicesScreen(self.screen_manager,"http://localhost:9010"))
        
        self.screen_manager.add_screen('options',OptionScreen(self.screen_manager))
        self.screen_manager.change_screen('services')
        # return self.screen_manager
        return ServicesScreen(self.screen_manager,"http://localhost:9010")


from server.controll_server import ControllServer
# from wakeonlan import send_magic_packet 
if __name__ == "__main__":
    HomeserverApp().run()
    # send_magic_packet('88.d7.f6.c4.f9.e1')
