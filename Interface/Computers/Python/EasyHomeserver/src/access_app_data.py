from dataclasses import dataclass
@dataclass
class ServerData:
    name: str
    address: str
    # Optional only if authentification is nessesary
    username: str
    password: str
    # Opional if server has and controller which can shut it down and start it
    controller_address: str
    # Optional only if authentification is nessesary
    controller_username: str
    controller_password: str

    def __init__(self,name:str,address:str,username:str,password:str,controller_address:str="",controller_username:str="",controller_password:str="") -> None:
        self.name=name
        self.address = address
        self.username = username
        self.password = password
        self.controller_address = controller_address
        self.controller_username = controller_username
        self.controller_password = controller_password
    

class AccessAppData:
    managed_servers= {}
    app_password="pass"

    def init_data():
        for i in range(5):
            server = ServerData("server"+str(i),"123.123.123.132","nae","pass")
            AccessAppData.managed_servers[server.name]=server

    def __init__(self):
        ...

    