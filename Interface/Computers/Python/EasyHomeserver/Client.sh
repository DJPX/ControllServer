#!/bin/bash
# Call this file with . filename or source filename to run it in the current bash

# Create/Activate virtualEnviroment
function activateEnviroment {
  ENVIROMENTNAME="EasyHomeserver"
  if [ -d "$ENVIROMENTNAME" ]; then
    source "$ENVIROMENTNAME"/bin/activate
  else
    # Install depenencys
    sudo dnf install python3-pip
    pip3 install --user virtualenv 
    python3 -m virtualenv "$ENVIROMENTNAME"
    source "$ENVIROMENTNAME"/bin/activate
    pip3 install requests
    pip3 install pyinstaller

    # gui depedencys
    pip3 install kivy[full]
    pip3 install kivymd
    # Wake on lan
    pip install wakeonlan
  fi
}



if [ "$#" -ne 1 ]
then
  echo "No action specified please use start, run or stop as parameter"
 # echo "Please run this script with source filename or . filename"
else
  if [ "$1" == "start" ]
  then
    activateEnviroment
  elif [ "$1" == "stop" ]
  then
    deactivate
  elif [ "$1" == "run" ]
  then
    source Client.sh start
    clear
    python3 test.py
  elif [ "$1" == "remove" ]
  then
    rm -r client
  elif [ "$1" == "reinstall" ]
  then
    Client.sh remove
    source Client.sh start


  fi
fi





