import { Component, OnInit } from '@angular/core';
import { ManageServicesService } from '../manage-services.service';

@Component({
  selector: 'app-manage-service',
  templateUrl: './manage-service.component.html',
  styleUrls: ['./manage-service.component.css']
})
export class ManageServiceComponent implements OnInit {
  servicename="Service";
  online=true;
  currentstatus=0;
  services = [ ];

  toggleservice() : void{
    if(this.currentstatus == 0)
    {
      this.currentstatus = 1
    } else if (this.currentstatus == 1)
    {
      this.currentstatus = 0
    }
  }
  constructor(private servicesdata: ManageServicesService) {
    this.services = this.servicesdata.getServices();
  }

  ngOnInit(): void {
  }

}
