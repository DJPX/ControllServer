import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ManageServicesService {
  services = [
    {
      servicename: "Nextcloud",
      status: "0"
    },
    {
      servicename: "Controllservice",
      status: "0"
    },
    {
      servicename: "Gitlab",
      status: "0"
    },
    {
      servicename: "Placeholder",
      status: "0"
    }
    
  ];
  
  constructor() { }
  getServices() {
    return this.services;
  }
}
