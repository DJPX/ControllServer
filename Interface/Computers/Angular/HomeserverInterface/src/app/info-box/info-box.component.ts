import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-info-box',
  templateUrl: './info-box.component.html',
  styleUrls: ['./info-box.component.css']
})
export class InfoBoxComponent implements OnInit {
  text="Homeserver Interface v0.1"
  hidden=true
  constructor() { }

  ngOnInit(): void {
  }

}
