#!/bin/bash
# TODO Check for sudo or root user

# Prepearing for rootless containers
#sudo dnf install slirp4netns podman -y
#sudo dnf install fuse-overlayfs -y

# Increasing number of namespaces
#sudo runuser -l  root -c 'echo “user.max_user_namespaces=28633” > /etc/sysctl.d/userns.conf'
#sudo sysctl -p /etc/sysctl.d/userns.conf
# Create user
#sudo useradd -c “Homeserver” homeserver #-p 'Homeserver!PW'   # --no-create-home
#echo 'Homeserver!PW' | sudo passwd homeserver --stdin

echo Installing the ControllerServer-Project ...
echo Setting permissions
chmod +x *.sh
sudo chown -R $(whoami) ../
echo Creating images
# TODO Check if image alread exists
./createNodejsImage.sh
# TODO Check if image was created
echo Creating containers
# TODO Check for existing containers
./createPod.sh
# TODO Check if pod and containers were created
echo Installation is now finish
echo Starting service
../startServices.sh

# Delete installation files
# rm -r ../Install

