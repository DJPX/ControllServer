#!/bin/bash

newcontainer=$(buildah from node:15.10.0-alpine3.10 )

#buildah run $newcontainer /bin/sh
buildah config --entrypoint /bin/sh $newcontainer
buildah config --user root $newcontainer
#buildah run $newcontainer adduser --disabled-password --shell /bin/false nodeuser
buildah run $newcontainer mkdir /server
buildah config --workingdir='/server' $newcontainer
#buildah run $newcontainer chown nodeuser /server -R

buildah config --user root $newcontainer

buildah config --created-by "DJPX" $newcontainer
buildah config --author "DJPX" $newcontainer
buildah config --label name=node-image $newcontainer
buildah commit --rm $newcontainer nodeimage

