#!/bin/bash
# TODO for a later version maybe add an webserver to controll
# the service like with the esp32 
podman pod create --name app_server  \
  --publish 9010:8080 
 
rm -r ../Databackend/node_modules
rm ../Databackend/Services.db

containerID=$(podman run -dt --pod app_server \
  --name nodejs \
  --restart always \
  -v ../Databackend:/server:Z \
nodeimage)


# Install nodejs dependencs
podman exec -it $containerID npm install
podman exec -it $containerID npm install express --save
podman exec -it $containerID npm install sqlite3 --save
podman exec $containerID npm audit fix
podman exec -d $containerID node .



# podman pod create --name web_server  --publish 7000:80
# podman run -dt --pod web_server nextcloud
